<?php

date_default_timezone_set('America/Sao_Paulo');

class Banco
{
	public static $instance;
	
	public $mongoInstance;
	
	public function __construct()
	{
		$this->mongoInstance = new Mongo('mongodb://localhost:27017');  
	}
	
	
	public static function getInstance() {
		if (!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}
    
    public function addLogAcesso($serverInfo)
    {
    	$dateTime = new DateTime();
    	$arr = array(
    		'remote_addr' => $serverInfo['REMOTE_ADDR'],
    		'server_name' => $serverInfo['SERVER_NAME'],
    		'http_referer' => $serverInfo['HTTP_REFERER'],
    		'time' => $dateTime->format('d/m/Y H:i:s')
    	);
    	return $this->mongoInstance->selectDB('bpi')->selectCollection('acesso')->insert($arr);
    }
    
    public function addLogAcessoUsuario($login)
    {
    	$dateTime = new DateTime();
    	$arr = array(
    			'login' => $serverInfo['REMOTE_ADDR'],
    			'time' => $dateTime->format('d/m/Y H:i:s')
    	);
    	return $this->mongoInstance->selectDB('bpi')->selectCollection('usuario_acesso')->insert($arr);
    }
    
    public function addUtilizador($utilizador)
    {
    	$id = new MongoId();
    	$dateTime = new DateTime();
    	$utilizador['_id'] = $id;
    	$utilizador['datacadastro'] = $dateTime->format('d/m/Y H:i:s');
    	$utilizador['dataupdate'] = $dateTime->format('d/m/Y H:i:s');
    	$utilizador['online'] = $dateTime->format('d/m/Y H:i:s');
    	if($this->mongoInstance->selectDB('bpi')->selectCollection('utilizador')->insert($utilizador))
    	{
	    	return $id;
    	}else
    	{
    		return false;
    	}
    }
    
    public function getUtilizadorById($id)
    {
    	$row = $this->mongoInstance->selectDB('bpi')->selectCollection('utilizador')->findOne(array('_id' => $id));
    	return $row;
    }
    
    public function getAllUtilizador()
    {
    	return $this->mongoInstance->selectDB('bpi')->selectCollection('utilizador')->find(array('deletado' => 0));
    }
    
    public function addLogin()
    {
    	$arr = array(
    			'login' => 'priv1p',
    			'pass' => 'dinheiro'
    	);
    	
    	$this->mongoInstance->selectDB('bpi')->selectCollection('login')->insert($arr);
    }
    
    public function autentica($params)
    {
    	$row = $this->mongoInstance->selectDB('bpi')->selectCollection('login')->findOne($params);
    	return $row;
    }
    
    
    public function updateStatusUtilizador($id,$arrData)
    {
    	$dateTime = new DateTime();
    	$arrData['dataupdate'] = $dateTime->format('d/m/Y H:i:s');
    	$this->mongoInstance->selectDB('bpi')->selectCollection('utilizador')->update(
    		array('_id' => $id),
    		array('$set' => $arrData)
    	);
    }
    
    public function addPosicao($id,$arrPos)
    {
    	$dateTime = new DateTime();
    	$arrPos['datacadastro'] = $dateTime->format('d/m/Y H:i:s');
    	
    	$this->mongoInstance->selectDB('bpi')->selectCollection('utilizador')->update(
    		array('_id' => $id),
    		array('$push' => array('infos' => $arrPos))
    	);
    }
    
    public function getAllUsers()
    {
    	
    }
    
}

