// DETECTS IF NAMESPACE OF THE APP EXISTS, CREATES THE NAMESPACE OF THE APP.
var BPI = BPI || {};

// APP MAIN INIT - MODULE PATTERN.
BPI.Main = (function () {

    //RETURN OBJECT LITERAL.
    return {
        //PUBLIC ATTRIBUTES ANYONE MAY READ/WRITE.
        NAME: "Application initialize module",
        VERSION: 1.1,

        init: function () {

					this.initTabs();
					this.initLabelhide();
					this.initSlider();
					this.initSites();
                                        this.initSimuLayer();
					this.initMenu();
					this.initFaqs();
					this.initPopin();
                                        this.initMyPopin();
					this.initSelectbox();
					this.initSearchLayer();
					this.initClickToCall();
					this.initClickToChat();
                                        this.initGallery();
                                        this.initBranches();
                                        this.resizeText();
                                        
                                        this.initCarroussel();
					this.initSlidePopin();
                                        
                                        this.initCardSize();
                                        this.initLinksBpi();
                                        this.initiScaleDownTitle();
                                        this.initPmelidertab();
                                        this.initiSimulaPlaceholder();
					// CALL TO PRIVATE FUNCTION.
			
        },
        initTabs: function () {
         if (!$('.tabs').length) return false; 
				BPI.UI.BPITabs();
        },
		initLabelhide: function () {
        	if (!$('#header').length) return false; 
				BPI.UI.formLabelAutoHide();
        },
		initSlider: function () {
        	if (!$('#focus').length) return false; 
				BPI.UI.adatptativeSlider();
        },
		initSites: function () {
        	if (!$('.sites').length) return false; 
				BPI.UI.initSitesDropdown();
        },
        initSimuLayer: function () {
        	if (!$('#simulators-list').length) return false; 
				BPI.UI.initSimuList();
        },
		initMenu: function () {
        	if (!$('#menu').length) return false;
        	BPI.UI.menu();
        },
		initPopin: function () {
        	if (!$('.popin').length) return false;
        	BPI.UI.popin('popin');
        },
		initMyPopin: function () {
        	if (!$('.mypopin').length) return false;
        	BPI.UI.mypopin();
        },
		initFaqs: function () {
        	if (!$('.accordion').length) return false;
        	BPI.UI.faqs();
        },
		initSelectbox: function () {
        	if (!$('.select').length) return false;
        	BPI.UI.selectbox();
		},
		initSearchLayer: function () {
		    if (!$('.simple-search').length) return false;
		    BPI.UI.searchShowLayer();
		    BPI.UI.onClickOptionSearch();
		},
		initClickToCall: function () {
		    if (!$('.click-call').length) return false;
		    BPI.UI.onClickShowFormClickToCall();
		    BPI.UI.onClickHideFormClickToCall();
		},
                initClickToChat: function () {
                    if (!$('.click-chat').length) return false;
                    BPI.UI.onClickShowFormClickToChat();
                    BPI.UI.onClickHideFormClickToChat();
                },
                initGallery: function () {
                    if (!$('.gallery').length) return false;
                    BPI.UI.gallery();
                },
                initBranches: function () {
			if (!$('#map-canvas').length) return false;
			BPI.UI.brancheFilters();
                },
                resizeText: function () {
                    if (!$('#textsize').length) return false; 
                        BPI.UI.resizeText();
						
                },
                initCarroussel: function () {
                    if (!$('.storefront').length) return false; 
                            BPI.UI.carroussel($('.storefront'));
                },
                initSlidePopin: function () {
                    if (!$('.slide-popin').length) return false; 
                            BPI.UI.popin('slide-popin');
                },
                initCardSize: function () {
        	if (!$('.cards').length) return false; 
			BPI.UI.cardHeaderSize();
                
                },
                initLinksBpi: function () {
                    if(!$('.linksBpi').length) return false;
                        BPI.UI.linksBpi();
                },
                initiScaleDownTitle: function () {
                    if(!$('.column header h1').length) return false;
                        BPI.UI.scaleDownTitle();
                },
                initPmelidertab: function () {
                    if(!$('.tabsdmk').length) return false;
                        BPI.UI.pmelidertab();
                },
                initiSimulaPlaceholder: function () {
                    if(Modernizr.input.placeholder) return false;
                        BPI.UI.simulaPlaceholder();
                }
		 
    }
	
}()); // THE PARENS HERE CAUSE THE ANONYMOUS FUNCTION TO EXECUTE AND RETURN.

// ON DOM READY INIT APPLICATION.
$(document).ready(function(){
	BPI.Main.init();
});

/**
 * Outputs a log of the passed in object. This is centralized in one method so
 * that we can keep info logs around the site and easily disable/enable them
 * when jumping between live/dev.
 * @param {string} o Message to log to console.
 */
BPI.log = function (o) {
    if (window.console && o) {
        window.console.log(o);
    }
};
