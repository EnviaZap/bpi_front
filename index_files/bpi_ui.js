if (!BPI) var BPI = {};
else if (BPI && typeof (BPI) != "object")
	throw new Error("BPI is not an Object type");

/* BPI.UI
* All user inteface under this class name
*/
BPI.UI = {};

/* BPI.UI.initSitesDropdown 
	Init fotter Sites list dropdown
*/
BPI.UI.initSitesDropdown = function () {
	
	var listHeader = $('.sites dt'),
            listElem = $('.sites dd');
	
	listElem.hide('fast');
	
	listHeader.bind('click', function(){
        
        var footerMinHeight = (listElem.size() + 1) * listElem.height();
 
        // Se a combobox de sites não ficar totalmente visível  depois de aberta move a janela para ficar visível
        var pos = $(".sites").offset().top -  $(window).height() + footerMinHeight;
        if(!listElem.is(':visible') && $(window).scrollTop() + $(window).height() < $(".sites").offset().top + footerMinHeight){
            $('html, body').animate({
                scrollTop: pos + 10
            }, 600);
        }
        
        $('#footer').css('min-height', footerMinHeight + 30 + 'px');
        
        
            listElem.show('fast', function(){
            
                $('body').bind('click', function(){
                    $('#footer').css('min-height', '0');
                    listElem.hide('fast', function(){
                            $('body').unbind('click');
                    });
                });
            });
	});
}

/* BPI.UI.BPITabs 
	Init simple tabs
*/
BPI.UI.BPITabs = function () {

	var tabElem = $('.tabs > ul li');

	tabElem.bind('click', function(e) {
		var tabLink = $(this),
					index = tabLink.index(),
			 tabLinks = tabLink.parents('ul').find('li'),
				getTabs = tabLink.parents('.tabs').find('article'),
				 getTab = getTabs.eq(index);
	
		getTabs.removeClass('show-tab');
		getTabs.addClass('hide-tab');
	
		getTab.removeClass('hide-tab');
		getTab.addClass('show-tab');
		
		tabLinks.removeClass('current');
		tabLink.addClass('current');
                //alert(tabLink.find('a').attr('href'));
                //if(tabLink.find('a').attr('href') == '#') {
                    e.preventDefault();
                //} 
		
	
	});
}


/* BPI.UI.formLabelAutoHide 
            Hide label form on click and on select input form (ex:search)
*/
BPI.UI.formLabelAutoHide = function () {
                        //$('.simple-search'),$('#login'),
            var labels = [$('.layerform'),$('.formfields'),$('.filters .field'),$('.currency-converter')];

            $.each(labels, function()
            {
                        
                        this.find('label').removeClass('hidden');
                        //LDIA START
                        this.find('label').bind('click', function() {
                        //alert('label click');
                                   $(this).hide();
                                   $(this).parent().parent().find('input').focus();
                                   $(this).parent().parent().find('textarea').focus();
                        
                        /*
                                   $(this).next('input').focus();
                                   $(this).next('textarea').focus();
                        */
                        //$(this)[0].control.focus();
                        
                        
                        });
                        
                        this.find('input[type=text]').bind('focus', function() {
                        //alert('input text focus: ' + $(this).val());
                                   if( !$(this).val() ){
                          //         alert('No val, please hide label: ' + $(this).attr('id'));
                            $(this).parent().parent().find('label').hide();
                             //$($(this)[0].labels[0]).hide();
                                   }
                        });
                        
                        this.find('input[type=text]').bind('blur', function() {
                        //alert('input text blur: ' + $(this).val());
                                   if( $(this).val() == '' ){
                            $(this).parent().parent().find('label').show();
                            //$($(this)[0].labels[0]).show();
                                   }
                        });
                        
                        this.find('textarea').bind('focus', function() {
                                   if( !$(this).val() ){
                            $(this).parent().parent().find('label').hide();
                            //$($(this)[0].labels[0]).hide();
                                   }
                        });
                        
                        this.find('textarea').bind('blur', function() {
                                   if( $(this).val() == '' ){
                            $(this).parent().parent().find('label').show();
                            //$($(this)[0].labels[0]).show();
                                   }
                        });
                //LDIA END
            });

}



/* BPI.UI.adatptativeSlider 
	Init home page slider
*/
BPI.UI.adatptativeSlider = function () {

        
        if($("#slider").length == 0) {
            $('#focus').remove();
            return;
        }
        
	var speed = 1200;
	
	if($('#items li').length <= 1) speed = 0;
	
	// Sets the slides width on page load
	var i = $('#focus').width();
	if (i > 319){ 
		$('#items > li').css({ width: i }); 
	}
	
	// Scrollable and navigator plugin settings.
	$("#slider").scrollable({ circular: true, touch: false, easing: 'easeOutQuart', speed: speed}).navigator({ navi: '#navigation' }).autoscroll({ 
		autoplay: true,
		autopause:true, 
		interval: 4000
	});
	
	resizeSlider();
	
	// Window resize code
	$(window).resize(function() {
		resizeSlider();
	});
        
        $('#navigation').attr('style' , 'width:' + $('#navigation li').length*18 + 'px');

}

/* recalculate slider on resize*/
resizeSlider = function () {

	window.api = $("#slider").data("scrollable");
	
	var a = 1 + window.api.getIndex(),
            w = $(window).width(),
            l = a * 1400;
            minWidth = 940;
//        if(w > 960) {
//            $('.site').css({ 'overflow-x': 'hidden'});
//        } else {
//            $('.site').css({ 'overflow-x': 'visible'});
//        }


	if (w > minWidth) {
		$('#slider').css({ width: 1400 });
//		$('#focus').css({ width: w-1});
                $('#focus').css({ 'width': ''});

		$('#items').css({ left: + - +l });
		$('#items > li').css({ width: 1400 });
		$('#items > li > .wrapper').removeAttr('style');
	}
	
	if (w > minWidth && w < 1400) {
		$('#slider').css({ marginLeft: -((1400-w)/2) });
	} else if (w >= 1400){
		$('#slider').css({ marginLeft: 'auto' });
	}
        
        if(w < minWidth) {
            $('#focus').css({ width: minWidth});
        }
}


/* BPI.UI.menu 
	Init submenu on menu ribbon
*/
BPI.UI.menu = function () {
	
	var childs = $('.submenu > ul > li'),
            submenu = $('.submenu');


	$('#menu > ul > li a + div').parents('li').bind('mouseenter', function(e){
		
		e.stopPropagation();
		
		$(this).addClass('menuover');
		$(this).parents('#menu').addClass('priority');
		
		var block1 = $(this).find('.menuinfo'),
                    block2 = $(this).find('.menuhighlights'),
                    elemheight = $(this).find("dl:first").height();

                if(elemheight > block2.height() && elemheight > block1.height()){
                    $('.menuover').find('.submenu').css('height', elemheight);
                } else if(block1.height() >= block2.height() && block1.height() > elemheight){
                    $('.menuover').find('.submenu').css('height',block1.height());
                } else {
                    $('.menuover').find('.submenu').css('height', block2.height());
                }
                
		$('.menuover').bind('mouseleave', function(e){
			
			e.stopPropagation();

			$(this).removeClass('menuover');
			$(this).parents('#menu').removeClass('priority');
			childs.unbind('mouseover');
			childs.removeClass('submenuover');
			submenu.removeAttr('style');
			submenu.find('.submenuover').removeClass('submenuover');

		});

		$('.submenu > dl > dd > a').bind('mouseover', function(e){
			e.stopPropagation();
			
			var lev3Menu = $(this).next('ul').height();
			
			submenu.find('.submenuover').removeClass('submenuover');
			$(this).parent().addClass('submenuover');

			if(lev3Menu !== null && lev3Menu > $('.menuover').find('.submenu').height()) $('.menuover').find('.submenu').css('height', lev3Menu);
		});
	});
}



/* BPI.UI.initSimuList 
	Init Simulator list layer on click
*/
BPI.UI.initSimuList = function () {

	var elem = $('#simulators-list strong + a');
	
	elem.bind('click', function(e){
		e.preventDefault();
		$(this).parent('.quick-access').toggleClass('selected');
	});

}

/* BPI.UI.faqs 
	Init faqs accordion
*/
BPI.UI.faqs = function () {
	
	var faqsDT = $('.accordion dt');
                        
        //$('.accordion dd').hide();
        faqsDT.unbind('click');
        faqsDT.bind('click', function(e){
                
                var content = $(this).next('dd'),
                        elem = $(this);

                if(!content.is(":visible")) {
                        var topo = elem.offset().top;
                   
                        $('.item-open + dd').slideUp('slow', function(){
                            topo = elem.offset().top;
                            //console.log(topo);
                        });
//                        $('.item-open + dd').slideUp('slow');
                        faqsDT.removeClass('item-open').addClass('item-close');
                        elem.removeClass('item-close').addClass('item-open');
                        content.slideDown('slow', function() {
//                            $(document).scrollTop( $(elem).offset().top );  
                            $('html, body').animate({scrollTop : topo},600);
                            return false;
                        });
                        //alert('scroll!');
                        
                } else 	{ 
                        elem.removeClass('item-open').addClass('item-close');
                        content.slideUp('slow');
                }

                e.stopPropagation();
                
        });
        
        if(window.location.hash) {
            var hash = '#' + window.location.hash.substring(1); //Puts hash in variable, and removes the # character
            //      alert (hash);
        
            if($(hash).length) {
                $('.accordion dd').hide();
             
                $(hash).click();
            }
          }

}

/* BPI.UI.accordion 
	Init accordion
*/
BPI.UI.accordion = function () {
	
	var accDT = $('.accordion dt');
	
	$('.accordion dd').hide();
	$('.accordion .open').slideDown('slow');
	
	accDT.bind('click', function(){
		
		var content = $(this).next('dd'),
			elem = $(this);

		if(!content.is(":visible")) {
			$('.item-open + dd').slideUp('slow');
			accDT.removeClass('item-open').addClass('item-close');
			elem.removeClass('item-close').addClass('item-open');
			content.slideDown('slow');
		} else 	{ 
			content.removeClass('open');
			elem.removeClass('item-open').addClass('item-close');
			content.slideUp('slow');
		}
		
	});

}

/* BPI.UI.popin 
	Init popins
*/
BPI.UI.popin = function (who) {
	
	var html = '<div id="' + who + '"><div class="overlay"></div><a href="#" class="popin-close"></a><div class="popin-content"></div></div>';
	
	$('.' + who).bind('click', function(e){
		e.preventDefault();
		
		var content = $(this).attr('rel'),
			url = $(this).attr('href');
		
		$('body').append(html);
		
		$('.popin-close, .overlay').bind('click', function(e){
			e.preventDefault();
			$('#'+ who).remove();
		});
		
		if(!content == '' && who == 'popin') BPI.UI.popinload(content, url);
		if(who == 'slide-popin') BPI.UI.slideGenerator(url);
	
		$(window).scrollTop(0);
		
	});

}

BPI.UI.mypopin = function (graph) {
	$('.mypopin').unbind('click');
//alert('graph: ' + graph);
	var html = '<div id="popin"><div class="overlay"></div><a href="#" class="popin-close">X</a><div class="popin-content"><article class="faqs"></article></div></div>';
	if(graph) {
            html = '<div id="popin-graph"><div class="overlay"></div><a href="#" class="popin-close">X</a><div class="popin-content"><article class="faqs"></article></div></div>';
        }
        
	$('.mypopin').bind('click', function(e){
		e.preventDefault();
                
		var cl = $(this).attr('rel');
		var content = $('.' + cl).html();
//			url = $(this).attr('href');
//		alert('Content: ' + content);

		$('body').append(html);
		
		$('.popin-close, .overlay').bind('click', function(e){
			e.preventDefault();
			$(this).parent('#popin').html('');
                        $(this).parent('#popin-graph').html('');
		});
		
		if(!content == '') $('.faqs').append(content);
		
                
		var faqsDT = $('.faqs .accordion dt');
            
            //$('.accordion dd').hide();
            
            faqsDT.bind('click', function(e){
                    
                    var content = $(this).next('dd'),
                            elem = $(this);
    
                    if(!content.is(":visible")) {
//                        var topo = elem.offset().top;
                            $('.faqs .item-open + dd').slideUp('slow');                       
//                        $('.faqs .item-open + dd').slideUp('slow', function(){
//                            topo = elem.offset().top;
//                            //console.log(topo);
//                        });
                            faqsDT.removeClass('item-open').addClass('item-close');
                            elem.removeClass('item-close').addClass('item-open');
                            content.slideDown('slow');
//                        content.slideDown('slow', function() {
//                                $('html, body').animate({scrollTop : topo},600);
//                                return false;
//                        });
                        //alert('scroll!');
                    } else 	{ 
                            elem.removeClass('item-open').addClass('item-close');
                            content.slideUp('slow');
                    }
    
                    e.stopPropagation();
                    
            });
                
                
                
                
                
                
                
                $(window).scrollTop(0);
                
                
                
                
		
	});
        
        
        
        
        
}
/* BPI.UI.popinload 
	Use parameters in order to load content from a page and insert it on the popin content area
*/
BPI.UI.popinload = function (content, url) {

	if (content != 'video') { 

		$('.popin-content').load(url + ' .' + content, function(response, status, xhr) {
		  if (status == "error") {
			var msg = "Sorry but there was an error: ";
			$("#error").html(msg + xhr.status + " " + xhr.statusText);
		  } else {
			BPI.UI.accordion();
		  }
		});
	
	} else {
		
		var vElem = '<iframe width="440" height="330" src="' + url + '?autoplay=1" frameborder="0" allowfullscreen></iframe>'
		$('.popin-content').append(vElem);
		
	}
	
	$('.popin-content').addClass(content);

}

/* BPI.UI.slideGenerator 
	slider for product popin
*/
BPI.UI.slideGenerator = function (url) {
	var start = url.replace('#content', '');
        var html = '<div class="storefront-popin"><div class="slider" style="margin-left: -' + (start-1) * 545 + 'px">';
	var slider = $('.slider .slider-item[data-rel="image"] img');
       
	
	slider.each(function(index, element) {
		var image = slider[index].outerHTML,
			container = $(element).data('container'),
			getTitle = $('#' + container).find('h4').html(),
			indexItems = $('#' + container).find('ul li').length,
			getItems = '';
		/*
                console.info($(element).parent().find('div').html());
                        
		for(i = 0; i<indexItems; ++i){
			var elem = $('#' + container).find('ul li:eq(' + i + ')').html();
			if (elem == 'undefined') return false;
			getItems += '<li>' + elem + '</li>';
		}
                */
		if ($(element).parent().find('div').html().length > 0) {
		    html += '<div class="slider-item">' + image + '<div class="slider-item-content">'+$(element).parent().find('div').html()+'</div></div>';
		} else {
		    html += '<div class="slider-item">' + image + '</div>';
		}

    });
	
	html += '</div><div class="slider-nav"><a href="#" class="nav-left icon-arrow-left"></a><a href="#" class="nav-right icon-arrow-right"></a></div></div>';
	
	$('.popin-content').html(html);
	
	BPI.UI.carroussel($('.storefront-popin'));
	
	$('.storefront-popin').find('.nav-left, .nav-right').css('display', 'block');
	
	if(start == 1) $('.storefront-popin').find('.nav-left').css('display', 'none');
	if(start == $('.storefront-popin .slider .slider-item').length) $('.storefront-popin').find('.nav-right').css('display', 'none');

}

/* BPI.UI.selectbox 
	Init select costumization
*/
BPI.UI.selectbox = function () {
	
	var who = $('.AFContentCell'),
		size = who.find('select').width();
		getSelectOption = $('.select select option:selected').text();
		html = '<span class="drop-selected" style="width:' + size + 'px">' + getSelectOption + '</span>';
        
        if(size != null) {
            who.append(html);
            
            who.find('select').change( function(){
            
                    $(this).next('.drop-selected').html($(this).find('option:selected').text());
                    
            });
        }

}

/* BPI.UI.selectbox 
	Init select costumization

BPI.UI.selectbox = function () {
	
	var who = $('.select');
	
	$.each(who, function( index, value ) {


	if( $("html").hasClass("lt-ie9")) var size = $(this).find('select').width() - 14;
	else var size = $(this).find('select').width();
		
		var getSelectOption = $(this).find('select option:selected').text(),
			html = '<span class="drop-selected" style="width:' + size + 'px"><span>' + getSelectOption + '</span></span>';
			
			$(this).append(html);
			$(this).find('select').change( function(){
				$(this).next('.drop-selected').find('span').html($(this).find('option:selected').text());
			});

	});
	


}
*/
/* BPI.UI.searchShowLayer 
	show layer when the search val bigger then 2 caracter. 
*/
BPI.UI.searchShowLayer = function () {
    
    if( $('.search-layer').is(":visible") ) {
            $('.simple-search').on('keydown', function (e) {
                    
                    var hover = $(this).find('.hover'),
                            firstHover = $(this).find('li:first-child');
                    
                    switch(e.which) {
            
                            case 38:
                                    hover.prev('li').addClass('hover').next('li').removeClass('hover');
                            break;
    
                            case 40:
                                    if( hover.is(":visible") ) hover.next('li').addClass('hover').prev('li').removeClass('hover');
                                    else firstHover.addClass('hover');
                            break;
                            
                            default: 
                                    hover.removeClass('hover');
                            return;
                    }
                    
                    $(this).find('input[type=text]').val($('.search-layer .hover').text());

                    
                    e.preventDefault();
            });
    }
    
    $('#search, #page-search').keyup(function (event) {
            
            var length = $(event.currentTarget).val().length;
            if (length > 2) {
                var _keyCode = event.which;
                if(_keyCode == 38 || _keyCode == 40) {
                    return;
                }
                if (_keyCode == 13 ) {
                
                    $('#search').parent().find('label').css('display', 'none');
                    $('#search').val($(event.currentTarget).val());
                    
                    doPost($(event.currentTarget).attr('id'), $("#contextPath").val());
                    BPI.UI.closeField();
                    return;
                    /*
                    if( $(event.currentTarget).parent().parent().find('.search-layer').css('visibility') !== 'hidden' &&  $(event.currentTarget).parent().parent().find('.search-layer').css('display') !== 'none' ) {
                        //alert('just close search layer please!');                        
                        BPI.UI.closeField();
                        return;
                    } else {
                        $('#search').parent().find('label').css('display', 'none');
                        $('#search').val($(event.currentTarget).val());
                        doPost($(event.currentTarget).attr('id'), $("#contextPath").val());
                        return;
                    }
                    */
                } else {
                    doKeyProcess($(event.currentTarget).attr('id'), $("#contextPath").val());
                    
                }
                
                $(this).parents('div.center').find('.search-layer').removeClass('hidden');
                $(window).on('click', BPI.UI.closeField);
        }
        /*
        if($('#search-page').val() != undefined && $('#search-page').val().length > 2) {
                $(this).parents('div.center').find('.search-layer').removeClass('hidden');
            $(window).on('click', BPI.UI.closeField);
        }
        */
    })
}

/* BPI.UI.onClickOptionSearch 
	onclick put the word choosed on the box
*/
BPI.UI.onClickOptionSearch = function () {
    $('.search-layer li').on('click', function () {
//		alert('click!');
        
		//$('#search').val($(this).text());
                var who = $(this).parents().find('[id*=search]');
                var what = $(this).text();
                
                $.each(who, function(){$(this).val(what);});
                BPI.UI.closeField();
    })
}

BPI.UI.closeField = function(){
    
    $('.search-layer .hover').removeClass('hover');
    $(window).off('click', BPI.UI.closeField);
    $('.search-layer').addClass('hidden');
}



/* BPI.UI.onClickShowFormClickToCall 
	onclick highlight click to call show the form
*/
BPI.UI.onClickShowFormClickToCall = function () {
    $('.click-call a').bind('click', function (e) {
        e.preventDefault();
        $('.form-click-call').removeClass('hidden');
        $('.click-call').addClass('hidden');
    })
}

/* BPI.UI.onClickHideFormClickToCall 
	onclick form close button hide form click to call
*/
BPI.UI.onClickHideFormClickToCall = function () {
    $('.form-click-call .buttons a').bind('click', function (e) {
        e.preventDefault();
        $('.form-click-call').addClass('hidden');
        $('.click-call').removeClass('hidden');
    })
}

/* BPI.UI.onClickShowFormClickToChat 
	onclick highlight click to chat show the form
*/
BPI.UI.onClickShowFormClickToChat = function () {
    $('.click-chat a').bind('click', function (e) {
        e.preventDefault();
        $('.form-click-chat').removeClass('hidden');
        $('.click-chat').addClass('hidden');
    })
}

/* BPI.UI.onClickHideFormClickToChat 
	onclick form close button hide form click to chat
*/
BPI.UI.onClickHideFormClickToChat = function () {
    $('.form-click-chat .buttons a').bind('click', function (e) {
        e.preventDefault();
        $('.form-click-chat').addClass('hidden');
        $('.click-chat').removeClass('hidden');
    })
}

/* BPI.UI.forms 
	radio buttons style
*/
BPI.UI.forms = function () {
	
	$(".checkfields input").on( "click", function() {
	  
	  var checkWho = $(this).parent().next('label');

	  if( !checkWho.hasClass('selected')) checkWho.addClass('selected')
	  else checkWho.removeClass('selected');
	  
	});
	
	$(".radiofields input").on( "click", function() {
	  
	  $('.radiofields label').removeClass('selected');
	  $(this).parent().next('label').addClass('selected');
	  
	});
}

/* BPI.UI.gallery 
	Init gallery slider
*/
BPI.UI.gallery = function () {

	var gallery = $('.gallery');
		elemts = gallery.find('li'),
		gallnav = '<div id="nav">',
		toMove = elemts.width();
	
	gallery.addClass('js-gallery');
	if(elemts.length > 1) {
            $.each( elemts, function( key, value ) {
                    gallnav += (key == 0) ? '<a href="#" class="active">' + parseFloat(key + 1) + '</a>' : '<a href="#">' + parseFloat(key + 1) + '</a>';
            });
    
            gallnav += '</div>';
    
            gallery.append(gallnav);
            
            
            $('#nav a').bind('click', function(e){
                    if ($('.gallery ul').is(':animated')) return false;
                    e.preventDefault();
                    
                    var elemt = parseFloat($(this).text() - 1) * 459;
                    
                    
                    $('#nav a').removeClass('active');
                    $(this).addClass('active');
                    
                    gallery.find('ul').animate({
                            marginLeft: -elemt
                      }, 1000);
                    
            });
        }
	
}

/* BPI.UI.brancheFilters 
	Branches filters options
*/
BPI.UI.brancheFilters = function () {
	
	$('#companies').hide();
	
	var selector = $('#branche');
	
	selector.change( function(){
	
		if ($(this).val() != 'investment'){
			$('.note').show();
			$('#companies, #branches').hide();
			$('#' + $(this).val()).show();
		} else {
			
			$('.note, #companies, #branches').hide();
		}

		
	});
}


/* BPI.UI.googleApi 
	Google map costumization
*/
BPI.UI.googleApi = function () {

	var map;
	var myLatlng = new google.maps.LatLng(38.723917, -9.148294);
	
	function initialize() {
		var mapOptions = {
			zoom: 17,
			center: myLatlng,
			disableDefaultUI: false,
			mapTypeControl: true,
			streetViewControl: true,
			zoomControl: true,
			panControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
	
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		
		var contentString = '<article class="map-info" style="margin-left: 15px; background: white; padding: 20px;">'+
							'<header class="infoBox">'+
							'<h1>Alvalade</h1>'+
							'<h2>Av. da Liberdade, 454, Loja B<br> 1200-324 Lisboa</h2>'+
							'</header>'+
							'<ul>'+
							'<li><strong>Telefone</strong> 214 547 512 </li>'+
							'<li><strong>Telefone a partir do estrangeiro</strong> +351 214 547 512 </li>'+
							'<li><strong>Fax</strong> 214 587 963 </li>'+
							'</ul>'+
							'<dl>'+
							'<dt>HorÃ¡rio de Funcionamento</dt>'+
							'<dd>10hÃ s 16h</dd>'+
							'<dt>Coordenadas GPS</dt>'+
							'<dd>-23.538982</dd>'+
							'<dd>-46.656514</dd>'+
							'</dl>'+
							'<dl>'+
							'<dt>ServiÃ§os</dt>'+
							'<dd>EspaÃ§o Internet</dd>'+
							'<dd>MÃ¡quina DepÃ³sito</dd>'+
							'<dd>MÃ¡quina de Cheques</dd>'+
							'</dl>'+
							'</article>';
		
		var marker = new google.maps.Marker({
		  position: myLatlng,
		  map: map,
		  title: 'BPI BalcÃ£o',
		  icon: "img/logos/icon-bpi.png"
		});
		
		var myOptions = {
				 content: contentString
				,disableAutoPan: false
				,maxWidth: "250px"
				,pixelOffset: new google.maps.Size(5, -150)
				,zIndex: null
				,boxStyle: { 
				  background: "url('img/bg/map-info-arrow.png') no-repeat 0 123px"
				  ,width: "286px"
				 }
				,closeBoxMargin: "3px 3px 0 0"
				,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
				,infoBoxClearance: new google.maps.Size(1, 1)
				,isHidden: false
				,pane: "floatPane"
				,enableEventPropagation: false
		};
		
		var maplayer = new InfoBox(myOptions);
		
		google.maps.event.addListener(marker, 'click', function() {
			maplayer.open(map, marker);
		});
	
	
	}
	
	google.maps.event.addDomListener(window, 'load', initialize);

}

/* BPI.UI.resizeText 
	Resize text for acessibility
*/
BPI.UI.resizeText = function () {
	
		var container = $('.text'),
			 	asise = $('.column header'),
			   header = $('.detail header')
			 fontSize = 1;
	
	$('#font-decrease a').bind('click', function(e) {
		
		e.preventDefault();
		
		if (fontSize < 0.8) return false;
		
		fontSize -= .1;
		container.css('fontSize' , fontSize + 'em');
		asise.css('fontSize' , fontSize + 'em');
		header.css('fontSize' , fontSize + 'em');
		
		execptions('add');
		
	});
	
	$('#font-reset a').bind('click', function(e) {
		
		e.preventDefault();
		
		fontSize = 1;
		container.css('fontSize', '');
		asise.removeAttr('style');
		header.removeAttr('style');
		
		execptions();
		
	});
	
	$('#font-increase a').bind('click', function(e) {
		
		e.preventDefault();

		if (fontSize > 1.22) return false;
		
		fontSize += .1;
		container.css('fontSize' , fontSize + 'em');
		asise.css('fontSize' , fontSize + 'em');
		header.css('fontSize' , fontSize + 'em');
		
		execptions('add');

	});

	
	
	function execptions(action) {
		
		var who = [$('.text .filters'),$('.icon-checkmark'),$('.accede')];	
		
		$.each(who, function(){
			
			if(action == 'add') $(this).css('fontSize' , '13px');
			else $(this).removeAttr('style');
			
		})
		
		if (!$('.cards').length) return false; 
		
		$('.cards div').removeAttr('style');
		BPI.UI.cardHeaderSize()

	}

}

/* BPI.UI.carroussel
	New carroussel for product area
*/
BPI.UI.carroussel = function (cWho) {
	var wItem = cWho.width(),
		wItemAll = cWho.find('.slider > div').length * cWho.width(),
		moveWho = cWho.find('.slider'),
		moveWhoChilds = cWho.find('.slider-nav-container'),
		selected = 0,
		navItems = cWho.find('.slider-nav-container a'),
		wNavItem = 0,
		nNavVisible = navItems.index(),
		where = 0,
		lenght = nNavVisible;
        
        // remove o slider quando existe um unico produto
        if(cWho.find('.slider > div').length == 1){
            $('.slider-nav').remove();
            return;
        }
        
        if($('.storefront-popin').length == 0 && $('.slider-nav-container').length > 0){
            for (var i = 0; i < 4; ++i) {
		$('.slider-nav-container a:eq(' + i + ')').addClass('visible');
            }
        }
	
	moveWho.width(wItemAll);
	cWho.find('.nav-left').css('display', 'none');
        if(navItems.length == 1)
            cWho.find('.nav-right').css('display', 'none');
            
	cWho.find('.slider-nav > a').on('click', function(e){
		
		e.preventDefault();
		
		if (moveWho.is(':animated')) return false;
		
		
		if($(this).hasClass('nav-right')){
			wItem = -cWho.width();
			selected = cWho.find('.slider-nav-container .selected').next();
			wNavItem = -172;
			nNavVisible = 0;
		} else {
			wItem = cWho.width();
			selected = cWho.find('.slider-nav-container .selected').prev();
			wNavItem = 172;
			nNavVisible = 4;
		}
		
		cWho.find('.slider-nav-container a').removeClass('selected');

		if(!selected.hasClass('visible')){
			selected.addClass('visible');
			moveWhoChilds.find('.visible:eq(' + nNavVisible + ')').removeClass('visible');
			moveWhoChilds.animate({ marginLeft: "+=" + wNavItem }, 1000);
		}

		$(this).parents('.slider-nav').prev('.slider').animate({ marginLeft: "+=" + wItem }, 1000, function(){
			selected.addClass('selected');
			arrowControl();
		});

	});

	navItems.on('click', function(e){
		
		e.preventDefault();

		var move = 0;
		var indexWhen = moveWhoChilds.find('.selected').index();
		
		if (moveWho.is(':animated')) return false;
		
		navItems.removeClass('selected');
		$(this).addClass('selected');
		
		var indexNow = $(this).index();
		
		if (indexWhen == indexNow) return false;
		
		move = (indexWhen-indexNow) * cWho.width();

		moveWho.animate({ marginLeft: "+=" + move }, 1000, function(){
			arrowControl();
		});
		
	});


	function arrowControl() {			
			where = (-parseInt(moveWho.css('margin-left')) / cWho.width()) + 1,
			lenght = cWho.find('.slider > div').length;             
			if(where == 1)
                            cWho.find('.nav-left').css('display', 'none');			
                        if(where == lenght) cWho.find('.nav-right').css('display', 'none');
                        
                        if(where > 1)
                            cWho.find('.nav-left').css('display', 'block');
                        if(where < lenght)
                            cWho.find('.nav-right').css('display', 'block');

	}
}

/* BPI.UI.cardHeaderSize
	Fix height for all div's
*/
BPI.UI.cardHeaderSize = function () {
	
	var elem = $('.cards div'),
		size = 0;

	
	$.each(elem, function( k, v ) {
	  
	  if( $(this).height() > size ) size = $(this).height();
	  
	});

	elem.height(size);
		
}

BPI.UI.linksBpi = function() {
    var elem = $('.linksBpi');


    $.each(elem, function (){
        var rel = $(this).attr('rel');
        var args = rel.split(';');
        var url = args[0];
        var target = args[1];
        var specs = args[2];
        $(this).click(function(){
            window.open(url, target, specs);
            return false;
        });
    })
}

BPI.UI.scaleDownTitle = function(){
    var title = $('.column header h1');
    
    scaleDownText(title);
}

// Específico para o contéudo PME Líder, PR_WCS01_UCM01011222.
// Abrir uma tab específica da página com o parametro #tab<número_da_tab> passado no url
BPI.UI.pmelidertab = function(){
    if(window.location.hash) {
        var hash = window.location.hash.substring(1);
        var test = '#' + window.location.hash.substring(1);
        
        if($(test).length) {
            var id = '#' + hash;
            
            $('a[href="#tab1"]').closest('li').removeClass('current');
            $('a[href=' + id + ']').closest('li').addClass('current');
            
            $('#tab1').removeClass('show-tab');
            $('#tab1').addClass('hide-tab');
            $(id).removeClass('hide-tab');
            $(id).addClass('show-tab');
        }
        // fix de modo a mover a posição da janela para o topo
        $(window).load(function() {
            $(window).scrollTop(0);
        });
    }
}

/* 
 * Se o browser não suportar placeholders simula o comportamento
 * dos placeholders
 * 
 * Fix para IE inferior a 10
 */
BPI.UI.simulaPlaceholder = function(){
//    $("input[type=text]").each(function(){
//        if($(this).attr("placeholder") != ""){
//            if($(this).val() == ""){
//                $(this).val($(this).attr("placeholder"));
//            }
//            $(this).focus(function(){
//                if($(this).val() == $(this).attr("placeholder")){
//                    $(this).val("");
//                }
//            });
//            $(this).blur(function(){
//                if($(this).val() == ""){
//                    $(this).val($(this).attr("placeholder"));
//                }
//            });
//        }
//    });

//    var labels = [$('.simple-search'),$('#login')];
//    $.each(function(){
    $("input[type=text]").each(function(){
        if($(this).attr("placeholder")!=""){
            if($(this).val() == ""){
                $(this).parent().find("label").show();
            }
            $(this).focus(function(){
                if($(this).val() == ""){
                    $(this).parent().find("label").hide();
                }
            });
            $(this).blur(function(){
                if($(this).val() == ""){
                    $(this).parent().find("label").show();
                }
            });
        }
    });
}