
$(function() {
    if ($('#login-users').length) {
        $('#login-users').on('keydown', function (e) {
            if(e.which == 10 || e.which == 13) {
                submit2particulares('login-users');
            }
        });
    }
});

$(function() {
    if ($('#login-company').length) {
        $('#login-company').on('keydown', function (e) {
            if(e.which == 10 || e.which == 13) {
                submit2empresas('login-company');
            }
        });
    }
});

function submit2particulares(origfield) {
    document.getElementById('users_username').value = document.getElementById(origfield).value;
    document.getElementById('login-users-form').submit();
    
    return false;
}

function submit2empresas(origfield) {
    document.getElementById('company_username').value = document.getElementById(origfield).value;
    document.getElementById('login-company-form').submit();
    
    return false;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function doPost(id, contextPath) {
    var what = $('#'+id);
    if(what.val().length > 2) {        
        what[0].form.setAttribute('action', contextPath); 
        what[0].form.submit();
    }
    return false;
}
            
function doKeyProcess(id, contextPath) {
    
        var val = $('#'+id).val();
        var who = $('#'+id).parents('div.center').find('.search-layer');
        
        //who.html('<span class="line"></span><img src="${pageContext.request.contextPath}/img/logos/loader.gif" alt="Loading"/>');
        $('#'+id).css('cursor', 'wait');
        var facesState = getParameterByName('_adf.ctrl-state');
        var jqxhr = $.get( contextPath, { '_adf.ctrl-state': facesState, suggestWord: val }, function(data) {
            who.html('<span class="line"></span>' +  data);
            BPI.UI.onClickOptionSearch();
        })
          .done(function() {
            // alert( "second success" );
          })
          .fail(function() {
                
          })
          .always(function() {
            // alert( "finished" );
            $('#'+id).css('cursor', 'default');
          });
         
        // Perform other work here ...
         
        // Set another completion function for the request above
        jqxhr.always(function() {
          // alert( "second finished" );
        });
}


function getGlyphsLength(s) {
    var glyphs = {"A": 20, "B": 20, "C": 20, "D": 20, "E":18, "F":15, "G":20, "H":21, "I":10, "J":11,
                  "K": 20, "L": 15, "M": 28, "N": 21, "O":20, "P":20, "Q":20, "R":20, "S":20, "T":18,
                  "U": 21, "V": 20, "W": 29, "X": 20, "Y": 20, "Z": 18, "a": 18, "b": 18, "c": 18, "d": 18,
                  "e": 18, "f": 10, "g": 18, "h": 18, "i": 10, "j": 10, "k": 18, "l": 10, "m": 25, "n": 18,
                  "o": 18, "p": 18, "q": 18, "r": 11, "s": 15, "t": 10, "u": 18, "v": 15, "w": 24, "x": 15,
                  "y": 15, "z": 15, "0": 20, "1": 20, "2": 20, "3": 20, "4": 20, "5": 20, "6": 20, "7": 20,
                  "8": 20, "9": 20 }
    
    var avgGlyphsLength = 0;
    var numGlyphs = 0;
    
    // Obter dimensão média
    for (var key in glyphs) {
        if (glyphs.hasOwnProperty(key)) {
            numGlyphs ++;
            avgGlyphsLength+=glyphs[key]
        }
    }
    avgGlyphsLength = Math.floor(avgGlyphsLength / numGlyphs);
    
    var glyphsLength = 0;
    for (var i = 0; i < s.length; i++ ) {
        if (glyphs[s.charAt(i)]!=null)
            glyphsLength += glyphs[s.charAt(i)];
        else
            glyphsLength += avgGlyphsLength; // para caracteres não medidos previamente;
    }
    return glyphsLength;
}

function scaleDownText(elemId) {
    var maxWidthTolerated = 215;
    var assumedFontSize = 36;
    
    var elem = elemId[0];
    var texto = elem.innerHTML;
    var words = texto.split(' ');
    var maxWidthFound = 0;
    for (var i = 0; i < words.length; i++ ) {
        if (getGlyphsLength(words[i]) > maxWidthFound) {
            maxWidthFound = getGlyphsLength(words[i]);
        }
    }
    if (maxWidthFound > maxWidthTolerated) {
        var newFontSize = Math.floor(assumedFontSize * maxWidthTolerated / maxWidthFound);
        elem.style.fontSize=newFontSize.toString()+"px";
    }
}
