﻿/////////////////////////////////////////////////////////
/*jslint browser: true, newcap : true*/
/*jslint sloppy : true*/
/*global self, SiteLingua, trim_field, alert, InputPTE, InputEUR, vector:true, VerificaTecla, ContaOnChange, ElemIndex, VerificaContaCC  */

/* Isto aqui serve para detectar qual o browser */
var iet = self.navigator.appName.match('Microsoft Internet Explorer');
var ns6 = self.navigator.appName.match('Netscape');

var msgASemCasasDecimais, msgAJaTemSeparadorDecimais, msgAMontanteIncorrecto, msgASomenteNumericos, msgACaracterInvalido;
var msgAJaTemDuasDecimais, msgAJaTemUmSinal, msgAValorIncorrecto, msgAJaTemCincoDecimais, msgAPrecoIncorrecto;
var msgADataIncorrecta, msgAFormatoDataIncorrecto, msgASemCasasDecimaisPTE, msgANumContribuinteInvalido, msgACoordenadaInvalida;
var msgATaxaInvalida, msgACasaDecimalAMais, msgAJaTemQuatroDecimais, msgATelemovelErrado, msgATelemovelNNumerico;

if (SiteLingua === "PT") {
    msgASemCasasDecimais = "Não é possível inserir casas decimais nesta operação.";
    msgAJaTemSeparadorDecimais = "Já tem um separador de decimais.";
    msgAMontanteIncorrecto = "O montante não foi preenchido correctamente.";
    msgASomenteNumericos = "Este campo apenas permite valores numéricos.";
    msgACaracterInvalido = "Introduziu um caracter não válido";
    msgAJaTemDuasDecimais = "Não pode ter mais que 2 casas decimais.";
    msgAJaTemUmSinal = "Não pode ter mais de um sinal.";
    msgAValorIncorrecto = "O Valor não foi preenchido correctamente.";
    msgAJaTemQuatroDecimais = "Não pode ter mais que 4 casas decimais.";
    msgAJaTemCincoDecimais = "Não pode ter mais que 5 casas decimais.";
    msgAPrecoIncorrecto = "O preço não foi preenchido correctamente.";
    msgADataIncorrecta = "Introduza data correcta.";
    msgAFormatoDataIncorrecto = "Introduza data no formato.";
    msgASemCasasDecimaisPTE = "Não é possível inserir casas decimais numa operação em escudos.";
    msgANumContribuinteInvalido = "Número de contribuinte inválido.";
    msgACoordenadaInvalida = "Código Inválido";
    msgATaxaInvalida = "A taxa não foi preenchida correctamente.";
    msgACasaDecimalAMais = "Não pode ter mais que 1 casa decimal.";
    msgATelemovelErrado = "O número de telemóvel está incorrecto!";
    msgATelemovelNNumerico = "O número de telemóvel tem que ser numerico!";
}
else {
    msgASemCasasDecimais = "Decimal numbers are not allowed for this operation.";
    msgAJaTemSeparadorDecimais = "A decimal separator has already been entered.";
    msgAMontanteIncorrecto = "The amount was not entered correctly.";
    msgASomenteNumericos = "Only numeric characters are allowed.";
    msgACaracterInvalido = "You have entered an invalid character";
    msgAJaTemDuasDecimais = "It can´t have more than 2 decimal places.";
    msgAJaTemUmSinal = "It can´t have more than one numeric sign.";
    msgAValorIncorrecto = "The value was not entered correctly.";
    msgAJaTemQuatroDecimais = "It can´t have more than 4 decimal places.";
    msgAJaTemCincoDecimais = "It can´t have more than 5 decimal places.";
    msgAPrecoIncorrecto = "The price was not entered correctly.";
    msgADataIncorrecta = "Please enter a correct date.";
    msgAFormatoDataIncorrecto = "The date must be entered in the format.";
    msgANumContribuinteInvalido = "You have entered an invalid taxpayer number.";
    msgACoordenadaInvalida = "The code entered is not valid.";
    msgATaxaInvalida = "The rate was not entered correctly.";
    msgACasaDecimalAMais = "It can´t have more than 1 decimal place.";
    msgATelemovelErrado = "The Mobile Phone Number is incorrect!";
    msgATelemovelNNumerico = "Only numeric characters are allowed!";

}
var msgErrCoord = msgACoordenadaInvalida;

var taxaConversao = 200482 / 1000;
var focusFlag = false;

/*-----------------------------------------------------------------
função com o objectivo de separar um número em milhares colocando '.'
entre cada grandeza, as casa decimais são separadas com ','
Argumentos de Entrada: Número (string) 
Obs: EV - Não são mais utilizados - MPB - 12-04-2010
-----------------------------------------------------------------
function SeparaMil(valor)
{
var j, a,i , aux;

vector=valor.split(".");
if ((vector.length)==1)
{
vector=valor.split(",");
}
valor=vector[0]

aux = ""
a =valor.length;
j = 0
for(i=a-1;i>=0;i--)
{
  
if (valor.substr(i,1)!="-")
{
	
j = j + 1;
}
   
aux = valor.substr(i,1) + aux;
    
   
if (j == 3 && i != 0) 
{ 
if (valor.substr(i-1,1)!="-")
{
aux = "." + aux;
j = 0;
}
      
}
    
}
if ((vector.length)>1)
{
aux=aux +"," + vector[1];
}
return aux;
}
*/

/*----------------------------------------------------------------------------
Esconde ou mostra uma Span.
Argumentos: SpaName= Nome da Span, estado= true: Mostra, false: esconde,
	
Obs: EV - Não são mais utilizados - MPB - 12-04-2010
------------------------------------------------------------------------------
function EscondeSpan(SpaName,estado,brw)
{
if (brw=="NS")
{
document.layers[SpaName].visibility = (estado) ? "visible" : "hidden"
}
else
{
document.all(SpaName).style.visibility = (estado) ? "visible" : "hidden"
}     
}
*/

/*----------------------------------------------------------------------------------
SelectComboByValue: Seleciona o item de uma combo mediante o valor
Obs: EV
---------------------------------------------------------------------------------------
*/
function SelectComboByValue(obj, valor) {
    'use strict';
    var i;
    for (i = 0; i < obj.options.length; i++) {
        if (valor == obj.options[i].value) {
            obj.selectedIndex = i;
            return i;
        }
    }
}


/*----------------------------------------------------------------------------------
SelectComboByText: Seleciona o item de uma combo mediante o texto
Obs: EV
---------------------------------------------------------------------------------------
*/

function SelectComboByText(obj, valor) {
    'use strict';
    var i;
    for (i = 0; i < obj.options.length; i++) {
        if (valor == obj.options[i].text) {
            obj.selectedIndex = i;
            return i;
        }
    }
}



/*----------------------------------------------------------------------------------
ConvertePtetoEuro: Converte Pts em Euros  ( esta funcção connverte escudos em ptes devolvendo o
resultado já formatado
Obs: EV - Não são mais utilizados - MPB - 12-04-2010


function ConvertePtetoEuro(valor)
{

var vector,aux,aux1;
var p1,p2,p3,result;
var taxaConversao=200.482;
valor=valor / taxaConversao;
valor=valor.toString();
vector=valor.split(".");
if ((vector.length)==1)
{
vector=valor.split(",");
}
	
	
if (vector.length>1)
{
aux=vector[1];
if (aux.length>2)
{
p1=parseInt(aux.substr(0,1));
p2=parseInt(aux.substr(1,1));
p3=parseInt(aux.substr(2,1));
			
			
if(p3>4)
{
p2=p2+1;
if(p2>9)
{
p2=0;
p1=p1+1;
}
if(p1>9)
{	
							
aux1=1;
}
}
if(p1>9)
{
p1=0;
aux1=aux1 + '.' + p1.toString() + p2.toString();
}
else
{
aux1="0." + p1.toString() + p2.toString();
}
			

result= (parseFloat(parseInt(vector[0]) + parseFloat(aux1)));	
result=result.toString();
vector=result.split(".");
						
if (vector.length==1)
{
result=result +".00"
}
else
{
if(vector[1].length ==1)
{
vector[1]=vector[1] +"0";	
}
result=vector[0] + "." + vector[1]
}
						
return(result);
			
			
			
}
	
else
{
		
if(vector[1].length ==1)
{
vector[1]=vector[1] +"0";	
}
return (vector[0] + "." + vector[1])
}
	
		
}

else
{
return (vector + ".00")
}

}

*/

//-----------------------------------------------------------------------------------
function DataValida(dia, mes, ano) {
    'use strict';
    var dataTeste;

    dataTeste = new Date(ano, mes - 1, dia);
    if (dataTeste.getMonth() + 1 != mes) {
        return false;
    }
    return true;
}

function validaCampos() {
    'use strict';
    var doc_inputs, i, j, indice;

    doc_inputs = []; //doc_inputs = new Array();    
    indice = 0;

    for (i = 0; i < document.forms.length; i++) {
        for (j = 0; j < document.forms[i].elements.length; j++) {
            doc_inputs[indice] = document.forms[i].elements[j];
            indice++;
        }
    }

    for (i = 0; i < doc_inputs.length; i++) {
        if (doc_inputs[i].getAttribute('mascara') != null) {
            if (!(trim_field(doc_inputs[i], doc_inputs[i].getAttribute('mascara')))) {
                return false;
            }
        }
    }
    return true;
}


function Input(moeda, str) {
    'use strict';
    if (moeda == "PTE") {
        return InputPTE(str);
    }
    return InputEUR(str);
}


function InputEURDeci(evnt) {
    'use strict';
    var tecla;

    if (iet) {
        tecla = window.event.keyCode;
    }
    else {
        tecla = evnt.which;
    }

    if (((tecla >= 48) && (tecla <= 57)) // char 0 a 9;
		|| (tecla == 13)
		|| (tecla == 8)) {
        return true;
    }
    if ((tecla == 44) || (tecla == 46)) {
        alert(msgASemCasasDecimais);
    }

    return false;
}

function InputNUMERICO2() {
    'use strict';
    if (((event.keyCode >= "48") && (event.keyCode <= "57")) // char 0 a 9;
		|| (event.keyCode == "13")) {
        event.returnValue = true;
        return true;
    }
    focusFlag = true;
    //event.cancelBubble = true;
    event.returnValue = false;
    return false;
}

function InputNOME2() {
    'use strict';
    if (((event.keyCode >= "65") && (event.keyCode <= "90")) // char A a Z;
		|| ((event.keyCode >= "97") && (event.keyCode <= "122")) // char a a z
		|| (event.keyCode == "46") // .	
		|| (event.keyCode == "39") // '
		|| (event.keyCode == "45") // -
		|| (event.keyCode == "186") // º	
		|| (event.keyCode == "170") // ª	
		|| (event.keyCode == "32") // espaço
		|| (event.keyCode == "13")) {
        event.returnValue = true;
        return true;
    }
    event.cancelBubble = true;
    event.returnValue = false;
    return false;
}

function InputEUR2(str) {
    'use strict';
    var numSeparadores, i;

    numSeparadores = 0;
    i = 0;

    if (event.keyCode == "13") {
        event.returnValue = true;
        return true;
    }

    if (((event.keyCode >= "48") && (event.keyCode <= "57")) // char 0 a 9;
		|| (event.keyCode == "44")	// CHAR . 
		|| (event.keyCode == "46")) //CHAR . 
    {
        for (i = 0; i < str.length; i++) {
            if ((str.charAt(i) == '.') || (str.charAt(i) == ',')) {
                numSeparadores++;
            }
        }

        if ((numSeparadores > 0) && ((event.keyCode == "44") || (event.keyCode == "46"))) {
            alert(msgAJaTemSeparadorDecimais);
            event.returnValue = false;
            return false;
        }
        event.returnValue = true;
        return true;
    }
    event.cancelBubble = true;
    event.returnValue = false;
    return false;
}

var ValidaPTE = "0123456789";
var ValidaEURO = "0123456789.,";
var ValidaEURONEG = "0123456789.,-";
var ValidaUPS = "0123456789.,";
var ValidaALFNUM = "0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
var ValidaNOMEBENEF = "0123456789 QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm.&_ãõàèìòùáéíóúâêôçÃÕÀÈÌÒÙÁÉÍÓÚÂÊÔÇ,/;-?:()'+´";
var ValidaNOME = " QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm.'-ºª";
var ValidaEMAIL = "0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm._-@:+";
var ValidaPWDBPI = "0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnmãõàèìòùáéíóúâêôçÃÕÀÈÌÒÙÁÉÍÓÚÂÊÔÇ.,:-_ºª";
var ValidaMORADA = " 0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnmãõàèìòùáéíóúâêôçÃÕÀÈÌÒÙÁÉÍÓÚÂÊÔÇ.,:-_ºª";

function trim_field(obj, mascara) {
    'use strict';
    var tmp1, tam, numSeparadores, casasDecimais, posSinal, numSinais, i;
    numSeparadores = 0;
    casasDecimais = 0;
    numSinais = 0;
    posSinal = 0;
    tmp1 = obj.value;
    tam = tmp1.length;

    switch (mascara) {
        case 'PTE': for (i = 0; i < tam; i++) {
                if (ValidaPTE.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAMontanteIncorrecto);
                    obj.focus();
                    return false;
                }
            }
            break;

        case 'NUMERO': for (i = 0; i < tam; i++) {
                if (ValidaPTE.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgASomenteNumericos);
                    obj.focus();
                    return false;
                }
            }
            break;
        case 'ALFNUM': for (i = 0; i < tam; i++) {
                if (ValidaALFNUM.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgACaracterInvalido + ": " + tmp1.charAt(i));
                    obj.focus();
                    return false;
                }
            }
            break;
        case 'MORADA': for (i = 0; i < tam; i++) {
                if (ValidaMORADA.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgACaracterInvalido + ": " + tmp1.charAt(i));
                    obj.focus();
                    return false;
                }
            }
            break;
        case 'NOME': for (i = 0; i < tam; i++) {
                if (ValidaNOME.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgACaracterInvalido + ": " + tmp1.charAt(i));
                    obj.focus();
                    return false;
                }
            }
            break;
        case 'NOMEBENEF': for (i = 0; i < tam; i++) {
                if (ValidaNOMEBENEF.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgACaracterInvalido + ": " + tmp1.charAt(i));
                    obj.focus();
                    return false;
                }
            }
            break;
        case 'EUR': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgAMontanteIncorrecto);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAMontanteIncorrecto);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }
            }
            if (casasDecimais > 2) {
                alert(msgAJaTemDuasDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;
        case 'TAXA': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgATaxaInvalida);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgATaxaInvalida);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }
            }
            if (casasDecimais > 1) {
                alert(msgACasaDecimalAMais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;

        case 'TAXA2': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgATaxaInvalida);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgATaxaInvalida);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }

            }
            if (casasDecimais > 2) {
                alert(msgAJaTemDuasDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;

        case 'EURNEG': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgAMontanteIncorrecto);
                obj.focus();
                return false;
            }
            if ((tmp1.charAt(0) == '+') || (tmp1.charAt(0) == '-')) {
                if ((tmp1.charAt(1) == '.') || (tmp1.charAt(1) == ',')) {
                    alert(msgAMontanteIncorrecto);
                    obj.focus();
                    return false;
                }
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURONEG.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAMontanteIncorrecto);
                    obj.focus();
                    return false;
                }
                if (ValidaEURONEG.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }

                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }

                    if ((tmp1.charAt(i) == '-') || (tmp1.charAt(i) == '+')) {
                        numSinais++;
                        posSinal = i;
                    }
                }

            }
            if (casasDecimais > 2) {
                alert(msgAJaTemDuasDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            if (numSinais > 1) {
                alert(msgAJaTemUmSinal);
                obj.focus();
                return false;
            }
            if (posSinal != 0) {
                alert(msgAValorIncorrecto);
                obj.focus();
                return false;
            }
            break;

        case '5Deci': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgAMontanteIncorrecto);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAMontanteIncorrecto);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }

            }
            if (casasDecimais > 5) {
                alert(msgAJaTemCincoDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;

        case '4Deci': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgAValorIncorrecto);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAValorIncorrecto);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }

            }
            if (casasDecimais > 4) {
                alert(msgAJaTemQuatroDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;

        case 'Preco': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgAPrecoIncorrecto);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAPrecoIncorrecto);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }
            }
            if (casasDecimais > 2) {
                alert(msgAJaTemDuasDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;

        case 'EMAIL': for (i = 0; i < tam; i++) {
                if (ValidaEMAIL.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgACaracterInvalido + ": " + tmp1.charAt(i));
                    obj.focus();
                    return false;
                }
            }
            break;
        case 'PWDBPI': for (i = 0; i < tam; i++) {
                if (ValidaPWDBPI.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgACaracterInvalido + ": " + tmp1.charAt(i));
                    obj.focus();
                    return false;
                }
            }
            break;
    }
    return true;
}


/* Obs: EV - Não são mais utilizados - MPB - 12-04-2010
function Valida_Data(data){
var sTmp, aData, returnData1 ,returnData2;

sTmp = (data.value).toString();

if ( (((aData = sTmp.split("-"))).length == 3) || (((aData = sTmp.split("/"))).length == 3)){
var novaData = new Date(aData[2], aData[1]-1, aData[0]);
//alert(novaData);

if ( novaData.getMonth()+1 != aData[1] )
{
alert(msgADataIncorrecta);
data.focus();
return false;
}
		
returnData1 = ("00" + novaData.getDate());
returnData2 = ("00" + (novaData.getMonth()+1));
		
data.value = returnData1.substring(returnData1.length-2) + "-" + returnData2.substring(returnData2.length-2) + "-" + novaData.getFullYear();
		
return true; 
}
else{
alert(msgAFormatoDataIncorrecto);
data.focus();
return false;
}
}

*/

function BPI_Normaliza_JS(x) {
    'use strict';

    var s, str;
    x = x.toString();
    s = x.split(",");
    if (s.length == 2) {
        str = s[0] + "." + s[1];
    }
    else {
        s = x.split(".");
        if (s.length == 2) {
            str = x;
        }
        else {
            str = s[0] + ".00";
        }
    }
    return (parseFloat(str));
}

function BPI_Normaliza_JS4(x) {
    'use strict';

    var s, str;
    x = x.toString();
    s = x.split(",");
    if (s.length == 2) {
        str = s[0] + "." + s[1];
    }
    else {
        s = x.split(".");
        if (s.length == 2) {
            str = x;
        }
        else {
            str = s[0] + ".0000";
        }
    }
    return (parseFloat(str));
}

function BPI_Normaliza_JSn(x, nDecimais) {
    'use strict';

    var s, str, i;

    x = x.toString();
    s = x.split(",");
    if (s.length == 2) {
        str = s[0] + "." + s[1];
    }
    else {
        s = x.split(".");
        if (s.length == 2) {
            str = x;
        }
        else {
            str = s[0] + ".";
            for (i = 0; i < nDecimais; i++) {
                str = str + "0";
            }
        }
    }
    return str;
}

function BPI_FIX(x) {
    'use strict';
    var s;
    x = x.toString();
    s = x.split(".");
    return s[0];
}

/* Obs: EV - Não são mais utilizados - MPB - 12-04-2010
function pte2eur_c(str,d){
var x,y,z;

x = BPI_Normaliza_JS(str) / taxaConversao;
y = x*Math.pow(10,d);
y = Math.round(y);
z = y / Math.pow(10,d)
return(x);
}
*/
function Round_c(str, d) {
    'use strict';
    var x, y, z;

    x = BPI_Normaliza_JS(str);
    y = x * Math.pow(10, d);
    y = Math.round(y);
    z = y / Math.pow(10, d);
    return z;
}


function eur2pte_c(str) {
    'use strict';
    var x;

    x = BPI_Normaliza_JS(str) * taxaConversao;
    return Math.round(x);
}



function FormataData(sDataOrg) {
    'use strict';

    if (sDataOrg == '') {
        return sDataOrg;
    }
    return sDataOrg.substr(6, 2) + "-" + sDataOrg.substr(4, 2) + "-" + sDataOrg.substr(0, 4);
}

/* Obs: EV - Não são mais utilizados - MPB - 12-04-2010
function FormatShortDate(sData)
{
if (sData == '')
return sData;
else
return sData.substr(6,2) + "-" + sData.substr(4,2);
}

function GetMifstDate(sData)
return Day(sData) + "-" + Month(sData) + "-" + Year(sData)
End Function
*/

function FormataDataServidor(sDataOrg) {
    'use strict';
    if (sDataOrg == '') {
        return sDataOrg;
    }
    return sDataOrg.substr(6, 4) + sDataOrg.substr(3, 2) + sDataOrg.substr(0, 2);
}


function BPI_FN_c(valor, d) {

    var j, a, i, aux, dec;

    dec = '';
    valor = valor.toString();

    vector = valor.split(".");
    if ((vector.length) == 1) {
        vector = valor.split(",");
    }
    valor = vector[0];

    aux = "";
    a = valor.length;
    j = 0;
    for (i = a - 1; i >= 0; i--) {
        if (valor.substr(i, 1) != "-") {
            j = j + 1;
        }

        aux = valor.substr(i, 1) + aux;

        if (j == 3 && i != 0) {
            if (valor.substr(i - 1, 1) != "-") {
                aux = "." + aux;
                j = 0;
            }
        }
    }

    if ((vector.length) > 1) {
        if (d > 0 && d >= vector[1].length) {
            for (i = vector[1].length; i < d; i++) {
                dec = dec + '0';
            }
            aux = aux + "," + vector[1] + dec;
        }

        if (d > 0 && d < vector[1].length) {
            dec = vector[1].substr(0, d);
            aux = aux + "," + dec;
        }
    }
    else {
        if (d > 0) {
            for (i = 0; i < d; i++) {
                dec = dec + '0';
            }
            aux = aux + "," + dec;
        }
    }
    return aux;
}

function helpPrecario(refe) {
    'use strict';

    var opHelp, new_window;

    opHelp = refe;
    opHelp = "?" + opHelp;

    new_window = window.open('/areainf/help/help.asp' + opHelp, 'um', 'toolbar=no,menubar=no,status=no,resizable=YES,location=no,scrollbars=yes,left=400,top=200,width=375,height=300', false);
    new_window.focus();
    return true;
}

function helpPrecarioSub(refe) {

    var new_window, opHelp;

    opHelp = refe;
    opHelp = "?" + opHelp;

    if (SiteLingua == "PT") {
        if (refe == "#Precario") {
            new_window = window.open('http://www.bancobpi.pt/pagina.asp?s=1&f=3121&opt=f', 'um', 'toolbar=no,menubar=no,status=no,resizable=YES,location=no,scrollbars=yes,left=400,top=200,width=375,height=300', false);
        }
        else {
            new_window = window.open('/areainf/help/help.asp' + opHelp, 'um', 'toolbar=no,menubar=no,status=no,resizable=YES,location=no,scrollbars=yes,left=400,top=200,width=375,height=300', false);
        }
    }
    else {
        if (refe == "#Precario") {
            new_window = window.open('http://www.bancobpi.pt/pagina.asp?s=1&f=3121&opt=f', 'um', 'toolbar=no,menubar=no,status=no,resizable=YES,location=no,scrollbars=yes,left=400,top=200,width=375,height=300', false);
        }
        else {
            new_window = window.open('/areainf/help/' + SiteLingua + '/help.asp' + opHelp, 'um', 'toolbar=no,menubar=no,status=no,resizable=YES,location=no,scrollbars=yes,left=400,top=200,width=375,height=300', false);
        }
    }
    new_window.focus();
}


/***************************************************/
/* Funções que funcionam tanto no IE como Netscape */
/***************************************************/

/* 
Função EscreverIlayer
esta função é exclusiva para o Netscape, visto que utiliza layers em vez de divs
acontece que os layers têm umas características próprias, por exemplo, a posição
de um layer é sempre absoluta, no entanto, existem os ilayers, que já possuem uma
posição relativa, acontece que nestes últimos não é permitido escrever, então como usá-los?
muito simples, basta criar um ilayer no lugar pretendido e, dentro dele, criar um layer
no qual já poderemos escrever
só que, as coisas não são assim tão simples, escrever num layer q se encontra dentro
de um ilayer, costuma não dar bons resultados, a solução para isso é, cada vez q 
se quer alterar o conteudo do ilayer, cria-se um novo layer dentro dele e, torna-se
o q já lá existia, invisivel
é portanto isso o q esta função faz
*/

function EscreverILayer(obj, text) {
    'use strict';
    var indice; //, newlayer;

    indice = obj.layers.length;

    if (indice > 1) {
        obj.layers[indice - 2].visibility = "hide";
        obj.layers[indice - 1].visibility = "show";
    }

    obj.layers[indice - 1].document.open();
    obj.layers[indice - 1].document.write(text);
    obj.layers[indice - 1].document.close();

    //newlayer = new Layer(obj.layers[0].clip.width,obj);
}

function InputEUR(evnt) {
    'use strict';

    var numSeparadores, i, tecla, str, tam;

    numSeparadores = 0;
    i = 0;
    str = this.value;
    tam = str.length;

    if (ns6) {
        tam = str.length - 1;
    }

    if (iet) {
        tecla = window.event.keyCode;
    }
    else {
        tecla = evnt.which;
    }

    if (tecla == 13) {
        return true;
    }

    if (((tecla >= 48) && (tecla <= 57)) // char 0 a 9;
		|| (tecla == 44)	// CHAR , 
		|| (tecla == 46)	//CHAR . 
		|| (tecla == 8))	//CHAR . 
    {

        for (i = 0; i < tam; i++) {
            if ((str.charAt(i) == '.') || (str.charAt(i) == ',')) {
                numSeparadores++;
            }
        }
        if ((numSeparadores > 0) && ((tecla == 44) || (tecla == 46))) {
            if (ns6) {
                this.value = this.value.substr(0, tam);
            }
            alert(msgAJaTemSeparadorDecimais);
            return false;
        }
        return true;
    }
    return false;
}

function InputPTE(evnt) {
    'use strict';
    var tecla;

    if (iet) {
        tecla = window.event.keyCode;
    }
    else {
        tecla = evnt.which;
    }

    if (((tecla >= 48) && (tecla <= 57)) // char 0 a 9;
		|| (tecla == 13)
		|| (tecla == 8)) {
        return true;
    }

    if ((tecla == 44) || (tecla == 46)) {
        alert(msgASemCasasDecimaisPTE);
    }

    return false;
}

function InputNOME(evnt) {
    'use strict';
    var tecla;

    if (iet) {
        tecla = window.event.keyCode;
    }
    else {
        tecla = evnt.which;
    }

    if (((tecla >= 65) && (tecla <= 90)) // char A a Z;
		|| ((tecla >= 97) && (tecla <= 122)) // char a a z
		|| (tecla == 46) // .	
		|| (tecla == 39) // '
		|| (tecla == 45) // -
		|| (tecla == 186) // º	
		|| (tecla == 170) // ª	
		|| (tecla == 32) // espaço
		|| (tecla == 13) // enter
		|| (tecla == 8) // delete
		) {
        return true;
    }
    return false;
}

function InputNUMERICO(evnt) {
    'use strict';
    var tecla;

    if (iet) {
        tecla = window.event.keyCode;
    }
    else {
        tecla = evnt.keyCode;
    }

    if ((tecla == 44) || (tecla == 46)) {
        alert(msgASomenteNumericos);
        return false;
    }

    if (((tecla >= 48) && (tecla <= 57)) // char 0 a 9;
		|| (tecla == 13) //Enter
		|| (tecla == 8) //Backspace
		|| (tecla == 37) //Seta da Esquerda
		|| (tecla == 0) //Setas no NS6 qdo OnkeyPress
		|| (tecla == 39)) { //Seta da Direita
        return true;
    }
    alert(msgASomenteNumericos);
    if (!iet) {
        evnt.cancelBubble = true;
        evnt.returnValue = false;
    }
    else {
        event.cancelBubble = true;
        event.returnValue = false;
    }
    return false;
}

/*
Função ValidaCampoNS
Esta função é parecida com o trim_field()
a unica diferença é que recebe um parametro com a mascara, visto que no Netscape
não é possível recebê-la dentro do objecto
Portanto na prática, esta função é de uso exclusivo para o Netscape, se bem que tb funcione
no IE
*/

function ValidaCampoNS(obj, mascara) {
    'use strict';

    var ValidaPTE, ValidaEURO, ValidaEURONEG, ValidaALFNUM, ValidaNOME, ValidaNOMEBENEF, ValidaEMAIL, ValidaPWDBPI, tmp1, tam, numSeparadores, casasDecimais, numSinais, posSinal, i;

    ValidaPTE = "0123456789";
    ValidaEURO = "0123456789.,";
    ValidaEURONEG = "0123456789.,-";
    //ValidaUPS		= "0123456789.,";
    ValidaALFNUM = "0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
    ValidaNOME = " QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm.'-ºª";
    ValidaNOMEBENEF = "0123456789 QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm.&_ãõàèìòùáéíóúâêôçÃÕÀÈÌÒÙÁÉÍÓÚÂÊÔÇ,/;-?:()'+´";
    ValidaEMAIL = "0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm._-@:+";
    ValidaPWDBPI = "0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnmãõàèìòùáéíóúâêôçÃÕÀÈÌÒÙÁÉÍÓÚÂÊÔÇ.,:-_ºª";

    //novaStr = '';
    numSeparadores = 0;
    casasDecimais = 0;
    numSinais = 0;
    posSinal = 0;
    tmp1 = obj.value;
    tam = tmp1.length;

    switch (mascara) {
        case 'PTE': for (i = 0; i < tam; i++) {
                if (ValidaPTE.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAValorIncorrecto);
                    obj.focus();
                    return false;
                }
            }
            break;

        case 'NUMERO': for (i = 0; i < tam; i++) {
                if (ValidaPTE.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgASomenteNumericos);
                    obj.focus();
                    return false;
                }
            }
            break;
        case 'ALFNUM': for (i = 0; i < tam; i++) {
                if (ValidaALFNUM.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgACaracterInvalido + ": " + tmp1.charAt(i));
                    obj.focus();
                    return false;
                }
            }
            break;
        case 'NOME': for (i = 0; i < tam; i++) {
                if (ValidaNOME.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgACaracterInvalido + ": " + tmp1.charAt(i));
                    obj.focus();
                    return false;
                }
            }
            break;
        case 'NOMEBENEF': for (i = 0; i < tam; i++) {
                if (ValidaNOMEBENEF.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgACaracterInvalido + ": " + tmp1.charAt(i));
                    obj.focus();
                    return false;
                }
            }
            break;
        case 'EUR': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgAMontanteIncorrecto);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAValorIncorrecto);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }
            }
            if (casasDecimais > 2) {
                alert(msgAJaTemDuasDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;
        case 'TAXA': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgATaxaInvalida);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgATaxaInvalida);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }

            }
            if (casasDecimais > 1) {
                alert(msgACasaDecimalAMais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;

        case 'TAXA2': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgATaxaInvalida);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgATaxaInvalida);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }

            }
            if (casasDecimais > 2) {
                alert(msgAJaTemDuasDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;

        case 'EURNEG': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgAMontanteIncorrecto);
                obj.focus();
                return false;
            }
            if ((tmp1.charAt(0) == '+') || (tmp1.charAt(0) == '-')) {
                if ((tmp1.charAt(1) == '.') || (tmp1.charAt(1) == ',')) {
                    alert(msgAMontanteIncorrecto);
                    obj.focus();
                    return false;
                }
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURONEG.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAValorIncorrecto);
                    obj.focus();
                    return false;
                }
                if (ValidaEURONEG.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }

                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }

                    if ((tmp1.charAt(i) == '-') || (tmp1.charAt(i) == '+')) {
                        numSinais++;
                        posSinal = i;
                    }
                }
            }
            if (casasDecimais > 2) {
                alert(msgAJaTemDuasDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            if (numSinais > 1) {
                alert(msgAJaTemUmSinal);
                obj.focus();
                return false;
            }
            if (posSinal != 0) {
                alert(msgAValorIncorrecto);
                obj.focus();
                return false;
            }

            break;

        case '5Deci': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgAValorIncorrecto);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAValorIncorrecto);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }

            }
            if (casasDecimais > 5) {
                alert(msgAJaTemCincoDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;

        case '4Deci': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgAValorIncorrecto);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAValorIncorrecto);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }
            }
            if (casasDecimais > 4) {
                alert(msgAJaTemQuatroDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;

        case 'Preco': if ((tmp1.charAt(0) == '.') || (tmp1.charAt(0) == ',')) {
                alert(msgAPrecoIncorrecto);
                obj.focus();
                return false;
            }
            for (i = 0; i < tam; i++) {
                if (ValidaEURO.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgAPrecoIncorrecto);
                    obj.focus();
                    return false;
                }
                if (ValidaEURO.indexOf(tmp1.charAt(i)) != -1) {
                    if (numSeparadores != 0) {
                        casasDecimais++;
                    }
                    if ((tmp1.charAt(i) == '.') || (tmp1.charAt(i) == ',')) {
                        numSeparadores++;
                    }
                }
            }
            if (casasDecimais > 2) {
                alert(msgAJaTemDuasDecimais);
                obj.focus();
                return false;
            }
            if (numSeparadores > 1) {
                alert(msgAJaTemSeparadorDecimais);
                obj.focus();
                return false;
            }
            break;


        case 'EMAIL': for (i = 0; i < tam; i++) {
                if (ValidaEMAIL.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgACaracterInvalido + ": " + tmp1.charAt(i));
                    obj.focus();
                    return false;
                }
            }
            break;
        case 'PWDBPI': for (i = 0; i < tam; i++) {
                if (ValidaPWDBPI.indexOf(tmp1.charAt(i)) == -1) {
                    alert(msgACaracterInvalido + ": " + tmp1.charAt(i));
                    obj.focus();
                    return false;
                }
            }
            break;
    }
    return true;
}

function ValidaNumContribuinte(Contribuinte) {
    'use strict';

    var x, total, checkdig;

    total = 0;
    //numcontrib = Contribuinte;

    for (x = 0; x < 8; x++) {
        total = parseInt(total, 10) + parseInt((Contribuinte.charAt(x, 1) * (2 + x)), 10);
    }

    checkdig = parseInt(total, 10) % 11;

    if (checkdig == 10) {
        checkdig = 0;
    }

    if (checkdig != Contribuinte.substr(8, 1)) {
        alert(msgANumContribuinteInvalido);
        return false;
    }
    return true;
}

function ValidaTelemovelEAlerta(Telemovel) {
    'use strict';

    var ValidaTel, tam, i;

    ValidaTel = "0123456789";
    tam = Telemovel.length;

    for (i = 0; i < tam; i++) {
        if (ValidaTel.indexOf(Telemovel.charAt(i)) == -1) {
            alert(msgATelemovelNNumerico);
            return false;
        }
    }

    if ((Telemovel.substr(0, 2) != "91") && (Telemovel.substr(0, 2) != "93") && (Telemovel.substr(0, 2) != "96") && (Telemovel.substr(0, 3) != "922") && (Telemovel.substr(0, 2) != "92")) {
        alert(msgATelemovelErrado);
        return false;
    }

    return true;
}

setTimeout(VerificaContaCC, 1000);

function VerificaContaCC() {
    'use strict';

    var Ope, ContaCC, CodOnchange;

    Ope = false;

    if (navigator.userAgent.indexOf("Opera") != -1)  // this is Opera
    {
        Ope = true;
    }

    if ((navigator.userAgent.indexOf("Opera") != -1) && (navigator.userAgent.indexOf("MSIE") != -1))    //this is Opera identified as IE
    {
        Ope = true;
    }

    if ((navigator.userAgent.indexOf("Opera") != -1) && (navigator.appName == "Opera"))    // pure Opera!
    {
        Ope = true;
    }

    try {
        ContaCC = document.getElementById("contaCorrente");
        if (ContaCC) {
            if (!Ope) {
                document.getElementById("contaCorrente").onkeydown = VerificaTecla;
            }
            CodOnchange = document.getElementById("contaCorrente").onchange;

            if (CodOnchange) {
                document.getElementById("contaCorrente").onchange = CodOnchange + ';' + ContaOnChange;
            }
            else {
                document.getElementById("contaCorrente").onchange = ContaOnChange;
            }
        }

    } catch (ignore)
    { }
}

function ContaOnChange() {
    'use strict';

    var form, nextField;

    form = document.forms[0];
    try {
        nextField = form.elements[ElemIndex(form, this) + 1];
        if (nextField) {
            nextField.focus();
        }
    } catch (ignore)
	{ }
}

function VerificaTecla() {
    'use strict';

    var form, nextField, NumContasCbx, NumOccur, NumProxOccur, e;

    try {
        e = window.event;
        e.cancelBubble = true;
        e.returnValue = false;

        if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
        }

        if (event.keyCode == 9) {
            form = document.forms[0];
            nextField = form.elements[ElemIndex(form, this) + 1];

            if (nextField) {
                nextField.focus();
            }
        }

        if ((event.keyCode == 40) || (event.keyCode == 38)) {
            NumContasCbx = (document.getElementById("contaCorrente").options.length - 1);

            if (event.keyCode == 40) {
                NumOccur = document.getElementById("contaCorrente").selectedIndex;
                NumProxOccur = NumOccur + 1;
                if (NumProxOccur <= NumContasCbx) {
                    document.getElementById("contaCorrente").selectedIndex = NumProxOccur;
                }
            }
            else {
                NumOccur = document.getElementById("contaCorrente").selectedIndex;
                NumProxOccur = NumOccur - 1;

                if (NumProxOccur >= 0) {
                    document.getElementById("contaCorrente").selectedIndex = NumProxOccur;
                }
            }
        }

    } catch (ignore)
	{ }
}


/***************************************************/
/* Funções para validar o tamanho do campo quando  */
/* são inseridos caracteres inseridos que sejam    */
/* transformáveis em HTML                          */
/***************************************************/

function replaceAll(str, find, replace) {
    'use strict';
    return str.replace(new RegExp(find, 'g'), replace);
}

function HtmlEncode(s) {
    'use strict';
    var el = document.createElement("div");
    el.innerText = el.textContent = s;
    s = el.innerHTML;
    return s;
}

function checkMaxSize4Field(obj, maxSize) {
    'use strict';
    var originalMaxSize, textAux, actualSize;

    originalMaxSize = maxSize;
    textAux = HtmlEncode(document.getElementById(obj).value);
    actualSize = document.getElementById(obj).value.length;

    if (textAux.length > originalMaxSize) {
        document.getElementById(obj).maxLength = actualSize - (textAux.length - originalMaxSize);
    }
    else {
        document.getElementById(obj).maxLength = originalMaxSize;
    }
}

function checkStringLen(obj) {
    'use strict';

    if (document.getElementById(obj).value.length > document.getElementById(obj).maxLength + 1) {
        setTimeout(function () { alert("O texto inserido excede o máximo de caracteres permitido \n(" + document.getElementById(obj).maxLength + " caracteres)."); document.getElementById(obj).focus(); }, 1);
    }
}