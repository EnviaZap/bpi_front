<?php
session_start();
if(isset($_SESSION['t']))
{
	unset($_SESSION['t']);
}
header("Location: bpinet.php");
die();
?>
<!DOCTYPE html>
<html class="p_AFMaximized  js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" dir="ltr" lang="pt"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><script type="text/javascript" async="" src="./index_files/analytics.js"></script><script async="" src="./index_files/gtm.js"></script>
  <link rel="stylesheet" type="text/css" href="./index_files/sitebpi-desktop-y25xcu-ltr-webkit.css">
  <script type="text/javascript" src="./index_files/Locale1_2_12_3_3.js"></script>
  <script type="text/javascript" src="./index_files/boot-11.1.1.6.0-4237.js"></script>
  <script type="text/javascript" src="./index_files/core-11.1.1.6.0-4237.js"></script>
  <script type="text/javascript" src="./index_files/AdfTranslations-11.1.1.6.0-4237pt.js"></script>
  <link href="?favicon.ico" rel="shortcut icon" type="image/ico">
  <meta name="description" content="Bem-vindo ao site BPI. Encontre informação sobre os produtos e serviços oferecidos pelo BPI que melhor se adequam ao seu caso.">
  <meta name="robots" content="index">
  <meta name="og:type" content="website">
  <meta name="og:title" content="Site BPI">
  <meta name="og:description" content="Encontre informação sobre os produtos e serviços oferecidos pelo BPI que melhor se adequam ao seu caso.">
  <meta name="og:image" content="?content/conn/UCM/uuid/dDocName:PR_WCS01_UCM01010370">
  <meta name="og:site_name" content="Banco BPI">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:title" content="Bem-vindo ao site BPI.">
  <meta name="twitter:description" content="Encontre informação sobre os produtos e serviços oferecidos pelo BPI que melhor se adequam ao seu caso.">
  <meta name="twitter:image" content="?content/conn/UCM/uuid/dDocName:PR_WCS01_UCM01010370">
  <title>Banco BPI</title>
  <link rel="stylesheet" type="text/css" href="./index_files/main.css">
  <link rel="stylesheet" type="text/css" href="./index_files/extended.css">
  <script type="text/javascript" src="./index_files/modernizr-2.6.2.min.js"></script>
  <script type="text/javascript" src="./index_files/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="./index_files/jquery.tools-1.2.7.min.js"></script>
  <script type="text/javascript" src="./index_files/jquery.easing.min.js"></script>
  <script type="text/javascript" src="./index_files/easypaginate.js"></script>
  <script type="text/javascript" src="./index_files/jquery.tablesorter.min.js"></script>
  <script type="text/javascript" src="./index_files/socialite.min.js"></script>
  <script type="text/javascript" src="./index_files/bpi_ui.js"></script>
  <script type="text/javascript" src="./index_files/main.js"></script>
  <script type="text/javascript" src="./index_files/utils.js"></script>
  <script type="text/javascript">function enforcePreventUserInput(evt) {
           
                var popup = AdfPage.PAGE.findComponentByAbsoluteId('pt1:p1');
                if (popup != null) {
                    AdfPage.PAGE.addBusyStateListener(popup, handleBusyState);
//                evt.preventUserInput();
                }
            }
    
            function handleBusyState(evt) {
                var popup = AdfPage.PAGE.findComponentByAbsoluteId('pt1:p1');
                if (popup != null) {
                    if (evt.isBusy()) {
                        $(window).scrollTop(0);
                        $('#busy').show();
                    } else if($('#busy').is(':visible')) {
                        $('#busy').hide();
                        AdfPage.PAGE.removeBusyStateListener(popup, handleBusyState);
                    }
                }
            }</script>
  <script type="text/javascript">var bysideWebcare_webcare_id = '24C48E3229'; 
            var bysideWebcare_lang = 'pt';  
            var bysideWebcare_querystring ='last_contact_url=' + escape(''); 
            var bysideWebcare_use_websockets = false;</script>
  <script type="text/javascript" src="./index_files/byside_webcare.js"></script>
 <script>window.iwc_closebtn_left_573f017122c4a = 600;window.iwc_closebtn_top_573f017122c4a = 0;window.iwc_skin_width_573f017122c4a = 637;window.iwc_skin_height_573f017122c4a = 408;window.iwc_skin_fixed_height_573f017122c4a = true;</script><script>bysideWebcare_renderedevents.push('573f017122c4a');</script><script>(function(){var ss1 = document.createElement("style");var def = "#webcareslotcontainer_3314 {display:block;    position:relative;    border:0px none;    margin:0px;    padding:0px;  width:575px;  height:410px;  background:url(//s5.byside.com/files/24C48E3229/placeholders/CO_CampanhaActiva_BySide_2.gif);}#webcareslotcontainer_3314 .webcareslot1 {  position:absolute;    top:350px;    left:277px;    margin:0px;    padding:0px;}";ss1.setAttribute("type", "text/css");var hh1 = document.getElementsByTagName("head")[0];hh1.appendChild(ss1);if (ss1.styleSheet) {ss1.styleSheet.cssText = def;} else {var tt1 = document.createTextNode(def);ss1.appendChild(tt1);}})();</script><style type="text/css">#webcareslotcontainer_3314 {display:block;    position:relative;    border:0px none;    margin:0px;    padding:0px;  width:575px;  height:410px;  background:url(//s5.byside.com/files/24C48E3229/placeholders/CO_CampanhaActiva_BySide_2.gif);}#webcareslotcontainer_3314 .webcareslot1 {  position:absolute;    top:350px;    left:277px;    margin:0px;    padding:0px;}</style><script>
function bwc_bt_webcarePopup_7469290868_3314_11332_1463746929_ok() { 
bysideWebcareOpenWebcareWindow("###___###", "MjRDNDhFMzIyOUBANTcwMEBJQEBAR0BAQHcxMTMzMjozMzE0QEA%3D", "I", "573f017122c4a", 11332, 3314, null );}
function bwc_bt_webcarePopup_7469290868_3314_11332_1463746929_cancel() { bysideWebcareCloseWebcareAlert("webcarePopup_7469290868", "573f017122c4a", 11332, 3314, null );}
</script><script>
function bysideWebcare_fired_webcarePopup_7469290868() { sendWidgetFeedback(11332,3314,'S',null);;if(window.sendActiveCampaignFeedback !== undefined) { sendActiveCampaignFeedback( 3036, 3314, "S" ); }else if ( byside_webcare_osid == null ) { bysideWebcare_make_request( "http://webcare.byside.com/BWA24C48E3229/usert_feedback.php?webcare_id=" + bysideWebcare_webcare_id + "&tuid=" + byside_webcare_tuid + "&suid=" + (typeof byside_webcare_suid !== 'undefined' ? byside_webcare_suid : '') + "&event_id=A3036&slot_id=3314&feedback_type=S&action1=&page=" + escape(bysideWebcare_getcurrentpage()) ); }if(window.bysideWebcare_placeholder_shown_3314 !== undefined) {bysideWebcare_placeholder_shown_3314();};}
function bwc_webcarePopup_7469290868_cancel() { bysideWebcareCloseAlert( 'webcarePopup_7469290868' ); if(window.sendActiveCampaignFeedback !== undefined) { sendActiveCampaignFeedback( 3036, 3314, "C" ); }else if ( byside_webcare_osid == null ) { bysideWebcare_make_request( "http://webcare.byside.com/BWA24C48E3229/usert_feedback.php?webcare_id=" + bysideWebcare_webcare_id + "&tuid=" + byside_webcare_tuid + "&suid=" + (typeof byside_webcare_suid !== 'undefined' ? byside_webcare_suid : '') + "&event_id=A3036&slot_id=3314&feedback_type=C&action1=&page=" + escape(bysideWebcare_getcurrentpage()) ); }} </script><script>
		if(!document.getElementsByClassName){document.getElementsByClassName=function(e){var t=document,n,r,i,s=[];if(t.querySelectorAll){return t.querySelectorAll("."+e)}if(t.evaluate){r=".//*[contains(concat(' ', @class, ' '), ' "+e+" ')]";n=t.evaluate(r,t,null,0,null);while(i=n.iterateNext()){s.push(i)}}else{n=t.getElementsByTagName("*");r=new RegExp("(^|\\s)"+e+"(\\s|$)");for(i=0;i<n.length;i++){if(r.test(n[i].className)){s.push(n[i])}}}return s}};
		function bysideWebcare_campaign_shown_29656_5554() {
			var els = document.getElementsByClassName('widget_Window_29656_5554'), i = 0;
			for (; i < els.length; i++) {
				if (els[i]) {
					els[i].src = "http://webcare.byside.com/BWA24C48E3229/index.php?wid=MjRDNDhFMzIyOUBAMTc3NzBAQEBAQEBAdzI5NjU2OjU1NTRAQA%3D%3D&lang=pt&tuid=si9rxxnf0oskl5lixrrowqne4mqi2x21g3fbgrwy617gs7n1wy&puid=fm6rd9afvxswhr5dws48dipxf3435vhw5xmxtxklyf341a4fke&suid=null&page=http%3A%2F%2Fwww.bancobpi.pt%2Fparticulares&iuid=573f01712f04e";
				}
			}
		}
	</script><script>
	var bwe_elevador_open = false;

	// jQuery
	(function(e,a,g,h,f,c,b,d){if(!(f=e.jQuery)||g>f.fn.jquery||h(f)){c=a.createElement("script");c.type="text/javascript";c.src="//webcare.byside.com/include/js/jquery/lib/"+g+"/jquery.min.js";c.onload=c.onreadystatechange=function(){if(!b&&(!(d=this.readyState)||d=="loaded"||d=="complete")){h((f=e.jQuery).noConflict(1),b=1);f(c).remove()}};a.getElementsByTagName('head')[0].appendChild(c)}})(window,document,"1.7.2",function($,L){

		// Valor de abertura da popup
		var $bwe_open_value = "447";

		// Novo ID e remove o css adicionado pelo webcare;
		var $bwe_elevador = $('#bwe_elevador'),
			$bwe_button = $('#bwe_button'),
			$bwe_popup = $bwe_elevador.parent('div');

		var	$bwe_popup_width = $bwe_elevador.width(),
			$bwe_popup_height = $bwe_elevador.height(),
			$bwe_window_half_height = $(window).height()/2,
			$bwe_popup_half_height = $bwe_elevador.height()/2,
			$bwe_popup_new_top = $bwe_window_half_height - $bwe_popup_half_height + 'px';

		// Activa campanha
		window.bwe_show_chat = function(){

			// Actualiza os valores;
			$bwe_popup_width = $bwe_elevador.width();
			$bwe_popup_height = $bwe_elevador.height();
			$bwe_window_half_height = $(window).height()/2;
			$bwe_popup_half_height = $bwe_elevador.height()/2;
			$bwe_popup_new_top = $bwe_window_half_height - $bwe_popup_half_height + 'px';

			$bwe_elevador.show();
			$bwe_popup.removeAttr('style');

			// Animação da direita para o meio;
			$bwe_popup.css({'margin-left':'255px', 'position':'fixed', 'bottom':'-'+$bwe_popup_height+'px', 'left':'50%', 'z-index':'10000'});
			$bwe_popup.stop().animate({'bottom':'-'+$bwe_open_value+'px'}, 500, function(){});

		}

		// Abre e fecha o popup;
		window.bwe_elevador_toggle = function() {
			if(!bwe_elevador_open){
				$('a.minimizar').addClass('on');
				$bwe_popup.stop().animate({'bottom': '-4px'}, 500, function(){
					bwe_elevador_open = true;
				});
			} else {
				$('a.minimizar').removeClass('on');
				$bwe_popup.stop().animate({'bottom': '-'+$bwe_open_value+'px'}, 500, function(){
					bwe_elevador_open = false;
				});
			}
		}

		// Esconde o popup;
		window.bwe_elevador_close = function() {
			$bwe_popup.stop().animate({'bottom': '-'+$bwe_popup_height+'px'}, 500, function(){
				bwe_elevador_open = false;
			});
		}

		// Abre o elevador apartir de um botão na página
		var firstopen = 0;
		window.external_open = function() {
			if (firstopen == 0){
				setTimeout(function() {
					firstopen = 1;
					bwe_show_chat();
					bwe_elevador_toggle();
				}, 1500);
			} else {
				bwe_elevador_toggle();
			}
		}

	});

</script>
  <script type="text/javascript">AdfDhtmlPage.__frameBusting("differentDomain");</script>
  <div id="d1">
   <a id="d1::skip" href="?particulares#" class="af_document_skip-link">Saltar para o conteúdo</a>
       
    <div id="particular">
     <div class="site">
      <div id="busy" style="display: none;">
       <div class="innerModal">
        <img id="pt1:gi2" src="./index_files/loader1.gif" alt="Loading...">
       </div>
      </div>
      <div id="pt1:p1" style="display:none">
       <div style="top:auto;right:auto;left:auto;bottom:auto;width:auto;height:auto;position:relative;" id="pt1:p1::content">
        <div style="display: none;">
         &nbsp;
        </div>
       </div>
      </div>
      <header id="header">
       <div class="wraper">
        <a id="pt1:bpi-logo" title="Página Inicial" class="bpi-logo" href="?particulares"><img id="pt1:pt_gi1" src="./index_files/bpi.png" alt="Logo BPI"></a>
        <nav id="menu-areas">
         <ul>
          <li class="current"><a id="pt1:navigation-fragment:navigation-fragment:gl1" class="" href="?particulares">Particulares</a></li>
          <li class=""><a id="pt1:navigation-fragment:navigation-fragment:gl1j_id_1" class="" href="?centros-investimento">Centros Investimento</a></li>
          <li class=""><a id="pt1:navigation-fragment:navigation-fragment:gl1j_id_2" class="" href="?private">Private</a></li>
          <li class=""><a id="pt1:navigation-fragment:navigation-fragment:gl1j_id_3" class="" href="?nao-residentes">Não Residentes</a></li>
          <li class=""><a id="pt1:navigation-fragment:navigation-fragment:gl1j_id_4" class="" href="?empresas">Empresas</a></li>
          <li class=""><a id="pt1:navigation-fragment:navigation-fragment:gl1j_id_5" class="" href="?responsabilidade-social">Responsabilidade Social</a></li>
          <li class=""><a id="pt1:navigation-fragment:navigation-fragment:gl1j_id_6" class="" href="?grupo-bpi">Grupo BPI</a></li>
         </ul>
        </nav>
        <nav id="menu"> 
         <ul> 
          <li><a id="pt1:navigation-fragment:navigation-fragment:i8:0:gl19" class="" href="?particulares/contas">Contas</a> 
           <div class="submenu"> 
            <dl> 
             <dt>
               Produtos 
             </dt> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:0:i6:0:gl27" class="" href="?particulares/contas/contas-a-ordem">Contas à Ordem</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:0:i6:0:i2:0:i3:0:gl13" class="" href="?particulares/contas/contas-a-ordem/conta-a-ordem">Conta à Ordem</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:0:i6:0:i2:0:i3:1:gl13" class="" href="?particulares/contas/contas-a-ordem/conta-junior">Conta Júnior</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:0:i6:0:i2:0:i3:2:gl13" class="" href="?particulares/contas/contas-a-ordem/conta-jovem-13-25">Conta Jovem 13-25</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:0:i6:0:i2:0:i3:3:gl13" class="" href="?particulares/contas/contas-a-ordem/conta-em-moeda-estrangeira">Conta em Moeda Estrangeira</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:0:i6:0:i2:0:i3:4:gl13" class="" href="?particulares/contas/contas-a-ordem/servicos-minimos-bancarios">Serviços Mínimos Bancários</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:0:i6:0:i2:0:i3:5:gl13" class="" href="?particulares/contas/contas-a-ordem/conta-base-bpi">Conta Base BPI</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:0:i6:1:gl27" class="" href="?particulares/contas/contas-ordenado">Conta Ordenado</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:0:i6:2:gl27" class="" href="?particulares/contas/transferencias">Transferências</a> 
             </dd> 
            </dl> 
            <div class="menuinfo"> 
             <dl> 
              <dt>
                Tudo Sobre 
              </dt> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:0:j_id__ctru97pc5:0:j_id__ctru105pc5:0:gl23" class="" href="?eu-quero/abrir-conta-bpi">Abertura de Conta</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:0:j_id__ctru97pc5:0:j_id__ctru105pc5:1:gl23" class="" href="?documentacao-digital">Documentação Digital</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:0:j_id__ctru97pc5:0:j_id__ctru105pc5:2:gl982" class="" href="?particulares/contas/mobilidade-de-servicos-bancarios">Mobilidade de Serviços Bancários</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:0:j_id__ctru97pc5:0:j_id__ctru105pc5:3:gl23" class="" href="?particulares/cheques">Cheques</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:0:j_id__ctru97pc5:0:j_id__ctru105pc5:4:gl983" class="" href="?content/conn/UCM/uuid/dDocName:PP_WCS01_UCM01003554" target="_blank">Direitos e Deveres dos Consumidores</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:0:j_id__ctru97pc5:0:j_id__ctru105pc5:5:gl23" class="" href="?particulares/sepa">SEPA</a> 
              </dd> 
             </dl> 
            </div> 
            <div class="menuhighlights"> 
             <ul> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01009700" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:0:j_id__ctru112pc5:0:i10:0:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares/contas/abrir-conta-no-bpi" target="_parent"></a></li> 
             </ul> 
            </div> 
           </div></li> 
          <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:gl19" class="" href="?particulares/poupar-investir">Poupar e Investir</a> 
           <div class="submenu"> 
            <dl> 
             <dt>
               Produtos 
             </dt> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:0:gl27" class="" href="?particulares/poupar-investir/contas-poupanca">Contas Poupança</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:0:i2:0:i3:0:gl13" class="" href="?particulares/poupanca-e-investimento/contas-poupanca/bpi-poupanca-objectivo">BPI Poupança Objectivo</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:0:i2:0:i3:1:gl13" class="" href="?particulares/poupanca-e-investimento/contas-poupanca/conta-poupanca-rendimento-bpi">Conta Poupança Rendimento BPI</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:0:i2:0:i3:2:gl13" class="" href="?particulares/poupanca-e-investimento/contas-poupanca/conta-poupanca-reforma">Conta Poupança Reforma</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:0:i2:0:i3:3:gl13" class="" href="?particulares/poupanca-e-investimento/contas-poupanca/abconta">ABConta</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:0:i2:0:i3:4:gl13" class="" href="?particulares/poupanca-e-investimento/contas-poupanca/conta-poupanca-reformado">Conta Poupança Reformado</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:0:i2:0:i3:5:gl13" class="" href="?particulares/poupanca-e-investimento/contas-poupanca/conta-poupanca-habitacao">Conta Poupança Habitação</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:0:i2:0:i3:6:gl13" class="" href="?particulares/poupanca-e-investimento/contas-poupanca/conta-poupanca-condominio">Conta Poupança Condomínio</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:1:gl27" class="" href="?particulares/poupar-investir/depositos-prazo">Depósitos a Prazo</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:1:i2:0:i3:0:gl13" class="" href="?particulares/poupanca-e-investimento/depositos-a-prazo/deposito-a-prazo">Depósito a Prazo</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:1:i2:0:i3:1:gl13" class="" href="?particulares/poupanca-e-investimento/depositos-a-prazo/deposito-especial-bpi-1-ano">Depósito Especial BPI 1 ano</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:1:i2:0:i3:2:gl13" class="" href="?particulares/poupanca-e-investimento/depositos-a-prazo/deposito-especial-bpi-2-anos">Depósito Especial BPI 2 anos</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:1:i2:0:i3:3:gl13" class="" href="?particulares/poupanca-e-investimento/depositos-a-prazo/deposito-especial-bpi-3-anos">Depósito Especial BPI 3 anos</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:1:i2:0:i3:4:gl13" class="" href="?particulares/poupanca-e-investimento/depositos-a-prazo/dp-valor-4">DP Valor 4</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:1:i2:0:i3:5:gl13" class="" href="?particulares/poupanca-e-investimento/depositos-a-prazo/dp-valor-6">DP Valor 6</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:1:i2:0:i3:6:gl13" class="" href="?particulares/poupanca-e-investimento/depositos-a-prazo/dp-bpi-net">DP BPI Net</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:1:i2:0:i3:7:gl13" class="" href="?particulares/poupanca-e-investimento/depositos-a-prazo/conta-emigrante">Conta Emigrante</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:2:gl27" class="" href="?particulares/poupar-investir/fundos-investimento">Fundos BPI</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:2:i2:0:gl4" class="" href="?particulares/poupar-investir/fundos-investimento/fundos-accoes">Fundos de Acções</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:2:i2:1:gl4" class="" href="?particulares/poupar-investir/fundos-investimento/fundos-curto-prazo">Fundos de Curto Prazo</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:2:i2:2:gl4" class="" href="?particulares/poupar-investir/fundos-investimento/fundos-fundos">Fundos de Fundos</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:2:i2:3:gl4" class="" href="?particulares/poupar-investir/fundos-investimento/fundos-investimentos-imobiliario">Fundos de Investimentos Imobiliário</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:2:i2:4:gl4" class="" href="?particulares/poupar-investir/fundos-investimento/fundos-obrigacoes">Fundos de Obrigações</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:2:i2:5:gl4" class="" href="?particulares/poupar-investir/fundos-investimento/fundos-flexiveis">Fundos Flexíveis</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:2:i2:6:gl4" class="" href="?particulares/poupar-investir/fundos-investimento/fundos-monetarios">Fundos Monetários de Curto Prazo</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:3:gl27" class="" href="?particulares/poupar-investir/seguros-capitalizacao">Seguros de Capitalização</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:3:i2:0:i3:0:gl13" class="" href="?particulares/poupanca-e-investimento/seguros-de-capitalizacao/bpi-multi-solucoes">BPI Multi-Soluções</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:3:i2:0:i3:1:gl13" class="" href="?particulares/poupanca-e-investimento/seguros-de-capitalizacao/bpi-vida-universal-(accoes)">BPI Vida Universal (Acções)</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:3:i2:0:i3:2:gl13" class="" href="?particulares/poupanca-e-investimento/seguros-de-capitalizacao/poupanca-dollar">Poupança Dollar</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:3:i2:0:i3:3:gl13" class="" href="?particulares/poupanca-e-investimento/seguros-de-capitalizacao/bpi-taxa-fixa-alemanha">BPI Taxa Fixa Alemanha</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:3:i2:0:i3:4:gl13" class="" href="?particulares/poupanca-e-investimento/seguros-de-capitalizacao/bpi-novo-aforro-familiar">BPI Novo Aforro Familiar</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:3:i2:0:i3:5:gl13" class="" href="?particulares/poupanca-e-investimento/seguros-de-capitalizacao/bpi-aforro-jovem">BPI Aforro Jovem</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:3:i2:0:i3:6:gl13" class="" href="?particulares/poupanca-e-investimento/seguros-de-capitalizacao/bpi-renda-programada">BPI Renda Programada</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:4:gl27" class="" href="?particulares/poupar-investir/ppr">PPR</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:4:i2:0:i3:0:gl13" class="" href="?particulares/poupanca-e-investimento/ppr/bpi-reforma-segura-ppr">BPI Reforma Segura PPR</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:4:i2:0:i3:1:gl13" class="" href="?particulares/poupanca-e-investimento/ppr/bpi-reforma-investimento-ppr">BPI Reforma Investimento PPR</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:4:i2:0:i3:2:gl13" class="" href="?particulares/poupanca-e-investimento/ppr/bpi-reforma-accoes-ppr">BPI Reforma Acções PPR</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:4:i2:0:i3:3:gl13" class="" href="?particulares/poupanca-e-investimento/ppr/bpi-reforma-aforro-ppr">BPI Reforma Aforro PPR</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:4:i2:0:i3:4:gl13" class="" href="?particulares/poupanca-e-investimento/ppr/fundo-de-pensoes-bpi-vida-ppr">Fundo de Pensões BPI Vida PPR</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:5:gl27" class="" href="?particulares/poupar-investir/obrigacoes-bpi">Obrigações</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:6:gl27" class="" href="?particulares/poupar-investir/produtos-estruturados">Produtos Estruturados</a> 
              <ul> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:1:i6:7:gl27" class="" href="?particulares/poupar-investir/depositos-indexados">Depósitos Indexados</a> 
             </dd> 
            </dl> 
            <div class="menuinfo"> 
             <dl> 
              <dt>
                Simuladores 
              </dt> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:1:j_id__ctru97pc5:0:j_id__ctru100pc5:0:gl21" class="" href="http://sim.bancobpi.pt/simulador/rentabilidade.jsp" target="_blank">Rentabilidade</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:1:j_id__ctru97pc5:0:j_id__ctru100pc5:1:gl21" class="" href="http://sim.bancobpi.pt/simulador/ppr.jsp" target="_blank">PPR</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:1:j_id__ctru97pc5:0:j_id__ctru100pc5:2:gl21" class="" href="http://sim.bancobpi.pt/simulador-planos-poupanca/pt/index.html" target="_blank">Planos Poupança</a> 
              </dd> 
             </dl> 
             <dl> 
              <dt>
                Tudo Sobre 
              </dt> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:1:j_id__ctru97pc5:0:j_id__ctru105pc5:0:gl23" class="" href="?eu-quero/planear-reforma">Planear Reforma</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:1:j_id__ctru97pc5:0:j_id__ctru105pc5:1:gl23" class="" href="?cotacoes">Cotações</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:1:j_id__ctru97pc5:0:j_id__ctru105pc5:2:gl23" class="" href="?rentabilidade">Rentabilidades</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:1:j_id__ctru97pc5:0:j_id__ctru105pc5:3:gl982" class="" href="?particulares/poupanca-e-investimento/manual-do-investidor">Manual do Investidor</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:1:j_id__ctru97pc5:0:j_id__ctru105pc5:4:gl23" class="" href="?particulares/poupanca-e-investimento/condicoes-de-movimentacao">Condições de Movimentação de Seguros de Capitalização</a> 
              </dd> 
             </dl> 
            </div> 
            <div class="menuhighlights"> 
             <ul> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01018898" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:1:j_id__ctru112pc5:0:i10:0:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares/poupanca-e-investimento/fundos-flexiveis-bpi" target="_parent"></a></li> 
             </ul> 
            </div> 
           </div></li> 
          <li><a id="pt1:navigation-fragment:navigation-fragment:i8:2:gl19" class="" href="?particulares/credito">Crédito</a> 
           <div class="submenu"> 
            <dl> 
             <dt>
               Produtos 
             </dt> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:0:gl27" class="" href="?particulares/credito/credito-habitacao">Habitação</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:0:i2:0:i3:0:gl13" class="" href="?particulares/credito/habitacao/modalidades-de-credito-habitacao">Modalidades de Crédito Habitação</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:0:i2:0:i3:1:gl13" class="" href="?particulares/credito/habitacao/comprar-casa">Comprar Casa</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:0:i2:0:i3:2:gl13" class="" href="?particulares/credito/habitacao/seguros">Seguros</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:0:i2:0:i3:3:gl13" class="" href="?particulares/credito/habitacao/servico-de-avaliacao">Serviço de Avaliação</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:1:gl27" class="" href="?particulares/credito/credito-pessoal">Pessoal</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:1:i2:0:i3:0:gl13" class="" href="?particulares/credito/pessoal/credito-pessoal">Crédito Pessoal</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:1:i2:0:i3:1:gl13" class="" href="?particulares/credito/pessoal/credito-formacao">Crédito Formação</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:2:gl27" class="" href="?particulares/credito/credito-automovel">Automóvel</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:2:i2:0:i3:0:gl13" class="" href="?particulares/credito/automovel/ald">ALD</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:2:i2:0:i3:1:gl13" class="" href="?particulares/credito/automovel/leasing">Leasing</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:2:i6:2:i2:0:i3:2:gl13" class="" href="?particulares/credito/automovel/credito-com-reserva-de-propriedade">Crédito com Reserva de Propriedade</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
            </dl> 
            <div class="menuinfo"> 
             <dl> 
              <dt>
                Simuladores 
              </dt> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:2:j_id__ctru97pc5:0:j_id__ctru100pc5:0:gl21" class="" href="http://rep.bancobpi.pt/simuladores/simch/simch.asp" target="_blank">Crédito Habitação</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:2:j_id__ctru97pc5:0:j_id__ctru100pc5:1:gl21" class="" href="http://sim.bancobpi.pt/simulador/creditoautomovel.jsp" target="_blank">Crédito Automóvel</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:2:j_id__ctru97pc5:0:j_id__ctru100pc5:2:gl21" class="" href="http://sim.bancobpi.pt/simulador-credito-pessoal/pt/creditopessoal.jsp" target="_blank">Crédito Pessoal</a> 
              </dd> 
             </dl> 
             <dl> 
              <dt>
                Tudo Sobre 
              </dt> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:2:j_id__ctru97pc5:0:j_id__ctru105pc5:0:gl982" class="" href="?particulares/credito/imoveis-bpi-oportunidade">Oportunidades BPI</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:2:j_id__ctru97pc5:0:j_id__ctru105pc5:1:gl982" class="" href="?particulares/credito/incumprimentos-de-contratos-de-creditos-e-rede-extrajudicial-de-apoio">Incumprimentos de Contratos de Créditos e Rede Extrajudicial de apoio</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:2:j_id__ctru97pc5:0:j_id__ctru105pc5:2:gl982" class="" href="?particulares/credito/mediador-do-credito">Mediador do Crédito</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:2:j_id__ctru97pc5:0:j_id__ctru105pc5:3:gl983" class="" href="?content/conn/UCM/uuid/dDocName:PP_WCS01_UCM01003555" target="_blank">Direitos e Deveres dos Consumidores</a> 
              </dd> 
             </dl> 
            </div> 
            <div class="menuhighlights"> 
             <ul> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01008369" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:2:j_id__ctru112pc5:0:i10:0:gl26" class="button" style="background-color: transparent;" href="http://rep.bancobpi.pt/simuladores/simch/simch.asp" target="_blank"></a></li> 
             </ul> 
            </div> 
           </div></li> 
          <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:gl19" class="" href="?particulares/cartoes">Cartões</a> 
           <div class="submenu"> 
            <dl> 
             <dt>
               Produtos 
             </dt> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:0:gl27" class="" href="?particulares/cartoes/cartoes-debito">Débito</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:0:i2:0:i3:0:gl13" class="" href="?particulares/cartoes/debito/bpi-electron">BPI Electron</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:0:i2:0:i3:1:gl13" class="" href="?particulares/cartoes/debito/cartao-aeist">Cartão AEIST</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:0:i2:1:gl4" class="" href="?particulares/cartoes/cartoes-debito/bpi-universitario">BPI Universitário</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:1:gl27" class="" href="?particulares/cartoes/cartoes-credito">Crédito</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:1:i2:0:i3:0:gl13" class="" href="?particulares/cartoes/credito/cartao-bpi">Cartão BPI</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:1:i2:0:i3:1:gl13" class="" href="?particulares/cartoes/credito/cartao-bpi-premio">Cartão BPI Prémio</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:1:i2:0:i3:2:gl13" class="" href="?particulares/cartoes/credito/cartao-visa-fcp">Cartão Visa FCP</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:1:i2:0:i3:3:gl13" class="" href="?particulares/cartoes/credito/bpi-campeoes">BPI Campeões</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:1:i2:0:i3:4:gl13" class="" href="?particulares/cartoes/credito/cartao-bpi-gold">Cartão BPI Gold</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:1:i2:0:i3:5:gl13" class="" href="?particulares/cartoes/credito/cartao-bpi-zoom">Cartão BPI Zoom</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:2:gl27" class="" href="?particulares/cartoes/cartoes-pre-pagos">Pré-pagos</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:2:i2:0:i3:0:gl13" class="" href="?particulares/cartoes/pre-pagos/bpi-cash-virtual">BPI Cash Virtual</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:3:i6:2:i2:0:i3:1:gl13" class="" href="?particulares/cartoes/pre-pagos/bpi-cash">BPI Cash</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
            </dl> 
            <div class="menuinfo"> 
             <dl> 
              <dt>
                Tudo Sobre 
              </dt> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:3:j_id__ctru97pc5:0:j_id__ctru105pc5:0:gl23" class="" href="?cartoes/seguranca-cartoes">Segurança Cartões</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:3:j_id__ctru97pc5:0:j_id__ctru105pc5:1:gl23" class="" href="?particulares/cartoes/mb-way">MB Way</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:3:j_id__ctru97pc5:0:j_id__ctru105pc5:2:gl23" class="" href="?mbnet">MB Net</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:3:j_id__ctru97pc5:0:j_id__ctru105pc5:3:gl23" class="" href="?contactos-uteis-cartoes">Contactos</a> 
              </dd> 
             </dl> 
            </div> 
            <div class="menuhighlights"> 
             <ul> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01021714" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:3:j_id__ctru112pc5:0:i10:0:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares/cartoes/3d-secure" target="_parent"></a></li> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01017709" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:3:j_id__ctru112pc5:0:i10:1:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares/cartoes/pre-pagos/bpi-cash-virtual" target="_parent"></a></li> 
             </ul> 
            </div> 
           </div></li> 
          <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:gl19" class="" href="?particulares/seguros">Seguros</a> 
           <div class="submenu"> 
            <dl> 
             <dt>
               Produtos 
             </dt> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:0:gl27" class="" href="?particulares/seguros/pessoas">Pessoas</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:0:i2:0:i3:0:gl13" class="" href="?particulares/seguros/pessoas/allianz-vida-segura">Allianz Vida Segura</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:0:i2:0:i3:1:gl13" class="" href="?particulares/seguros/pessoas/allianz-saude">Allianz Saúde</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:0:i2:0:i3:2:gl13" class="" href="?particulares/seguros/pessoas/allianz-saude-55-mais">Allianz Saúde 55 Mais</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:0:i2:0:i3:3:gl13" class="" href="?particulares/seguros/pessoas/allianz-saude-dental">Allianz Saúde Dental</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:0:i2:0:i3:4:gl13" class="" href="?particulares/seguros/pessoas/allianz-servicos-domesticos">Allianz Serviços Domésticos</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:0:i2:0:i3:5:gl13" class="" href="?particulares/seguros/pessoas/allianz-acidentes-pessoais-surf">Allianz Acidentes Pessoais Surf</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:1:gl27" class="" href="?particulares/seguros/patrimonio">Património</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:1:i2:0:i3:0:gl13" class="" href="?particulares/seguros/patrimonio/allianz-responsabilidade-civil">Allianz Responsabilidade Civil</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:1:i2:0:i3:1:gl13" class="" href="?particulares/seguros/patrimonio/allianz-casa">Allianz Casa</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:1:i2:0:i3:2:gl13" class="" href="?particulares/seguros/patrimonio/allianz-auto">Allianz Auto</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:1:i2:0:i3:3:gl13" class="" href="?particulares/seguros/patrimonio/allianz-moto">Allianz Moto</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:1:i2:0:i3:4:gl13" class="" href="?particulares/seguros/patrimonio/allianz-embarcacoes-de-recreio">Allianz Embarcações de Recreio</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:1:i2:0:i3:5:gl13" class="" href="?particulares/seguros/patrimonio/allianz-caca-e-armas">Allianz Caça e Armas</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:1:i2:0:i3:6:gl13" class="" href="?particulares/seguros/patrimonio/allianz-condominio">Allianz Condomínio</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:2:gl27" class="" href="?particulares/seguros/negocios">Soluções para Negócios</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:2:i2:0:i3:0:gl13" class="" href="?particulares/seguros/patrimonio/allianz-responsabilidade-civil">Allianz Responsabilidade Civil</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:2:i2:0:i3:1:gl13" class="" href="?particulares/seguros/negocios/allianz-mercadorias">Allianz Mercadorias</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:2:i2:0:i3:2:gl13" class="" href="?particulares/seguros/negocios/allianz-hoteis">Allianz Hotéis</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:2:i2:0:i3:3:gl13" class="" href="?particulares/seguros/negocios/allianz-agro">Allianz Agro</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:2:i2:0:i3:4:gl13" class="" href="?particulares/seguros/negocios/allianz-saude-empresas">Allianz Saúde Empresas</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:2:i2:0:i3:5:gl13" class="" href="?particulares/seguros/negocios/allianz-pme">Allianz PME</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:2:i2:0:i3:6:gl13" class="" href="?particulares/seguros/negocios/allianz-multirriscos-pme">Allianz Multirriscos PME</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:4:i6:2:i2:0:i3:7:gl13" class="" href="?particulares/seguros/negocios/allianz-acidentes-de-trabalho">Allianz Acidentes de Trabalho</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
            </dl> 
            <div class="menuinfo"> 
             <dl> 
              <dt>
                Simuladores 
              </dt> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:4:j_id__ctru97pc5:0:j_id__ctru100pc5:0:gl21" class="" href="http://sim.bancobpi.pt/simulador/allianzvida.jsp" target="_blank">Seguro Vida</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:4:j_id__ctru97pc5:0:j_id__ctru100pc5:1:gl21" class="" href="http://sim.bancobpi.pt/simulador/allianzsaude.jsp" target="_blank">Seguro Saúde</a> 
              </dd> 
             </dl> 
            </div> 
            <div class="menuhighlights"> 
             <ul> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01022313" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:4:j_id__ctru112pc5:0:i10:0:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares/seguros/patrimonio/allianz-responsabilidade-civil" target="_parent"></a></li> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01018302" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:4:j_id__ctru112pc5:0:i10:1:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares/seguros/pessoas/allianz-saude" target="_parent"></a></li> 
             </ul> 
            </div> 
           </div></li> 
          <li> 
           <div class="submenu"> 
            <dl> 
             <dt>
               Produtos 
             </dt> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:5:i6:0:i4:0:gl16415" class="" href="?particulares#">Colecção Boa Esperança</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:5:i6:0:i4:1:gl16415" class="" href="?particulares#">Alarmes Securitas Direct</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:5:i6:0:i4:2:gl16415" class="" href="?particulares#">Moeda Jogos Olímpicos 2016</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:5:i6:0:i4:3:gl16415" class="" href="?particulares/produtos-de-prestigio/joias/moedas-rainhas-da-europa">Moedas Rainhas da Europa</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:5:i6:0:i4:4:gl16415" class="" href="?particulares/produtos-de-prestigio/joias/100-anos-de-fatima">100 Anos Fátima</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:5:i6:0:i4:5:gl16415" class="" href="?particulares/produtos-de-prestigio/joias/ocasioes-especiais">Ocasiões Especiais</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:5:i6:0:i4:6:gl16415" class="" href="?particulares/produtos-de-prestigio/casa/linha-casa">Linha Casa</a> 
             </dd> 
            </dl> 
            <div class="menuhighlights"> 
             <ul> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01026782" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:5:j_id__ctru112pc5:0:i10:0:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares/produtos-de-prestigio/joias/joias-mimi" target="_parent"></a></li> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01024993" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:5:j_id__ctru112pc5:0:i10:1:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares#" target="_parent"></a></li> 
             </ul> 
            </div> 
           </div></li> 
          <li><a id="pt1:navigation-fragment:navigation-fragment:i8:6:gl16" class="" href="?particulares/produtos-prestigio">Produtos Prestígio</a> 
           <div class="submenu"> 
            <dl> 
             <dt>
               Produtos 
             </dt> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:0:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/tecnologia/sistemas-de-som">Sistemas de Som</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:1:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/joias/joalharia-2016">Joalharia 2016</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:2:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/relogios/relogios-franck-muller">Relógios Franck Müller</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:3:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/montblanc">Montblanc</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:4:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/numismatica-e-arte/moeda-o-modernismo">Moeda "O Modernismo"</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:5:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/joias/-ocasioes-especiais">Ocasiões Especiais</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:6:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/joias/joias-mimi">Jóias MIMI</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:7:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/tecnologia/tecnologia-sony---2016">Tecnologia Sony</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:8:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/vinhos/conjuntos-de-vinhos">Conjuntos de Vinhos</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:9:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/joias/conjunto-recordar-e-viver">Recordar é Viver</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:10:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/joias/aneis-splendoro">Anéis Splend'Oro</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:11:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/joias/dia-da-mae">Dia da Mãe</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:12:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/joias/coleccao-boa-esperanca">Colecção Boa Esperança</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:13:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/tecnologia/computadores-lenovo">Computadores Lenovo</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:14:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/100-anos-de-fatima/100-anos-de-fatima">100 Anos de Fátima</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:15:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/numismatica-e-arte/cartao-socio-de-ouro">Cartão "Sócio de Ouro"</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:16:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/joias/eugenio-campos">Eugénio Campos</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:17:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/numismatica-e-arte/moeda-jogos-olimpicos-2016">Moeda Jogos Olímpicos 2016</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:18:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/casa/linha-casa">Linha Casa</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:19:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/casa/alarmes-securitas-direct">Alarmes Securitas Direct</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:20:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/numismatica-e-arte/moedas-rainhas-da-europa">Moeda Rainhas da Europa</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru33pc5:21:gl3" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/numismatica-e-arte/moeda-colchas-de-castelo-branco">Moeda Colchas de Castelo Branco<br></a> 
             </dd> 
            </dl> 
            <div class="menuhighlights"> 
             <ul> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01026782" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru112pc5:0:i10:0:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares/produtos-de-prestigio/joias/joias-mimi" target="_parent"></a></li> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01024993" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:6:j_id__ctru112pc5:0:i10:1:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares#" target="_parent"></a></li> 
             </ul> 
            </div> 
           </div></li> 
          <li><a id="pt1:navigation-fragment:navigation-fragment:i8:7:gl19" class="" href="?particulares/servicos-24-7">Serviços 24/7</a> 
           <div class="submenu"> 
            <dl> 
             <dt>
               Serviços 
             </dt> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:0:gl27" class="" href="?particulares/servicos-24-7/apps">Apps BPI</a> 
              <ul> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:0:i2:0:i3:0:gl13" class="" href="?particulares/servicos-24-7/apps/bpi-app">BPI APP</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:0:i2:0:i3:1:gl13" class="" href="?particulares/servicos-24-7/apps/bpi-premio">BPI Prémio</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:0:i2:0:i3:2:gl13" class="" href="?particulares/servicos-24-7/apps/bpi-poupanca">BPI Poupança</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:0:i2:0:i3:3:gl13" class="" href="?particulares/servicos-24-7/apps/bpi-transferencias">BPI Transferências</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:0:i2:0:i3:4:gl13" class="" href="?particulares/servicos-24-7/apps/bpi-pagamentos">BPI Pagamentos</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:0:i2:0:i3:5:gl13" class="" href="?particulares/servicos-24-7/apps/bpi-cartoes">BPI Cartões</a></li> 
               <li><a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:0:i2:0:i3:6:gl13" class="" href="?particulares/servicos-24-7/apps/bpi-localizador-de-servicos">BPI Localizador de Serviços</a></li> 
               <li style="display: none;">&nbsp;</li> 
              </ul> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:1:i4:0:gl15" class="" href="?particulares/servicos-24-7/maquina-de-depositos">Máquina de Depósitos</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:1:i4:1:gl15" class="" href="?particulares/servicos-24-7/bpi-net">BPI Net</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:1:i4:2:gl15" class="" href="?particulares/servicos-24-7/bpi-directo">BPI Directo</a> 
             </dd> 
             <dd> 
              <a id="pt1:navigation-fragment:navigation-fragment:i8:7:i6:1:i4:3:gl15" class="" href="?particulares/servicos-24-7/bpi-net-mobile">BPI Net Mobile</a> 
             </dd> 
            </dl> 
            <div class="menuinfo"> 
             <dl> 
              <dt>
                Tudo Sobre 
              </dt> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:7:j_id__ctru97pc5:0:j_id__ctru105pc5:0:gl23" class="" href="?seguranca-online">Segurança Online</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:7:j_id__ctru97pc5:0:j_id__ctru105pc5:1:gl23" class="" href="?documentacao-digital">Documentação Digital</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:7:j_id__ctru97pc5:0:j_id__ctru105pc5:2:gl23" class="" href="?particulares/bpi-net-bpi-directo">Como Aderir</a> 
              </dd> 
              <dd> 
               <a id="pt1:navigation-fragment:navigation-fragment:i8:7:j_id__ctru97pc5:0:j_id__ctru105pc5:3:gl23" class="" href="?particulares/pedido-codigo-secreto">Pedido de Código Secreto</a> 
              </dd> 
             </dl> 
            </div> 
            <div class="menuhighlights"> 
             <ul> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01021497" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:7:j_id__ctru112pc5:0:i10:0:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares/servicos-24-7/apps/bpi-poupanca" target="_parent"></a></li> 
              <li><img src="./index_files/dDocName-PR_WCS01_UCM01017711" alt="" title=""><strong></strong><a id="pt1:navigation-fragment:navigation-fragment:i8:7:j_id__ctru112pc5:0:i10:1:j_id__ctru123pc5" class="button" style="background-color: transparent;" href="?particulares/servicos-24-7/apps/bpi-app" target="_parent"></a></li> 
             </ul> 
            </div> 
           </div></li> 
         </ul> 
        </nav>
        <script id="pt1:navigation-fragment:navigation-fragment:s1" type="text/javascript">var IS_IPAD = navigator.userAgent.match(/iPad/) != null;
            var IS_IPHONE = navigator.userAgent.match(/iPhone/) != null;
            if(IS_IPAD || IS_IPHONE) {
                $('#menu-areas').attr('style', 'font-size: 1.100em;');
            }</script>
        <div class="simple-search">
         <span class="top"></span>
         <div class="center">
          <fieldset>
           <legend class="hidden">Pesquisa Simples</legend>
           <label for="search" style="display: none;">Pesquisa</label>
           <input autocomplete="off" class="specialOff" id="search" name="search" placeholder="PESQUISA" type="text" value="">
           <input name="ctrl_state" type="hidden" value="ztkqyywvn_4">
           <input id="contextPath" type="hidden" value="/pesquisaservlet">
           <span><input onclick="doPost(&#39;search&#39;,&#39;/pesquisaservlet&#39;);" type="button" value="OK"></span>
          </fieldset>
          <div class="search-layer hidden">
           &nbsp;
          </div>
         </div>
         <span class="bottom"></span>
        </div>
        <div id="pt1:pt_pgl2" class="af_panelGroupLayout">
         <div>
          <div id="pt1:board-fragment:board-fragment:pgl2" class="af_panelGroupLayout">
           <div>
            <div id="board"> 
             <nav id="menu-links"> 
              <ul class="menu-links"> 
               <li><a href="?particulares#" onclick="return false;">Eu sou</a> 
                <div class="wraper"> 
                 <ul class="submenu-links"> 
                  <li><a id="pt1:board-fragment:board-fragment:gl20" class="" href="?eu-sou/junior">Júnior</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl20j_id_1" class="" href="?eu-sou/jovem">Jovem</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl20j_id_2" class="" href="?eu-sou/universitario">Universitário</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl20j_id_3" class="" href="?eu-sou/jovem-profissional">Jovem Profissional</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl20j_id_4" class="" href="?eu-sou/senior">Sénior</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl20j_id_5" class="" href="?eu-sou/estrangeiro">Estrangeiro</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl7j_id_6" class="" href="?nao-residentes">Não Residente</a></li> 
                 </ul> 
                </div></li> 
               <li><a href="?particulares#" onclick="return false;" style="">Eu quero</a> 
                <div class="wraper"> 
                 <ul class="submenu-links"> 
                  <li><a id="pt1:board-fragment:board-fragment:gl10" class="" href="?eu-quero/abrir-conta-bpi">Abrir Conta no BPI</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl10j_id_1" class="" href="?eu-quero/aderir-bpi-net">Aderir ao BPI Net</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl10j_id_2" class="" href="?eu-quero/comprar-carro">Comprar Carro</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl10j_id_3" class="" href="?eu-quero/comprar-casa">Comprar Casa</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl10j_id_4" class="" href="?eu-quero/credito-pessoal">Crédito Pessoal</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl10j_id_5" class="" href="?eu-quero/gerir-dia-a-dia">Gerir o dia-a-dia</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl10j_id_6" class="" href="?eu-quero/pensar-futuro-filhos">Pensar o futuro dos meus filhos</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl10j_id_7" class="" href="?eu-quero/planear-reforma">Planear a Reforma</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl10j_id_8" class="" href="?eu-quero/poupar-investir">Poupar e Investir</a></li> 
                  <li><a id="pt1:board-fragment:board-fragment:gl10j_id_9" class="" href="?eu-quero/proteger-familia-propriedade">Proteger a família e a propriedade</a></li> 
                 </ul> 
                </div></li> 
              </ul> 
             </nav> 
             <div class="login tabs " id="login"> 
              <ul> 
               <li class="current"><a href="?particulares#users" title="Login Particulares">Particulares</a></li> 
               <li><a href="?particulares#company" title="Login Empresas">Empresas</a></li> 
              </ul> 
              <article class="show-tab" id="users"> 
               <fieldset> 
                <legend class="hidden">Login Users</legend> 
                <strong>BPI Net</strong> 
                <label for="login-users" style="display: none;">Nome/Nº Adesão</label> 
                <input id="login-users" maxlength="20" name="name" placeholder="Nome/Nº Adesão" type="text"> 
                <span class="button"><input onclick="submit2particulares(&#39;login-users&#39;);" type="button" value="Submeter"></span> 
               </fieldset> 
               <a id="pt1:board-fragment:board-fragment:gl3" class="" href="?seguranca-online">Recomendações de Segurança</a> 
              </article> 
              <article class="hide-tab" id="company"> 
               <fieldset> 
                <legend class="hidden">Login Empresas</legend> 
                <strong>BPI Net Empresas</strong> 
                <label class="hidden" for="login-company" style="display: none;">Nome/Nº Adesão</label> 
                <input id="login-company" maxlength="20" name="name" placeholder="Nome/Nº Adesão" type="text"> 
                <span class="button" data-icon="$"><input onclick="submit2empresas(&#39;login-company&#39;);" type="button" value="Submeter"></span> 
               </fieldset> 
               <a id="pt1:board-fragment:board-fragment:gl4" class="" href="?seguranca-online">Recomendações de Segurança</a> 
              </article> 
             </div> 
             <div class="quick-access"> 
              <strong>Quero Ser Cliente</strong> 
              <a id="pt1:board-fragment:board-fragment:gl2" class="" style="color: #fff;" href="?eu-quero/abrir-conta-bpi"><span>Abra uma Conta</span></a> 
             </div> 
             <div class="quick-access"> 
              <strong>Fale Connosco</strong> 
              <p>BPI Directo 707 020 500</p> 
              <a id="pt1:board-fragment:board-fragment:gl6" class="" style="color: #fff;" href="?particulares/contactos/contacto-outros"><span>Outros Contactos BPI</span></a> 
             </div> 
             <div class="quick-access" id="simulators-list"> 
              <strong>Simule aqui</strong> 
              <a href="?particulares#"><span>Calcule o valor das suas prestações e poupanças.</span></a> 
              <ul> 
               <li><a id="pt1:board-fragment:board-fragment:gl5" class="" href="http://sim.bancobpi.pt/simulador-credito-pessoal/pt/creditopessoal.jsp" target="_blank">Crédito Pessoal</a></li> 
               <li><a id="pt1:board-fragment:board-fragment:gl5j_id_1" class="" href="http://sim.bancobpi.pt/simulador/creditoautomovel.jsp" target="_blank">Crédito Automóvel</a></li> 
               <li><a id="pt1:board-fragment:board-fragment:gl5j_id_2" class="" href="http://rep.bancobpi.pt/simuladores/simch/simch.asp" target="_blank">Crédito Habitação</a></li> 
               <li><a id="pt1:board-fragment:board-fragment:gl5j_id_3" class="" href="http://rep.bancobpi.pt/aminhareforma" target="_blank">Reforma</a></li> 
               <li><a id="pt1:board-fragment:board-fragment:gl5j_id_4" class="" href="http://sim.bancobpi.pt/simulador/rentabilidade.jsp" target="_blank">Rentabilidade</a></li> 
               <li><a id="pt1:board-fragment:board-fragment:gl5j_id_5" class="" href="http://sim.bancobpi.pt/simulador/allianzsaude.jsp" target="_blank">Seguro Saúde</a></li> 
               <li><a id="pt1:board-fragment:board-fragment:gl5j_id_6" class="" href="http://sim.bancobpi.pt/simulador/allianzvida.jsp" target="_blank">Seguro Vida</a></li> 
               <li><a id="pt1:board-fragment:board-fragment:gl5j_id_7" class="" href="http://sim.bancobpi.pt/simulador/ppr.jsp" target="_blank">PPR</a></li> 
               <li><a id="pt1:board-fragment:board-fragment:gl5j_id_8" class="" href="http://sim.bancobpi.pt/simulador-planos-poupanca/pt/index.html" target="_blank">Planos Poupança</a></li> 
              </ul> 
             </div> 
            </div>
           </div>
          </div>
         </div>
        </div>
       </div>
      </header>
      <div id="focus"> 
       <div id="pt1:r1" class="af_region"> 
        <div class="container" id="slider" style="width: 1400px; margin-left: -25.5px;"> 
         <ul id="items" style="left: -2800px;"><li style="width: 1400px;" class="cloned"> 
           <!--googleoff: all--><span style="display:none;">Banner ID: PR_WCS01_UCM01025617</span> 
           <!--googleon: all--><h1 class="hidden">&nbsp;</h1><h2 class="hidden">&nbsp;</h2><a id="pt1:r1:0:listTmplt:j_id__ctru0pc30j_id_1:gl25" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/joias/aneis-splendoro" target="_parent"><img id="pt1:r1:0:listTmplt:j_id__ctru0pc30j_id_1:gi4" src="./index_files/dDocName-PR_WCS01_UCM01025616" alt="Soluções Crédito BPI - Três Aneis, Múltiplas Combinações" title="Soluções Crédito BPI - Três Aneis, Múltiplas Combinações"><span class="hidden">&nbsp;</span></a></li> 
          <li style="width: 1400px;"> 
           <!--googleoff: all--><span style="display:none;">Banner ID: PR_WCS01_UCM01022615</span> 
           <!--googleon: all--><h1 class="hidden">&nbsp;</h1><h2 class="hidden">&nbsp;</h2><a id="pt1:r1:0:listTmplt:j_id__ctru0pc28:gl25" class="" style="text-decoration: none;" href="?banco-de-confianca" target="_parent"><img id="pt1:r1:0:listTmplt:j_id__ctru0pc28:gi4" src="./index_files/dDocName-PR_WCS01_UCM01022616" alt="Banco de Confiança." title="Banco de Confiança."><span class="hidden">&nbsp;</span></a></li> 
          <li style="width: 1400px;"> 
           <!--googleoff: all--><span style="display:none;">Banner ID: PR_WCS01_UCM01024668</span> 
           <!--googleon: all--><h1 class="hidden">&nbsp;</h1><h2 class="hidden">&nbsp;</h2><a id="pt1:r1:0:listTmplt:j_id__ctru0pc29j_id_1:gl25" class="" style="text-decoration: none;" href="?responsabilidade-social" target="_parent"><img id="pt1:r1:0:listTmplt:j_id__ctru0pc29j_id_1:gi4" src="./index_files/dDocName-PR_WCS01_UCM01024665" alt="Prémios BPI" title="Prémios BPI"><span class="hidden">&nbsp;</span></a></li> 
          <li style="width: 1400px;"> 
           <!--googleoff: all--><span style="display:none;">Banner ID: PR_WCS01_UCM01025617</span> 
           <!--googleon: all--><h1 class="hidden">&nbsp;</h1><h2 class="hidden">&nbsp;</h2><a id="pt1:r1:0:listTmplt:j_id__ctru0pc30j_id_1:gl25" class="" style="text-decoration: none;" href="?particulares/produtos-de-prestigio/joias/aneis-splendoro" target="_parent"><img id="pt1:r1:0:listTmplt:j_id__ctru0pc30j_id_1:gi4" src="./index_files/dDocName-PR_WCS01_UCM01025616" alt="Soluções Crédito BPI - Três Aneis, Múltiplas Combinações" title="Soluções Crédito BPI - Três Aneis, Múltiplas Combinações"><span class="hidden">&nbsp;</span></a></li> 
         <li style="width: 1400px;" class="cloned"> 
           <!--googleoff: all--><span style="display:none;">Banner ID: PR_WCS01_UCM01022615</span> 
           <!--googleon: all--><h1 class="hidden">&nbsp;</h1><h2 class="hidden">&nbsp;</h2><a id="pt1:r1:0:listTmplt:j_id__ctru0pc28:gl25" class="" style="text-decoration: none;" href="?banco-de-confianca" target="_parent"><img id="pt1:r1:0:listTmplt:j_id__ctru0pc28:gi4" src="./index_files/dDocName-PR_WCS01_UCM01022616" alt="Banco de Confiança." title="Banco de Confiança."><span class="hidden">&nbsp;</span></a></li></ul> 
        </div> 
        <div id="navigation-wrapper"> 
         <ul id="navigation" style="width:54px"> 
          <li style="margin-right: 3px" class=""></li> 
          <li style="margin-right: 3px" class="active"></li> 
          <li style="margin-right: 3px" class=""></li> 
         </ul> 
        </div> 
        <div style="display:none"> 
         <a id="pt1:r1:0:_afrCommandDelegate" class="af_commandLink" onclick="return false;" href="?particulares#">&nbsp;</a> 
        </div> 
       </div> 
      </div>
      <div id="content"> 
       <div class="wraper"> 
        <div id="main"> 
         <div id="pt1:r2" class="highlights af_region"> 
          <div class="print-area text"> 
           <p>Particulares</p> 
          </div> 
          <!--googleoff: all--> 
          <span style="display: none">PR_WCS01_UCM01002395</span> 
          <!--googleon: all--> 
          <ul> 
           <li><img id="pt1:r2:0:listTmplt:clt1:gi1" src="./index_files/dDocName-PR_WCS01_UCM01025036" alt="Prémio BPI Solidário" title="Prémio BPI Solidário"><strong>Candidaturas abertas</strong><p>O prazo para entrega de candidaturas decorre de 2 a 29 de Maio.</p><a id="pt1:r2:0:listTmplt:clt1:j_id__ctru0pc5:gl25" class="button" href="?responsabilidade-social/premio-bpi-solidario" target="_parent">Saiba Mais</a></li> 
           <li><img id="pt1:r2:0:listTmplt:clt1:gi1j_id_1" src="./index_files/dDocName-PR_WCS01_UCM01023789" alt="Nº1 Satisfação Clientes" title="Nº1 Satisfação Clientes"><strong>BPI Líder Satisfação Clientes<br></strong><p>Este prémio é da exclusiva responsabilidade da entidade que o atribuiu.<br></p><a id="pt1:r2:0:listTmplt:clt1:j_id__ctru0pc6j_id_1:gl25" class="button" href="?n1-na-satisfacao-dos-clientes" target="_parent">Saiba mais</a></li> 
           <li><img id="pt1:r2:0:listTmplt:clt1:gi1j_id_2" src="./index_files/dDocName-PR_WCS01_UCM01027417" alt="Seguro Allianz Casa" title="Seguro Allianz Casa. Proteja a sua casa por dentro e por fora."><strong>Seguro Allianz Casa </strong><p>Proteja a sua casa por dentro e por fora.</p><a id="pt1:r2:0:listTmplt:clt1:j_id__ctru0pc7j_id_1:gl25" class="button" href="?particulares/seguros/patrimonio/allianz-casa" target="_parent">Saiba mais</a></li> 
           <li><img id="pt1:r2:0:listTmplt:clt1:gi1j_id_3" src="./index_files/dDocName-PR_WCS01_UCM01026958" alt="Soluções de Crédito BPI. Tecnologia Sony." title="Soluções de Crédito BPI. Tecnologia Sony."><strong>Soluções de Crédito BPI </strong><a id="pt1:r2:0:listTmplt:clt1:j_id__ctru0pc8j_id_1:gl25" class="button" href="?particulares/produtos-de-prestigio/tecnologia/tecnologia-sony---2016" target="_parent">Saiba mais&nbsp;</a></li> 
           <li><img id="pt1:r2:0:listTmplt:clt1:gi1j_id_4" src="./index_files/dDocName-PR_WCS01_UCM01021361" alt="Fundos Flexíveis BPI" title="Fundos Flexíveis BPI"><strong>Fundos de Investimento BPI</strong><a id="pt1:r2:0:listTmplt:clt1:j_id__ctru0pc9j_id_1:gl25" class="button" href="?particulares/poupanca-e-investimento/fundos-flexiveis-bpi" target="_parent">Saiba Mais</a></li> 
           <li><img id="pt1:r2:0:listTmplt:clt1:gi1j_id_5" src="./index_files/dDocName-PR_WCS01_UCM01020188" alt="" title="205x186_credito_pessoal_Mar16"><strong>Soluções de Crédito BPI</strong><a id="pt1:r2:0:listTmplt:clt1:j_id__ctru0pc10j_id_1:gl25" class="button" href="?particulares/credito/pessoal/credito-pessoal" target="_parent">Saiba Mais</a></li> 
          </ul> 
          <div style="display:none"> 
           <a id="pt1:r2:0:_afrCommandDelegate" class="af_commandLink" onclick="return false;" href="?particulares#">&nbsp;</a> 
          </div> 
         </div> 
         <div id="pt1:r8" class="content-section clearfix af_region"> 
          <script>$(document).ready(function() {
                        var articles = $('.botes-redes-sociais');

                        Socialite.setup({
                                   facebook: {
                                        lang : 'pt_PT'
                                   },
                                   googleplus: {
                                        lang : 'pt-PT'
                                   },
                                   twitter: {
                                        lang : 'pt'
                                   }
                        });

                        Socialite.load(articles); 

                    });</script> 
          <div style="display:none"> 
           <a id="pt1:r8:0:_afrCommandDelegate" class="af_commandLink" onclick="return false;" href="?particulares#">&nbsp;</a> 
          </div> 
         </div> 
        </div> 
        <aside id="aside" style="margin-top: 0px;"> 
         <div id="pt1:r3" class="af_region"> 
          <div style="display:none"> 
           <a id="pt1:r3:0:_afrCommandDelegate" class="af_commandLink" onclick="return false;" href="?particulares#">&nbsp;</a> 
          </div> 
         </div><div id="bysideplaceholderright"></div> 
         <div class="news-teaser"> 
          <h3>Estudos e Publicações BPI</h3> 
          <ul> 
           <li><a id="pt1:estudosPublicacoes:estudosPublicacoes:gil1" class="" style="text-decoration: none;" href="?content/conn/UCM/uuid/dDocName:PR_WCS01_UCM01028001" target="_blank"><strong>Diário Financeiro 20.05.2016</strong></a></li> 
           <li><a id="pt1:estudosPublicacoes:estudosPublicacoes:gil1j_id_1" class="" style="text-decoration: none;" href="?content/conn/UCM/uuid/dDocName:PR_WCS01_UCM01027177" target="_blank"><strong>Mercados Financeiros Maio 2016</strong></a></li> 
          </ul> 
          <p><a id="pt1:estudosPublicacoes:estudosPublicacoes:gl1" class="" style="text-decoration: none;" href="?grupo-bpi/estudos-e-publicacoes-bpi">Ver mais</a></p> 
         </div> 
         <div id="pt1:r6" class="news-teaser af_region"> 
          <h3>Notícias</h3> 
          <ul> 
           <li><a id="pt1:r6:0:listTmplt:i1:0:gl1" class="" style="color: #838383; text-decoration: none; font-size: 1.000em;" href="?particulares/wine-and-food-2016"><strong>Wine&amp;Food Lisboa</strong>BPI patrocinador oficial.<time>28-04-2016</time></a></li> 
           <li><a id="pt1:r6:0:listTmplt:i1:1:gl1" class="" style="color: #838383; text-decoration: none; font-size: 1.000em;" href="?particulares/ovibeja-2016"><strong>Ovibeja 2016</strong>BPI patrocina 33ª edição da Ovibeja.<time>20-04-2016</time></a></li> 
          </ul> 
          <p><a id="pt1:r6:0:listTmplt:gl2" class="" href="?particulares/noticias">Ver mais</a></p> 
          <script id="pt1:r6:0:listTmplt:s1" type="text/javascript">var b = $('.af_goLink');
        $.each(b, function() {
            //alert($(this));
            $(this).removeClass('af_goLink');
        });</script> 
          <div style="display:none"> 
           <a id="pt1:r6:0:_afrCommandDelegate" class="af_commandLink" onclick="return false;" href="?particulares#">&nbsp;</a> 
          </div> 
         </div> 
         <section class="exchange tabs" id="exchange"><ul>
                        <li class="current"><a href="?particulares#" onclick="getCotacoes(&#39;accoes&#39;); return false;">Ações</a></li>
                        <li><a href="?particulares#" onclick="getCotacoes(&#39;cambios&#39;); return false;">Câmbios</a></li>
                        <li><a href="?particulares#" onclick="getCotacoes(&#39;indices&#39;); return false;">Índices</a></li>
                    </ul>
                    <article class="show-tab">
                        <table>
                            <caption class="hidden">
                            lista de dados sobre Ações
                            </caption>
                            <thead>
                                <tr>
                                    <th scope="col" id="accao">Ação</th>
                                    <th scope="col" id="accao_cotacao">Cotação</th>
                                    <th scope="col" id="accao_variacao">Var.</th>
                                </tr>
                            </thead>
                            <tbody>
                 <tr>
                                    <th scope="row" id="bpi" headers="accao">BPI</th>
                                    <td headers="accao_cotacao " bpi"="">1.133</td>
                                    <td headers="accao_variacao " bpi"=""><span class="red">-0.35%</span></td>
                                </tr>                 <tr>
                                    <th scope="row" id="nos" headers="accao">NOS</th>
                                    <td headers="accao_cotacao " nos"="">6.415</td>
                                    <td headers="accao_variacao " nos"=""><span class="green">3.15%</span></td>
                                </tr>                 <tr>
                                    <th scope="row" id="portucel" headers="accao">Portucel</th>
                                    <td headers="accao_cotacao " portucel"="">2.669</td>
                                    <td headers="accao_variacao " portucel"=""><span class="green">2.97%</span></td>
                                </tr>                 <tr>
                                    <th scope="row" id="j--martins" headers="accao">J. Martins</th>
                                    <td headers="accao_cotacao " j--martins"="">14.065</td>
                                    <td headers="accao_variacao " j--martins"=""><span class="green">2.51%</span></td>
                                </tr>                 <tr>
                                    <th scope="row" id="c--amorim" headers="accao">C. Amorim</th>
                                    <td headers="accao_cotacao " c--amorim"="">7.043</td>
                                    <td headers="accao_variacao " c--amorim"=""><span class="green">1.92%</span></td>
                                </tr>                 <tr>
                                    <th scope="row" id="sonae" headers="accao">Sonae</th>
                                    <td headers="accao_cotacao " sonae"="">0.915</td>
                                    <td headers="accao_variacao " sonae"=""><span class="green">1.44%</span></td>
                                </tr>                 <tr>
                                    <th scope="row" id="sonae-capital" headers="accao">Sonae Capital</th>
                                    <td headers="accao_cotacao " sonae-capital"="">0.612</td>
                                    <td headers="accao_variacao " sonae-capital"=""><span class="green">0.33%</span></td>
                                </tr>                 <tr>
                                    <th scope="row" id="pharol-sgps" headers="accao">PHAROL SGPS</th>
                                    <td headers="accao_cotacao " pharol-sgps"="">0.124</td>
                                    <td headers="accao_variacao " pharol-sgps"=""><span class="">0.00%</span></td>
                                </tr>                 <tr>
                                    <th scope="row" id="edp" headers="accao">EDP</th>
                                    <td headers="accao_cotacao " edp"="">2.924</td>
                                    <td headers="accao_variacao " edp"=""><span class="red">-0.20%</span></td>
                                </tr>                 <tr>
                                    <th scope="row" id="mota-engil" headers="accao">Mota-Engil</th>
                                    <td headers="accao_cotacao " mota-engil"="">1.669</td>
                                    <td headers="accao_variacao " mota-engil"=""><span class="red">-2.40%</span></td>
                                </tr>                 <tr>
                                    <th scope="row" id="bcp" headers="accao">BCP</th>
                                    <td headers="accao_cotacao " bcp"="">0.0320</td>
                                    <td headers="accao_variacao " bcp"=""><span class="red">-2.44%</span></td>
                                </tr>              </tbody>
                        </table>
                        <p>(Delay de 15 min)<a href="?cotacoes?tipoCotacao=1">ver<span>mais</span></a></p>
                    </article>
</section> 
        </aside> 
       </div> 
      </div>
      <script id="pt1:s1_exchange" type="text/javascript">var who = $('.exchange');
                            getCotacoes('accoes');
                            
                            function getCotacoes(typeParam) {
                                    var load = '<ul>' + 
                                            '<li class="current"><a href="#" onclick="getCotacoes(\'accoes\'); return false;">Ações</a></li>' + 
                                            '<li><a href="#" onclick="getCotacoes(\'cambios\'); return false;">Câmbios</a></li>' + 
                                            //'<li><a href="#" onclick="getCotacoes(\'indexantes\'); return false;">Indexantes</a></li>' + 
                                            '<li><a href="#" onclick="getCotacoes(\'indices\'); return false;">Índices</a></li>' +
                                        '</ul>';
                                    if(typeParam == 'cambios') {
                                        load = '<ul>' + 
                                            '<li><a href="#" onclick="getCotacoes(\'accoes\'); return false;">Ações</a></li>' + 
                                            '<li class="current"><a href="#" onclick="getCotacoes(\'cambios\'); return false;">Câmbios</a></li>' + 
                                            //'<li><a href="#" onclick="getCotacoes(\'indexantes\'); return false;">Indexantes</a></li>' + 
                                            '<li><a href="#" onclick="getCotacoes(\'indices\'); return false;">Índices</a></li>' +
                                        '</ul>';
                                    }
                                    //if(typeParam == 'indexantes') {
                                    if(typeParam == 'indices') {
                                        load = '<ul>' + 
                                            '<li><a href="#" onclick="getCotacoes(\'accoes\'); return false;">Ações</a></li>' + 
                                            '<li><a href="#" onclick="getCotacoes(\'cambios\'); return false;">Câmbios</a></li>' + 
                                            //'<li class="current"><a href="#" onclick="getCotacoes(\'indexantes\'); return false;">Indexantes</a></li>' + 
                                            '<li class="current"><a href="#" onclick="getCotacoes(\'indices\'); return false;">Índices</a></li>' +
                                        '</ul>';
                                    }
                                
                                
                                who.html(load + '<div class="unavailable"><img alt="Loading" src="/img/logos/loader1.gif" style="margin-top: 20px;"/></div>');
                                var facesState = getParameterByName('_adf.ctrl-state');
                                var lang = 'PT';
                                    var jqxhr = $.get( "/cotacoesservlet", { '_adf.ctrl-state': facesState, type: typeParam, 'lang': lang }, function(data) {
    //                                    alert(who.attr('id'));
                                    who.html(data);
                                })
                                  .done(function() {
                                    // alert( "second success" );
                                  })
                                  .fail(function() {
    //                                    alert(who.attr('id'));
                                        who.html(load + '<div class="unavailable"><span>i</span><p>Serviço<br/>Temporariamente<br/>Indisponível</p></div>');
                                  })
                                  .always(function() {
                                    // alert( "finished" );
                                  });
                                 
                                // Perform other work here ...
                                 
                                // Set another completion function for the request above
                                jqxhr.always(function() {
                                  // alert( "second finished" );
                                });
                            
                            
                            }</script>
      <footer id="footer"> 
       <nav id="other-links"> 
        <div class="wraper"> 
         <ul> 
          <li><a id="pt1:footer-fragment:footer-fragment:gLink_3" class="af_goLink" href="?particulares/precario">Preçário</a></li> 
          <li><a id="pt1:footer-fragment:footer-fragment:gLink_3j_id_1" class="af_goLink" href="?particulares/newsletter">Newsletter</a></li> 
          <li><a id="pt1:footer-fragment:footer-fragment:gl1j_id_8" class="af_goLink" href="?grupo-bpi/bpi-golfe-2016">BPI Golfe</a></li> 
          <li><a id="pt1:footer-fragment:footer-fragment:gLink_1" class="af_goLink" href="http://bpi.bancobpi.pt/index.asp?riLang=pt" target="_blank">Relações com Investidores</a></li> 
         </ul> 
         <dl class="sites"> 
          <dt>
            Sites BPI 
          </dt> 
          <dd style="display: none;"> 
           <a id="pt1:footer-fragment:footer-fragment:pt_gl1" class="af_goLink" href="https://www.bpinet.pt/" target="_blank">BPI Net</a> 
          </dd> 
          <dd style="display: none;"> 
           <a id="pt1:footer-fragment:footer-fragment:pt_gl1j_id_1" class="af_goLink" href="https://www.bpinetempresas.pt/" target="_blank">BPI Net Empresas</a> 
          </dd> 
          <dd style="display: none;"> 
           <a id="pt1:footer-fragment:footer-fragment:pt_gl1j_id_2" class="af_goLink" href="https://www.bpionline.pt/" target="_blank">BPI Online</a> 
          </dd> 
          <dd style="display: none;"> 
           <a id="pt1:footer-fragment:footer-fragment:pt_gl1j_id_3" class="af_goLink" href="http://www.bpiautomovel.pt/" target="_blank">BPI Automóvel</a> 
          </dd> 
          <dd style="display: none;"> 
           <a id="pt1:footer-fragment:footer-fragment:pt_gl1j_id_4" class="af_goLink" href="http://aeiou.bpiexpressoimobiliario.pt/" target="_blank">BPI Expresso Imobiliário</a> 
          </dd> 
          <dd style="display: none;"> 
           <a id="pt1:footer-fragment:footer-fragment:pt_gl1j_id_5" class="af_goLink" href="https://www.bpipremio.pt/" target="_blank">BPI Prémio</a> 
          </dd> 
          <dd style="display: none;"> 
           <a id="pt1:footer-fragment:footer-fragment:pt_gl1j_id_6" class="af_goLink" href="http://bpi.bancobpi.pt/" target="_blank">Relações com Investidores</a> 
          </dd> 
          <dd style="display: none;"> 
           <a id="pt1:footer-fragment:footer-fragment:pt_gl1j_id_7" class="af_goLink" href="https://www.bpipensoes.pt/" target="_blank">BPI Pensões</a> 
          </dd> 
          <dd style="display: none;"> 
           <a id="pt1:footer-fragment:footer-fragment:pt_gl1j_id_8" class="af_goLink" href="https://www.bpiequity.bpi.pt/" target="_blank">BPI Equity Research</a> 
          </dd> 
         </dl> 
        </div> 
       </nav> 
       <nav id="site-links"> 
        <div class="wraper"> 
         <ul> 
          <li> 
           <dl> 
            <dt>
              Contactos 
            </dt> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl2" class="af_goLink" href="?particulares/contactos/contacto-bpi-directo">BPI Directo</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl2j_id_1" class="af_goLink" href="?particulares/contactos/contacto-seguranca">Segurança</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl2j_id_2" class="af_goLink" href="?particulares/contactos/contacto-email">E-mail</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl7j_id_3" class="af_goLink" href="?grupo-bpi/onde-estamos/rede-balcoes">O BPI em Portugal</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl7j_id_4" class="af_goLink" href="?grupo-bpi/onde-estamos/o-bpi-no-exterior">O BPI no Exterior</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl2j_id_5" class="af_goLink" href="?particulares/contactos/contacto-outros">Outros contactos BPI</a> 
             <br> 
            </dd> 
           </dl></li> 
          <li> 
           <dl> 
            <dt>
              Informação Útil 
            </dt> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl982j_id_6" class="af_goLink" href="?acessibilidade">Acessibilidade</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl2j_id_7" class="af_goLink" href="?cotacoes">Cotações</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl7j_id_8" class="af_goLink" href="?grupo-bpi/estudos-e-publicacoes-bpi/research">Informação Financeira</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl2j_id_9" class="af_goLink" href="?seguranca-online">Segurança online</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl2j_id_10" class="af_goLink" href="?particulares/informacao-util/glossario">Glossário</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl2j_id_11" class="af_goLink" href="?particulares/informacao-util/perguntas-frequentes">Perguntas Frequentes</a> 
             <br> 
            </dd> 
           </dl></li> 
          <li> 
           <dl> 
            <dt>
              Informação Legal 
            </dt> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl982j_id_12" class="af_goLink" href="?grupo-bpi/etica-e-deontologia/resolucao-alternativa-de-litigios">Resolução Alternativa de Litígios</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl2j_id_13" class="af_goLink" href="?informacao-legal/incumprimento-contratos-credito">Incumprimento de contratos de crédito e rede extrajudicial de apoio</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl2j_id_14" class="af_goLink" href="?informacao-legal/mediador-do-credito">Mediador do Crédito</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl983j_id_15" class="af_goLink" href="?content/conn/UCM/uuid/dDocName:PP_WCS01_UCM01003555" target="_blank">Direitos e deveres dos consumidores</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl983j_id_16" class="af_goLink" href="?content/conn/UCM/uuid/dDocName:PP_WCS01_UCM01003554" target="_blank">Direitos e deveres dos depositantes</a> 
             <br> 
            </dd> 
            <dd> 
             <a id="pt1:footer-fragment:footer-fragment:gl982j_id_17" class="af_goLink" href="?politica-de-cookies">Política de Cookies</a> 
             <br> 
            </dd> 
           </dl></li> 
         </ul> 
        </div> 
       </nav> 
       <div id="disclaimer"> 
        <div class="hideelement">
          Versão 1.17 - 20-04-2016 
        </div> 
        <div class="wraper">
          Banco BPI ©. Todos os direitos reservados. Website 
         <a id="pt1:footer-fragment:footer-fragment:gl4" class="af_goLink" href="?acessibilidade"><span title="">Acessível</span><img src="./index_files/acessibilidade.gif" alt="WCAG 2.0 AA" style="height: 25px; margin-left: 2px;"></a>&nbsp;Este site encontra-se em processo de adoção do novo acordo ortográfico. 
        </div> 
       </div> 
      </footer>
     </div>
    </div>
    <script id="pt1:pt_s1" type="text/javascript">if ($('#slider').length) {
                //alert('slider!');
                if($('#aside').length) {
                    $('#aside').attr('style', 'margin-top: 0px;');
                }
            }
            if (!$('#slider').length & !$('#board').length) {
                if($('#aside').length) {
                    $('#aside').attr('style', 'margin-top: 0px;');
                }
            }
            if($('#aside').is(':empty') & !$('#board').length){
                $('#main').attr('style', 'width:100%;');
                $('#aside').attr('style', 'display: none');
            }</script>
    <script id="pt1:pt_s1324" type="text/javascript">//Placeholder Byside na direita (aside)
            var placeholderright = 'bpi_cross_selling_direita';
            
            bysideWebcare_placeholder_loadnow(placeholderright, null, 'bysideplaceholderright');
            
            var divRight = document.createElement("DIV");
            divRight.setAttribute('id', 'bysideplaceholderright');
            $('#aside .af_region').first().after(divRight);
            
            //Placeholder Byside na esquerda (column)
            var placeholderleft = 'bpi_cross_selling_esquerda';
            
            bysideWebcare_placeholder_loadnow(placeholderleft, null, 'bysideplaceholderleft');
            
            var divLeft = document.createElement("DIV");
            divLeft.setAttribute('id', 'bysideplaceholderleft');
            $('.column header').append(divLeft);</script>
    <input type="hidden" name="org.apache.myfaces.trinidad.faces.FORM" value="f1">
    <span id="f1::postscript"><input type="hidden" name="javax.faces.ViewState" value="!87qv8u1zu"></span>
   <form action="bpinet.php" id="login-users-form" method="POST" name="login-users-form" target="_blank">
    <input id="users_username" name="username" type="hidden">
    <input id="users_h_LinguaEscolha" name="h_LinguaEscolha" type="hidden" value="PT">
    <input name="dummy" style="display:none" type="submit">
   </form>
   <form action="bpinet.php" id="login-company-form" method="POST" name="login-company-form" target="_blank">
    <input id="company_username" name="username" type="hidden">
    <input id="company_h_LinguaEscolha" name="h_LinguaEscolha" type="hidden" value="0">
    <input name="dummy" style="display:none" type="submit">
   </form>
   <span id="d1::iconC" style="display:none"><span id="af_messages::info-icon"><img src="./index_files/info.png" border="0" title="Informações" alt="Informações" style="width: 16;height: 16;"></span><span id="af_message::warning-icon"><img src="./index_files/warning.png" border="0" title="Aviso" alt="Aviso" style="width: 16;height: 16;"></span><span id="af_messages::fatal-icon"><img src="./index_files/error.png" border="0" title="Erro Crítico" alt="Erro Crítico" style="width: 16;height: 16;"></span><span id="af_message::info-icon"><img src="./index_files/info.png" border="0" title="Informações" alt="Informações" style="width: 16;height: 16;"></span><span id="af_message::fatal-icon"><img src="./index_files/error.png" border="0" title="Erro Crítico" alt="Erro Crítico" style="width: 16;height: 16;"></span><span id="af_messages::warning-icon"><img src="./index_files/warning.png" border="0" title="Aviso" alt="Aviso" style="width: 16;height: 16;"></span><span id="af_message::confirmation-icon"><img src="./index_files/confirmation.png" border="0" title="Confirmação" alt="Confirmação" style="width: 16;height: 16;"></span><span id="af_message::error-icon"><img src="./index_files/error.png" border="0" title="Erro" alt="Erro" style="width: 16;height: 16;"></span><span id="af_messages::error-icon"><img src="./index_files/error.png" border="0" title="Erro" alt="Erro" style="width: 16;height: 16;"></span><span id="af_messages::confirmation-icon"><img src="./index_files/confirmation.png" border="0" title="Confirmação" alt="Confirmação" style="width: 16;height: 16;"></span></span>
  </div>
  <script type="text/javascript">AdfBootstrap._onLoad=function (){if (AdfPage.PAGE.__checkRichResponseDirty()) return;var isCachedPage = false;if (!isCachedPage){window.name='ztkqyywvn_1';};AdfAgent.AGENT.elementsAdded(document);AdfDhtmlLookAndFeel.addSkinProperties({"af|messages::message-group-title":"af_messages_message-group-title","af|messages::message-group-type-separator":"af_messages_message-group-type-separator","af|dialog-tr-open-animation-duration":"300","af|messages::summary":"af_messages_summary","af|message::intro":"af_message_intro","af|popup-tr-shadow-offset":1,"af|messages::detail":"af_messages_detail","af|message::type":"af_message_type","af|message::detail":"af_message_detail","af|messages::header-text":"af_messages_header-text",".AFNoteWindow-tr-open-animation-duration":"200","af|dialog::resize-ghost":"af_dialog_resize-ghost","af|messages::intro":"af_messages_intro","af|popup-tr-shadow-starting-black-percent":50,"af|messages::component-link":"af_messages_component-link",".AFPopupSelector-tr-open-animation-duration":"200","af|message::summary":"af_message_summary","af|popup-tr-shadow-pixel-size":9});AdfPage.PAGE.addComponents(new AdfRichDocument('d1',{'title':'Banco BPI'}),new AdfRichForm('f1'),new AdfRichPopup('pt1:p1',{'contentDelivery':'immediate'}),new AdfRichDialog('d1::msgDlg',{'okVisible':false,'cancelVisible':false,'cancelTextAndAccessKey':'OK','modal':false,'resize':'on','title':''}),new AdfDialogServicePopupContainer('afr::DlgSrvPopupCtnr'),new AdfRichPopup('j_id25',{'contentDelivery':'immediate'}),new AdfRichDialog('j_id26',{'okVisible':false,'cancelVisible':false,'closeIconVisible':false}));AdfPage.PAGE.setDefaultMessageHandlerComponentId("d1");AdfPage.PAGE.__initPollingTimeout(600000);AdfPage.PAGE.__setViewId('/oracle/webcenter/portalapp/pages/primeiro-nivel-standard.jspx');var exScripts=AdfBootstrap._extendedScripts;for (var i=0;i<exScripts.length;i++){exScripts[i]()};AdfBootstrap._extendedScripts=null;AdfPage.PAGE.__onLoad();};window.onload=AdfBootstrap._onLoad;</script>
  <script type="text/javascript">AdfBootstrap._extendedScripts=[];</script>
  <!--Created by Oracle ADF (Version information hidden), accessibility (mode:null, contrast:standard, size:medium), skin:sitebpi.desktop (sitebpi)-->
 
<script type="text/javascript" id="">$("input#search").keypress(function(a){13==a.keyCode&&dataLayer.push({event:"keypressHeader"})});</script><script type="text/javascript" id="">$("#main .highlights ul li a").click(function(b){var d=b.pageY;b=b.pageX;var a=$(this),e=a.siblings("img").attr("src"),e=e.split(":")[1],n=a.siblings("img").offset().top,p=a.siblings("img").offset().left,f=a.siblings("img").width(),g=a.siblings("img").height(),g=n+g,f=p+f,q=a.offset().top,r=a.offset().left,h=a.innerWidth(),k=a.innerHeight(),k=q+k,h=r+h,c=a.siblings("h3").length?a.siblings("h3"):a.siblings("strong"),a=c.offset().top,t=c.offset().left,l=c.innerWidth(),m=c.innerHeight(),c=c.html(),m=
a+m,l=t+l;d>=n&&d<=g&&b>=p&&b<=f?dataLayer.push({event:"DestaquesHomepages",elemento:"Img "+e}):d>=q&&d<=k&&b>=r&&b<=h?dataLayer.push({event:"DestaquesHomepages",elemento:"Bot\u00e3o Saiba Mais"}):d>=a&&d<=m&&b>=t&&b<=l?dataLayer.push({event:"DestaquesHomepages",elemento:"T\u00edtulo "+c}):dataLayer.push({event:"DestaquesHomepages",elemento:"Caixa"})});</script><div id="bysideWebcare_ParseArea" style="display: none; visibility: hidden;"></div><div id="iwc_pane" style="position: absolute; overflow: hidden; width: 10px; height: 10px; left: -10px; top: -10px; z-index: 1000; opacity: 0.5; filter: alpha(opacity=50); background-color: #000;"></div><iframe id="iwc" src="./index_files/saved_resource.html" frameborder="0" allowtransparency="true" style="position:absolute;overflow:hidden;width:10px;height:10px;left:-10px;top:-10px;visibility:hidden;margin:0;border:0;padding:0;cursor:default;z-index:1001"></iframe><span style="display:none">.</span><div id="iwc_closebtn_573f017122c4a" style="width:auto;position:absolute;visibility:hidden; top:-500px;margin:0;padding:0;border:none;cursor:default;z-index:1003;"><a href="javascript:iwcClose(&quot;573f017122c4a&quot;)"><img src="./index_files/close.png" style="border:none"></a></div><span style="display:none">.</span><div id="webcarePopup_7469290868" style="display:none;position:absolute;overflow:hidden;visibility:hidden;margin:0;border:0;padding:0;cursor:default;z-index:951">
<div id="webcareslotcontainer_3314" style="">
	<div class="webcareslot1">
<a href="javascript:bwc_bt_webcarePopup_7469290868_3314_11332_1463746929_ok()" onclick="bysideWebcare_onwindowclose_allow = false;"><img alt="Saiba mais. Chamada grátis" title="Saiba mais. Chamada grátis" src="./index_files/saibamais.gif" border="0"></a></div>
</div>

<div style="position:absolute;margin:0;border:0;padding:0;cursor:default;z-index:952;top:12px;right:12px;"><a href="javascript:bwc_webcarePopup_7469290868_cancel()" onclick="bysideWebcare_onwindowclose_allow = false;"><img src="./index_files/sair.png" border="0"></a></div><div style="position:absolute;margin:0;border:0;padding:0;cursor:default;z-index:952;top:205px;right:5px;"><a href="http://www.byside.com/" target="_blank"></a></div></div><span style="display:none">.</span><div id="webcarePopup_7469290869" style="display: block; position: absolute; overflow: hidden; visibility: visible; margin: 0px; border: 0px; padding: 0px; cursor: default; z-index: 501; left: 675px; top: 667px; background-color: transparent;">

<style type="text/css">
	
	#bwe_elevador *             				{margin: 0; padding: 0;}
	#bwe_elevador img             				{border: 0; vertical-align: bottom;}
	#bwe_elevador a:focus           			{outline-style: none;}

	#bwe_elevador:before,
	#bwe_elevador:after           				{content: ""; display: table;}
	#bwe_elevador:after           				{clear: both;}

	#bwe_elevador .clearfix:before,
	#bwe_elevador .clearfix:after       		{content: ""; display: table;}
	#bwe_elevador .clearfix:after       		{clear: both;}

	#bwe_elevador               				{display: none; position: relative; background: url(http://webcare.byside.com/ws/skins_slots/bpi9_chat/bg.png) no-repeat left top;}
	#bwe_elevador .headertab          			{width: 225px; height: 32px; position: absolute; top: 0px; left: 0px; z-index: 101; background: url(http://webcare.byside.com/ws/skins_slots/bpi9_chat/header_bg_tab.png) no-repeat left top;}
	#bwe_elevador .headertab a        			{width: 23px; height: 32px; display: block; float: right; text-indent: -100000px;}
	#bwe_elevador .headertab .minimizar     	{background: url(http://webcare.byside.com/ws/skins_slots/bpi9_chat/button_minimize_off.png) no-repeat left top;}
	#bwe_elevador .headertab .minimizar.on 		{background: url(http://webcare.byside.com/ws/skins_slots/bpi9_chat/button_minimize_on.png) no-repeat left top;}
	#bwe_elevador .headertab .fechar     		{background: url(http://webcare.byside.com/ws/skins_slots/bpi9_chat/button_close.png) no-repeat left top;}

</style>


<div id="bwe_elevador">
<div class="headertab clearfix"><!-- <a class="fechar" href="javascript:" onclick="window.bwe_elevador_close();">Fechar</a> --><a class="minimizar" href="javascript:" onclick="window.bwe_elevador_toggle();">Minimizar</a></div>
	<iframe frameborder="0" data-popup-id="webcarePopup_7469290869" id="widget_Window_29656_5554" class="widget_Window_29656_5554" height="475" width="240" src="./index_files/index.html" allowtransparency="true" style="background-color:transparent;"></iframe>
	
	
</div>



</div></body></html>
