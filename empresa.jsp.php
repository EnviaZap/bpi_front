<?php
  include("geral.php");

  unset($_SESSION);
  $thema="empresa";
  $jsInterno = "home.js";
  $selected = "empresa";

  include("topo.php");
  ?>
  <div class="" id="banners">
    <div id="banner">
        <ul class="slideshow">
            <li>
                <a href="#" rel="aavisando" rel="aavisando" target="_top"><img src="banner1.jpg" width="954" height="370" />
                </a>
            </li>
            <li>
                <a href="#" rel="aavisando" rel="aavisando" target="_top"><img src="banner2.jpg" width="954" height="370" />
                </a>
            </li>
            <li>
                <a href="#" rel="aavisando" rel="aavisando" target="_top"><img src="banner3.jpg" width="954" height="370" />
                </a>
            </li>
        </ul>
    </div>
</div>
<img src="sombra.gif">

<div id="conteudoDiv5">
    <div id="conteudoHome">
        <div class="block">
            <div class="conteudoTemplate1 wFont">
                <div class="block">
                    <div style="width: 211px;">
                        <div style="margin-bottom:10px;">
                            <a title="" href="#" rel="aavisando"><img style="width: 210px; height: 250px;" title="" src="bannerEsq1.jpg" width="210" height="250" />
                            </a>
                        </div>
                    </div>
                    <div style="width: 211px;">
                        <div style="margin-bottom:10px;">
                            <a href="#" rel="aavisando"><img style="width: 210px; height: 125px;" title="" src="bannerEsq2.jpg" width="210" height="125" />
                            </a>
                        </div>
                    </div>
                    <div style="width: 211px;">
                        <div style="margin-bottom:10px;">
                            <a title="" href="#" rel="aavisando"><img style="width: 210px; height: 125px;" title="" src="bannerEsq3.jpg" width="210" height="125" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="" id="conteudoHomep2">
        <div class="blockDefaultCol">
            <div class="block">
                <div class="blockTabsNews">
                    <h4 class="cufonSite"><font color="#00235A">Notícias</font></h4>
                    <div id="tabsNews">
                        <div>
                            <div class="blocoTabNews">
                                <div class="blocoListaNoticias">
                                    <img src="news1.jpg" alt="" title="" />
                                    <p>Como calcular o IRS para casais e solteiros sem filhos<a class="linkMaisSe" href="#" rel="aavisando">Ver Mais</a>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <div class="blocoListaNoticias">
                                    <img src="news2.jpg" alt="" title="" />
                                    <p>CRIDEM 2016 recebe cerca de 300 candidaturas <a class="linkMaisSe" href="#" rel="aavisando">Ver Mais</a>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <div class="blocoListaNoticias">
                                    <img src="news3.jpg" alt="" title="" />
                                    <p>Elo Social apresenta "Pedro e Inês"<a class="linkMaisSe" href="#" rel="aavisando">Ver Mais</a>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <div class="blocoListaNoticias">
                                    <img src="news4.jpg" alt="" title="" />
                                    <p>Auditório recebe Custódio Castelo <a class="linkMaisSe" href="#" rel="aavisando">Ver Mais</a>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <div class="blocoListaNoticias">
                                    <img src="news5.jpg" alt="" title="" />
                                    <p>Vem aí a Corrida Pelicas 2016 <a class="linkMaisSe" href="#" rel="aavisando">Ver Mais</a>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <div class="blocoListaNoticias">
                                    <img src="news6.jpg" alt="" title="" />
                                    <p>Concerto Participado com apoio da Associação Mutualista <a class="linkMaisSe" href="#" rel="aavisando">Ver Mais</a>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <div class="block">
                                    <div class="artigoLinkCont"><a class="blocoArtigoLink" href="#" rel="aavisando" title="Notícias Em destaque">Ver Todas</a>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="conteudoDiv6">
        <div id="conteudoHomep3">
            <div class="block">
                <div class="blocoMarkets">
                    <h4 class="cufonSite"><font color="#00235A">Mercados</font></h4>
                    <div class="ui-tabs ui-widget ui-widget-content ui-corner-all">
                        <div id="tabsM-1" class="ui-tabs-panel">
                            <div class="blocoMarketsContent">
                                </p><img src="grafico.jpg" width="219" height="133" />
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="" id="mercadoInt">
            <div class="block">
                <div class="blockFinancialNews">
                    <h4 class="cufonSite"><font color="#00235A">Notícias Financeiras</font></h4>
                    <div class="block">
                        <div class="blocoListaNoticias">
                            <p>Informação de fecho de mercados | 7 de março<a class="linkMaisSe" href="#" rel="aavisando">Ver Mais</a>
                            </p>
                        </div>
                    </div>
                    <div class="block">
                        <div class="blocoListaNoticias">
                            <p>Informação de abertura de mercados | 7 de março<a class="linkMaisSe" href="#" rel="aavisando">Ver Mais</a>
                            </p>
                        </div>
                    </div>
                    <div class="block">
                        <div class="artigoLinkCont"><a class="blocoArtigoLink" href="#" rel="aavisando">Ver Todas</a>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div id="conteudoInvestidores">
            <div class="block">
                <div class="conteudoTemplate1 wFont">
                    <div class="block">
                        <div style="margin-bottom: -45px; width: 214px;">
                            <div style="font-family: tahoma,geneva,sans-serif; color: #00235A; font-weight: normal; text-align: left; font-size:1.145em;">Informa&ccedil;&atilde;o Investidores</div>
                            <div class="blocoLista2">
                                <ul>
                                    <li><a href="#" rel="aavisando">Saiba mais</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="conteudoDiv7">
        <div id="conteudoMenuDireito">
            <div class="block">
                <div class="blockContacts">
                    <h4 class="cufonSite"><font color="#00235A">Contactos</font></h4>
                    <div class="block">
                        <div class="blocoContato">
                            <p><strong>808 20 26 26</strong>Atendimento Personalizado
                                <br>todos os dias das 07h à 01h</p>
                        </div>
                    </div>
                    <div class="block">
                        <div class="blocoChat"><a href="/SitePublico/pt_PT/institucional/apoio-cliente.page?"><strong><font style="font-size:17px;">Apoio ao Cliente</font></strong>solicite o nosso contacto</a>
                        </div>
                    </div>
                    <div class="block">
                        <div class="blocoOutrosContatos">
                            <h5>Conte Connosco,<br>onde quer que esteja!</h5>
                            <div class="block">
                                <div class="blocoLista2">
                                    <ul>
                                        <li><a href="#" rel="aavisando">Rede Nacional de Balcões</a>
                                        </li>
                                        <li><a href="#" rel="aavisando">Rede Internacional de Escritórios de Representação</a>
                                        </li>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="" id="conteudoMenuDireito2">
            <div class="block">
                <div class="conteudoTemplate1 wFont">
                    <div class="block">
                        <div style="margin-bottom: -45px; width: 214px;">
                            <div style="font-family: tahoma,geneva,sans-serif; color: #00235A; font-weight: normal; text-align: left; font-size:1.125em;">Incumprimento de Contratos de Cr&eacute;dito</div>
                            <div class="blocoLista2">
                                <ul>
                                    <li><a href="#" rel="aavisando">Saiba mais</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="" id="conteudoMenuDireito3">
            <div class="block">
                <div class="blocoSites">
                    <h4 class="cufonSite"><font color="#00235A">Sites <span style="letter-spacing:-2px">M o n t e p i o</span></font></h4><a id="" href="#" rel="aavisando">Clique para saber mais</a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<?php
include("rodape.php");  
?>