
function Client_SendBizCase(sTX, sINPUT, trataResp){
    'use strict';
    $.ajax({
        data:sINPUT,
        url: "/MCF/toMCF_server.asp?TX=" + sTX,
        type: "POST",
        contentType:"application/xml; charset=utf-8",
        dataType:'json',
        cache:false,
        success:function(json){
            trataResp(json);
        },
        error:function(json,status){
            //window.alert(status);
        }
        });
}

function validaBicAderente(json) {
    'use strict';
   
    var result,conta,moeda,isAnEuropeanBic;

    result = json.output.split(";")[1];
 
    validaDirectiva = true;
    isAnEuropeanBic = result == "S";
   
    conta = document.getElementById("contaCorrente").options[document.getElementById("contaCorrente").selectedIndex].value;
    conta = conta.split('|')[2];
    moeda = document.getElementById("cbx_moeda").options[document.getElementById("cbx_moeda").selectedIndex].value;

    validaDirectiva = isAnEuropeanBic;

    document.getElementById("optSHA").style.display = "block";
    document.getElementById("optOUR").style.display = "block";
    document.getElementById("optBEN").style.display = "block";

    document.getElementById("optSHA").disabled = false;
    document.getElementById("optOUR").disabled = false;
    document.getElementById("optBEN").disabled = false;

    if (isAnEuropeanBic && moeda === "GBP" && conta === "GBP") {
        
        document.getElementById("optSHA").style.display = "block";
        document.getElementById("optOUR").style.display = "none";
        document.getElementById("optBEN").style.display = "none";

        document.getElementById("optSHA").disabled = false;
        document.getElementById("optOUR").disabled = true;
        document.getElementById("optBEN").disabled = true;
    }

    if (isAnEuropeanBic && moeda === "EUR" && (conta == "USD" || conta === "GBP")) {
        
        document.getElementById("optSHA").style.display = "block";
        document.getElementById("optOUR").style.display = "block";
        document.getElementById("optBEN").style.display = "none";

        document.getElementById("optSHA").disabled = false;
        document.getElementById("optOUR").disabled = false;
        document.getElementById("optBEN").disabled = true;
    }



    var option = document.getElementById("cbx_Despesas").options[document.getElementById("cbx_Despesas").selectedIndex].id;
    
    if (document.getElementById(option).disabled) {
    
        document.getElementById("cbx_Despesas").options[0].selected = true;
    }
    
    return true;

}

function trataResp(json){
    'use strict';
    var strDspOutput, RespostaVB, strBicSwift, ContaDeb, moedaOri, moedaDes, strDspInput, strTipoDespOper;

    if (json)
    {
        RespostaVB = json.output.split(";");

        if (RespostaVB[0] === "ERRO") {
            //alert('<%=RetornaValor(oXmlLinguagem, "msgDspError")%>');
            window.alert('Erro na obten��o da DSP.');
            //alert("Erro na obten��o da DSP.");
            return false;
        } else {
            strDspOutput = RespostaVB[1];
        }
    }

    //strBicSwift = document.Form_EmissaoOPE.txt_SwiftBic.value.toUpperCase();
    ContaDeb = document.getElementById("contaCorrente").options[document.getElementById("contaCorrente").selectedIndex].value;

    moedaOri = ContaDeb.split("|")[2];
    if (moedaOri === "") {
        moedaOri = "EUR";
    }
    

    moedaDes = document.getElementById("cbx_moeda").options[document.getElementById("cbx_moeda").selectedIndex].value;
    //strDspInput = "OCEE" + strBicSwift.substring(4, 6) + moedaDes;
    strTipoDespOper = document.getElementById("cbx_Despesas").options[document.getElementById("cbx_Despesas").selectedIndex].value;
        
     //if ((moedaOri !== moedaDes) && (strDspOutput === "S") && (strTipoDespOper === "OUR" || strTipoDespOper === "SHA")) {
        //Os tipos de Regime de Despesas a disponibilizar dependem do tipo de transac��o a efectuar:
        //1. Transfer�ncias COM cambial (moeda da conta diferente da moeda da transac��o) ao abrigo da DSP permitem:
        //- OUR //- SHA
        //document.Form_EmissaoOPE.submit();
        //return true;        
    //}

    //Regra2
    //if ((moedaOri === moedaDes) && (strDspOutput === "S") && (strTipoDespOper === "SHA")) {
        //2. Transfer�ncias SEM cambial (moeda da conta igual � moeda da transac��o) ao abrigo da DSP permitem apenas:
        //- SHA
        //document.Form_EmissaoOPE.submit();
        //return true;        
    //}

    //Regra3
    //if (strDspOutput === "N") {
        //3. Transfer�ncias n�o ao abrigo da DSP permitem qualquer regime:
        //- OUR - SHA - BEN
        //document.Form_EmissaoOPE.submit();
        //return true;        
    //}

    //Opera��o n�o abrangida pela Dsp

    if (strDspOutput === "N") {
        window.alert('Tipo de despesas n�o ao abrigo da DSP.');

        return false;
    }
    return true;
}