var XML = {};

/**
 * Create a new Document object.  If no arguments are specified, 
 * the document will be empty.  If a root tag is specified, the document
 * will contain that single root tag.  If the root tag has a namespace
 * prefix, the second argument must specify the URL that identifies the
 * namespace.
 */

XML.newDocument = function (rootTagName, namespaceURL) {
    'use strict';
    if (!rootTagName) { rootTagName = ""; }
    if (!namespaceURL) { namespaceURL = ""; }

    if (document.implementation && document.implementation.createDocument) {
        // This is the W3C standard way to do it
        return document.implementation.createDocument(namespaceURL,
                                                      rootTagName, null);
    }
    else { // This is the IE way to do it
        // Create an empty document as an ActiveX object
        // If there is no root element, this is all we have to do
        var doc = new ActiveXObject("MSXML2.DOMDocument");

        // If there is a root tag, initialize the document
        if (rootTagName) {
            // Look for a namespace prefix
            var prefix = "", tagname = rootTagName, p = rootTagName.indexOf(':') ;
           
            if (p != -1) {
                prefix = rootTagName.substring(0, p);
                tagname = rootTagName.substring(p + 1);
            }

            // If we have a namespace, we must have a namespace prefix
            // If we don't have a namespace, we discard any prefix
            if (namespaceURL) {
                if (!prefix) { prefix = "a0"; } // What Firefox uses
            }
            else { prefix = ""; }

            // Create the root element (with optional namespace) as a
            // string of text
            var text = "<" + (prefix ? (prefix + ":") : "") + tagname + (namespaceURL ? (" xmlns:" + prefix + '="' + namespaceURL + '"'): "") + "/>";
            // And parse that text into the empty document
            doc.loadXML(text);
        }
        return doc;
    }
};

/**
 * Parse the XML document contained in the string argument and return
 * a Document object that represents it.
 */
XML.parse = function(text) {
    'use strict'; 
    if (typeof DOMParser != "undefined") {
        // Mozilla, Firefox, and related browsers
        return (new DOMParser()).parseFromString(text, "application/xml");
    }
    else if (typeof ActiveXObject != "undefined") {
        // Internet Explorer.
        var doc = XML.newDocument( );   // Create an empty document
        doc.loadXML(text);              //  Parse text into it
        return doc;                     // Return it
    }
    else {
        // As a last resort, try loading the document from a data: URL
        // This is supposed to work in Safari. Thanks to Manos Batsis and
        // his Sarissa library (sarissa.sourceforge.net) for this technique.
        var url = "data:text/xml;charset=utf-8," + encodeURIComponent(text), request = new XMLHttpRequest();;
         
        request.open("GET", url, false);
        request.send(null);
        return request.responseXML;
    }
}; 

/**
 * Serialize the XML document or node contained in object argument and return 
 * a string that represents it. 
 */
XML.serialize = function (node) {
    'use strict';
    var nodestr = "";
    if (!node) { return nodestr; }

    if (typeof ActiveXObject != "undefined") {
        nodestr = node.xml;
    }
    else if (typeof XMLSerializer != "undefined") {
        // create serializer object
        var xmlSerializer = new XMLSerializer();
        // serialize
        nodestr = xmlSerializer.serializeToString(node);
    }
    else if (document.implementation && document.implementation.createLSSerializer) {
        // create serializer object
        var xmlSerializer = document.implementation.createLSSerializer();
        // serialize
        nodestr = xmlSerializer.writeToString(xmlDocument);
    }

    return nodestr;

};

XML.loadDataIsland = function (dtId) {
    'use strict'; 
    if (!document.getElementById(dtId)){
        return '';
    };
    
    if (typeof DOMParser != "undefined") {
        return XML.parse(document.getElementById(dtId).innerHTML);
    }else{
         return document.getElementById(dtId).XMLDocument;
    };
};

XML.createElementNS = function (namespaceURI, elementName, elementValue, ownerDocument) {
    'use strict';
    var element = null;
    if (!ownerDocument) { return null; }
    if (!elementName) { return null; }
    if (!namespaceURI) { namespaceURI = ""; }

    if (typeof ownerDocument.createElementNS != 'undefined') {
        element = ownerDocument.createElementNS(namespaceURI, elementName);
    }
    else if (typeof ownerDocument.createNode != 'undefined') {
        element = ownerDocument.createNode(1, elementName, namespaceURI);
    };
    if (element && elementValue != "") {
        var textNode = ownerDocument.createTextNode(elementValue);
        element.appendChild(textNode);
    };
    return element;
};
