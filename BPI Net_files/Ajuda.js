/*jslint newcap:true */
/*global PeqNegocio, Linguagem, ConstroiArray:false */var gif;

function tiraCaminhoEParams(sPagina) {
    'use strict';
	var s, iTam, i, sAux, sResultado;
	sAux = "";
    
    s = sPagina + sAux;
    iTam = s.length;    
    sResultado = "";

    for (i = iTam - 1; i >= 0; i = i - 1) {
        if (s.charAt(i) !== "/") {
            sAux = s.charAt(i) + sAux;
        } else {
            break;
        }
    }
    iTam = sAux.length;

    for (i = 0; i < iTam; i = i + 1) {
        if (sAux.charAt(i) !== ".") {
            sResultado += sAux.charAt(i);
        } else {
            break;
        }
    }

    return sResultado;
}

function WhatGif(pag) {
    'use strict';
    var str, aux, paghref, vector, paggif;
    aux = [];
    paghref = pag.href;
    vector = paghref.split("&");
    paggif = vector[1];

    //Retira os 4 primeiros caracteres da string
    //que dever�o conter "gif="
    if (paggif.substr(0, 4) === "gif=") {
        str = paggif.substr(4, (paggif.length - 4));
        aux = ConstroiArray(str);
    } else {
        aux[0] = "";
    }
    return aux[0];
}

//Fun��o para construir um array a partir de uma string, em que
//o caracter & marca os limites
function ConstroiArray(str) {
    'use strict';
    var res, pos, aux, i;
    res = [];
    aux = str;

    i = 0;
    while ((pos = aux.search("&")) > -1) {
        res[i] = aux.substr(0, pos);
        aux = aux.substr(pos + 1, aux.length);
        i = i + 1;
    }
    res[i] = aux;
    return res;
}

//funcao para ir buscar o tipo de transferencia para as consultas e alteracao/cancelamento, pois � usado pelas nacionais e estrangeiro
function getTipoTransferencia() {
    'use strict';
    var tamanho;
    tamanho = window.parent.frames.INF_AREA.location.toString().length;
    return window.parent.frames.INF_AREA.location.toString().substr(tamanho - 1, 1);
}


function helpLocAux() {
    'use strict';
    var pagCentral, opHelp, new_window, tipoTransf;

    pagCentral = tiraCaminhoEParams(window.parent.frames.INF_AREA.location);
    opHelp = "";

    switch (pagCentral) {
        //Menu de Consulta  
        case 'extratoIntegrado':
            opHelp = "PosiIntegrada.htm"; //Posi��o Integrada
            break;
        case 'extratoIntegradohist':
            opHelp = "PosiIntegradaMensal.htm"; //Posi��o Integrada Mensal
            break;
        case 'PosIntegradaOIC':
            opHelp = "PosiIntegradaFranca.htm"; //Posi��o Integrada Fran�a
            break;
        case 'ExtractoDigitalLista':
            opHelp = "ExtractoDigitalCons.htm"; //Consulta Extracto Digital
            break;
        case 'Movimentos':
        case 'movimentosIeA':
            opHelp = "MOVIMENTOS.htm"; //Movimentos
            break;
        case 'saldos':
            opHelp = "SALDOS.htm"; //Saldos
            break;
        case 'nib':
            opHelp = "NIB.htm"; //NIB
            break;
        case 'ChequesCancelados':
            opHelp = "ConsCheqCanc.htm"; //Cheques Cancelados
            break;
        case 'MovsCartoesDebitoLista':
        case 'MovsCartoesDebito':
            opHelp = "ConsMovCartDeb.htm"; //Movs.Cartoes Debito
            break;
        //Menu C.Ordem Operacoes  
        case 'TransfContaBPI':
        case 'TransfContaBPIConf':
        case 'TransfContaBPIOpEfect':
            if (PeqNegocio === "S") { //Transfer�ncias Internas
                opHelp = "TransfContasBPI_PN.htm";
            } else {
                opHelp = "TransfContasBPI.htm";
            }
            break;
        case 'TransfNib':
        case 'TransfNibConf':
        case 'TransfNibOpEfect':
        case 'transferenciaIntOpEfect':
            if (PeqNegocio === "S") { //Transfer�ncias Interbancarias
                opHelp = "TransfNIB_PN.htm";
            } else {
                opHelp = "TransfNIB.htm";
            }
            break;

        case 'TransfIBAN':
        case 'TransfIBANConf':
        case 'TransfIBANOpEfect':
            if (PeqNegocio === "S") { //Transfer�ncias IBAN
                opHelp = "TransfIBAN_PN.htm";
            } else {
                opHelp = "TransfIBAN.htm";
            }
            break;

        case 'TransfUrgente':
        case 'TransfUrgenteConf':
        case 'TransfUrgenteOpEfect': opHelp = "contasordem_urgentes.htm"; //Transfer�ncias Urgentes
            break;

        case 'TransfBeneficiarios':
        case 'TransfBeneficiariosConf':
        case 'TransfBeneficiariosOpEfect':
            if (PeqNegocio === "S") { //Transfer�ncias Beneficiarios
                opHelp = "TransfBeneficiario_PN.htm";
            } else {
                opHelp = "TransfBeneficiario.htm";
            }
            break;

        case 'TransfMoedaEstr':
        case 'TransfMoedaEstrConf':
        case 'TransfMoedaEstrOpEfect': opHelp = "TransfMoedaEstr.htm"; //Transfer�ncias Moeda Estrangeira
            break;

        // Manuten��o das Agendas de Transferencias  

        case 'ListaAgendaActiva':
            //		case 'ListaAgendaHist':

            switch (WhatGif(window.parent.frames.INF_AREA.location)) {
                case 'Consulta':
                    tipoTransf = getTipoTransferencia();
                    if (tipoTransf == "N") {
                        opHelp = "TransfConsultasNacionais.htm"; //Consultas Nacionais
                    }
                    else {
                        opHelp = "TransfConsultasEstrangeiro.htm"; //Consultas Estrangeiro
                    }
                    break;
                case 'agendaconsulta': opHelp = "Agenda_Consulta.htm";
                    break;
                default: opHelp = "help.asp";
            }
            break;


        case 'ListaTransfRecebida':
        case 'DetalheTransfRecebida':
        case 'ComprovativoTransf':
        case 'ListaTransferenciasEfectuadas':
        case 'DetalheTransfEfect':
        case 'DetalheTransfEfectPDF':
            tipoTransf = getTipoTransferencia();
            if (tipoTransf == "N") {
                opHelp = "TransfConsultasNacionais.htm"; //Consultas Nacionais
            }
            else {
                opHelp = "TransfConsultasEstrangeiro.htm"; //Consultas Estrangeiro
            }
            break;

        case 'ListaCancelaAgenda':
        case 'ListaAlteracaoAgenda':
            tipoTransf = getTipoTransferencia();
            if (tipoTransf == "N") {
                opHelp = "TransfNacAltCancelamento.htm"; //Altera��o / Cancelamento nacionais
            }
            else {
                opHelp = "TransfEstAltCancelamento.htm"; //Altera��o / Cancelamento estrangeiro
            }
            break;

        // ADC		  
        case 'CriacaoADC':
        case 'CriacaoADCConf':
        case 'CriacaoADCOpEfect': opHelp = "ADC_Criacao.htm"; //Cria��o/Alt.Domicilio
            break;

        case 'AlteraLimitesLista':
        case 'AlteraLimites':
        case 'AlteraLimitesConf':
        case 'AlteraLimitesOpEfect': opHelp = "ADC_AltLimites.htm"; //Alt.Limites
            break;

        case 'CancelaADCLista':
        case 'CancelaADCConf':
        case 'CancelaADCOpEfect': opHelp = "ADC_Cancelamento.htm"; //Cancelamento
            break;

        case 'ConsultaADCDet':
        case 'ConsultaADCLista':
        case 'ConsultaADCPendDet':
        case 'ConsultaADCPendLista': opHelp = "ADC_Consulta.htm"; //Consulta
            break;

        // Cheques		  
        case 'ReqCheques':
        case 'ReqChequesConf':
        case 'ReqChequesOpEfect': opHelp = "ReqCheq.htm"; //Requisi��o de Cheques
            break;
        case 'ConsultaCheques':
        case 'DetCheques':
        case 'ListaEventosCheques':
        case 'ListaModulosCheques':
        case 'ListaReqCheques':
        case 'ListaCheques': opHelp = "ConsCheqCanc.htm"; //Consulta de Cheques
            break;
        case 'CanChequesLista':
        case 'CancelamentoCheques':
        case 'CancelamentoChequesConf':
        case 'CancelamentoChequesOpEfect': opHelp = "CancelCheques.htm"; //Cancelamento de Cheques
            break;

        case 'ConsCheques':
        case 'DetPedImgCheques':
            //case 'ListaCheques':
        case 'ListaPedImgCheques': opHelp = "ChequesVisualizacao.htm"; //Visualiza��o de Cheques
            break;

        //Menu Estrangeiro  

        case 'TransferenciaBrasil':
        case 'TransferenciaBrasilConf':
        case 'TransferenciaBrasilOpEfect': opHelp = "TransfBrasil.htm"; //Transfer�ncias P/Brasil
            break;

        case 'TransferenciaEuro':
        case 'TransferenciaEuroConf':
        case 'TransferenciaEuroOpEfect':
            
			if (PeqNegocio === "S") { //Transfer�ncias EURO
				opHelp = "TransfEuropeias_PN.htm";
			} else {
				opHelp = "TransfEuropeias.htm";
			}
            
            break;
        case 'EmissaoOPE':
        case 'EmissaoOPEConf':
        case 'EmissaoOPEOpEfect':
            
                if (PeqNegocio === "S") { //Transfer�ncias internacionais
                    opHelp = "TransfInternacionais_PN.htm";
                } else {
                    opHelp = "TransfInternacionais.htm";
                }
            
            break;
        case 'ListaOPEActiva':
        case 'ListaOPEHist':
        case 'DetalheOPE': opHelp = "TransfConsultasEstrangeiro.htm"; //Consulta OPE
            break;
        case 'ListaOPRActiva':
        case 'ListaOPRHist':
        case 'DetalheOPR': opHelp = "TransfConsultasEstrangeiro.htm"; //Consulta OPR
            break;

        //Menu Pagamentos Servi�os  

        case 'pagservicos':
        case 'pagservicosConf':
        case 'pagservicosOpEfect': opHelp = "PagServicos.htm"; //Pagamento de Servi�os
            break;
        case 'PagEstado':
        case 'PagEstadoConf':
        case 'PagEstadoOpEfect': opHelp = "PagEstado.htm"; //Pagamento ao Estado
            break;

        case 'CarregaTelemoveis':
        case 'CarregaTelemoveisConf':
        case 'CarregaTelemoveisOpEfect': opHelp = "CarregaTelemoveis.htm"; //Carregamento Telemoveis
            break;
        case 'PagTelecomunicacoes':
        case 'PagTelecomunicacoesConf':
        case 'PagTelecomunicacoesOpEfect': opHelp = "PagTelecomunicacoes.htm"; //Pagamento Telecomunica��es
            break;
        case 'PagViaCard':
        case 'PagViaCardConf':
        case 'PagViaCardOpEfect': opHelp = "PagViaCard.htm"; //Pagamento Via Card
            break;

        case 'CarregaOptimus':
        case 'CarregaOptimusConf':
        case 'CarregaOptimusOpEfect': opHelp = "CarregaOptimus.htm"; //Carregamento Optimus
            break;
        case 'CarregaTMN':
        case 'CarregaTMNConf':
        case 'CarregaTMNOpEfect': opHelp = "CarregaTMN.htm"; //Carregamento TMN
            break;
        case 'CarregaVodafone':
        case 'CarregaVodafoneConf':
        case 'CarregaVodafoneOpEfect': opHelp = "CarregaVodafone.htm"; //Carregamento Vodafone
            break;
        case 'CarregaNetPac':
        case 'CarregaNetPacConf':
        case 'CarregaNetPacOpEfect': opHelp = "CarregaNetPac.htm"; //Carregamento NetPac
            break;
        case 'PagSapo':
        case 'PagSapoConf':
        case 'PagSapoOpEfect': opHelp = "PagSapo.htm"; //Pagamento Sapo
            break;
        case 'PagSegSocial':
        case 'PagSegSocialConf':
        case 'PagSegSocialOpEfect': opHelp = "PagSegSocial.htm"; //Pagamento Seg. Social
            break;
        case 'PagCustasJudiciais':
        case 'PagCustasJudiciaisConf':
        case 'PagCustasJudiciaisOpEfect': opHelp = "PagCustasJudic.htm"; //Pagamento Custas Judic.
            break;
        case 'CarregaSCML':
        case 'CarregaSCMLConf':
        case 'CarregaSCMLOpEfect': opHelp = "CarregaSCML.htm"; //Carregamento SCML
            break;

        case 'TSU':
        case 'TSUConf':
        case 'TSUOpEfect': opHelp = "TSU.htm"; //Pagamento Taxa social unica
            break;
        case 'ComprasConf':
        case 'ComprasDet':
        case 'ComprasLista':
        case 'ComprasOpEfect':
        case 'PagEfectuadosDet':
        case 'PagEfectuadosLista': opHelp = "PagClubeBPI.htm"; //Pagamento Clube BPI
            break;
        //Menu Agenda de Pagamentos Servi�os  

        case 'ListaCancelamento': opHelp = "Agenda_Cancelamento.htm"; //Cancelamento Agenda Pagamento de Servi�os
            break;

        //		case 'DetalheOperacao':  
        case 'DetalhePagEfect':
        case 'DetalhePagEfectPDF':
            //		case 'ListaAgendaActiva':						
            //		case 'ListaAgendaHist':
        case 'ListaPagamentosEfect': opHelp = "Agenda_Consulta.htm"; //Consulta Agenda Pagamento de Servi�os
            break;

        //Menu Cartoes Credito  

        case 'DadosCartao': opHelp = "Cartoes_Dados.htm"; //Consulta Dados de Cart�o
            break;

        case 'Saldos': opHelp = "Cartoes_Saldo.htm"; //Consulta Saldo de Cart�o
            break;

        case 'MovsExtractoCartoes': opHelp = "Cartoes_HistMovimentos.htm"; //Consulta Movs de extracto de Cart�o
            break;

        case 'MovsNExtractados': opHelp = "Cartoes_UltMovimentos.htm"; //Consulta Movs Nao extractados de Cart�o
            break;

        case 'ExtractoResumo': opHelp = "Cartoes_Extractos.htm"; //Consulta de extracto de Cart�o
            break;

        case 'PedAdesao':
        case 'PedAdesaoConf':
        case 'PedAdesaoOpEfect': opHelp = "Cartoes_Pedido.htm"; //Pedido de Cart�es
            break;

        case 'PagCartoesLista':
        case 'PagCartoes':
        case 'PagCartoesConf':
        case 'PagCartoesOpEfect': opHelp = "Cartoes_Pagamentos.htm"; //Pagamento de Cart�es
            break;
        case 'AltLimiteCredLista':
        case 'AltLimiteCred':
        case 'AltLimiteCredConf':
        case 'AltLimiteCredOpEfect': opHelp = "Cartoes_LimCred.htm"; //Alt. Limite Credito
            break;
        case 'AltOpcaoPagLista':
        case 'AltOpcaoPag':
        case 'AltOpcaoPagConf':
        case 'AltOpcaoPagOpEfect': opHelp = "Cartoes_OpcaoPag.htm"; //Alt. Opcao Pagamento
            break;

        //Menu MBNet  

        case 'FListaCartoes':
            switch (WhatGif(window.parent.frames.INF_AREA.location)) {
                case 'adesao': opHelp = "MBNet_Adesao.htm";
                    break;
                case 'consulta': opHelp = "MBNet_Consulta.htm";
                    break;
                case 'codigosecreto': opHelp = "MBNet_CodSecreto.htm";
                    break;
                case 'montante': opHelp = "MBNet_Montante.htm";
                    break;
                case 'cancelamento': opHelp = "MBNet_Cancelamento.htm";
                    break;
                default: opHelp = "help.asp";
            }
            break;


        case 'Cartoes':
        case 'CartoesConf':
        case 'CartoesOpEfect': opHelp = "MBNet_Adesao.htm"; //Adesao MBNet
            break;

        case 'CancelamentoConf':
        case 'CancelamentoOpEfect': opHelp = "MBNet_Cancelamento.htm"; //Cancelamento Adesao MBNet
            break;

        //		case 'AlteracaoCodSecreto':		  
        //		case 'AlteracaoCodSecretoOpEfect' :	opHelp = "MBNet_CodSecreto.htm";	//Alteracao Cod.Secreto MBNet  
        //													break;   

        case 'CartoesDetalhe': opHelp = "MBNet_Consulta.htm"; //Consulta Adesao MBNet
            break;

        case 'InstalacaoIcone':
        case 'InstalacaoIconeOpEfect': opHelp = "MBNet_InstIcon.htm"; //Instala��o Icone
            break;

        case 'AlteracaoMontante':
        case 'AlteracaoMontanteConf':
        case 'AlteracaoMontanteOpEfect': opHelp = "MBNet_Montante.htm"; //Altera��o Montante
            break;

        //Menu Fundos de Investimento  

        case 'FListaResgate':
        case 'Resgate':
        case 'ResgateConf':
        case 'ResgateOpEfect': opHelp = "FundosResgate.htm"; //Resgate de fundos
            break;
        case 'Subscricao':
        case 'SubscricaoConf':
        case 'SubscricaoOpEfect': opHelp = "FundosSubscricao.htm"; //Subscri��o de Fundos
            break;
        case 'FListaFundosPP':
        case 'PlanoPeriodicoFundos':
        case 'PlanoPeriodicoFundosDet':
        case 'PlanoPeriodicoFundosConf':
        case 'PlanoPeriodicoFundosOpEfect': opHelp = "FundosPlanosInv.htm"; //Plano Periodico de Fundos
            break;
        case 'FListaReforco':
            //		case 'Reforco':
            //		case 'ReforcoConf':
            //		case 'ReforcoOpEfect':	

            switch (WhatGif(window.parent.frames.INF_AREA.location)) {
                case 'SubSeguintes': opHelp = "FundosReforco.htm"; //Refor�o de Fundos
                    break;
                case 'PPASubSeguintes': opHelp = "ReformaReforco.htm"; //Refor�o de Reforma
                    break;

                default: opHelp = "help.asp";
            }
            break;

        case 'FListaConsulta':
            switch (WhatGif(window.parent.frames.INF_AREA.location)) {
                case 'Consulta': opHelp = "FundosConsulta.htm"; //Consulta Carteira de Fundos
                    break;
                case 'FListaConsulta': opHelp = "ReformaPlanosReforco.htm"; //Cria��o Planos de Reforma
                    break;
                default: opHelp = "help.asp";
            }
            break;

        //Menu Seguros de Capitaliza��o  

        case 'SubscricaoSC':
        case 'SubscricaoSCConf':
        case 'SubscricaoSCOpEfect': opHelp = "SegCap_Subscricao.htm"; //Subscricao
            break;

        case 'FListaReforcoSC':
        case 'ReforcoSC':
        case 'ReforcoSCConf':
        case 'ReforcoSCOpEfect': opHelp = "SegCap_Reforco.htm"; //Refor�o
            break;

        case 'FListaConsultaSC':
        case 'FListaConsultaPP':
        case 'ConsultaDetSC': opHelp = "SegCap_Consulta.htm"; //Consulta
            break;

        //Menu Reforma  

        case 'CancelarConf':
        case 'CancelarOpEfect':
        case 'PPADetalhe':
        case 'PPAListaConsulta': opHelp = "ReformaConsulta.htm"; //Consulta PPA
            break;

        case 'SubscricaoInicial':
        case 'SubscricaoInicialConf':
        case 'SubscricaoInicialOpEfect': opHelp = "ReformaSubscricao.htm"; //Subscri��o PPA
            break;

        case 'PlanoPeriodicoDet':
        case 'PlanoPeriodicoConf':
        case 'PlanoPeriodicoOpEfect': opHelp = "ReformaPlanosReforco.htm"; //Planos Reforma
            break;
        case 'Simulador':
        case 'SimuladorOk':
        case 'SimuladorPDF': opHelp = "SimuladorReforma.htm"; //Simulador Reforma
            break;

        //Menu Dep�sitos a Prazo  
        case 'DPConstit':
        case 'DPConstitConf':
        case 'DPConstitOpEfect': opHelp = "App_Constituicao.htm"; //Ap Prazo - Constitui��o
            break;

        case 'DPListaReforco':
        case 'DPReforco':
        case 'DPReforcoConf':
        case 'DPReforcoOpEfect': opHelp = "App_reforco.htm"; //Ap Prazo - Mobiliza��o
            break;
        case 'DPListaMobiliz':
        case 'DPMobiliz':
        case 'DPMobilizConf':
        case 'DPMobilizOpEfect': opHelp = "App_Mobilizacao.htm"; //Ap Prazo - Mobiliza��o
            break;

        case 'DPDetalheOperAgenda':
        case 'DPListaAgendas':
        case 'DPListaCarteira':
        case 'DPListaMovimentos':
        case 'DPListaOperAgenda':
        case 'DPDetalhe': opHelp = "App_Consulta.htm"; //Ap Prazo - Consulta
            break;

        case 'DPTaxa':
        case 'DPTaxaConf': opHelp = "App_Taxas.htm"; //Ap Prazo - Simula Taxas
            break;

        //Menu Contas Poupan�a  

        case 'CPConstit':
        case 'CPConstitConf':
        case 'CPConstitOpEfect': opHelp = "Cpp_Constituicao.htm"; //Contas Poupan�a - Constitui��o
            break;

        case 'CPListaReforco':
        case 'CPReforco':
        case 'CPReforcoConf':
        case 'CPReforcoOpEfect': opHelp = "Cpp_reforco.htm"; //Contas Poupan�a - Mobiliza��o
            break;
        case 'CPListaMobiliz':
        case 'CPMobiliz':
        case 'CPMobilizConf':
        case 'CPMobilizOpEfect': opHelp = "Cpp_Mobilizacao.htm"; //Contas Poupan�a - Mobiliza��o
            break;

        case 'CPDetalheOperAgenda':
        case 'CPListaAgendas':
        case 'CPListaCarteira':
        case 'CPListaTranches':
        case 'CPListaOperAgenda':
        case 'CPDetalhe': opHelp = "Cpp_Consulta.htm"; //Contas Poupan�a - Consulta
            break;

        //Menu Obriga��es/P.E.'s  

        case 'ObrigacoesDet':
        case 'ObrigacoesSubs':
        case 'ObrigacoesSubsConf':
        case 'ObrigacoesSubsLista':
        case 'ObrigacoesSubsOpEfect': opHelp = "ObrigacoesSubscricao.htm"; //Obrigacoes - Subscricao
            break;

        case 'ObrigacoesCancLista':
        case 'ObrigacoesCancConf':
        case 'ObrigacoesCancOpEfect': opHelp = "ObrigacoesCancelamento.htm"; //Obrigacoes - Cancelamento
            break;
        case 'ConsultaListaOrdens':
        case 'DetalheOrdem': opHelp = "ObrigacoesConsultaOrdens.htm"; //Obrigacoes - Consulta Ordens
            break;


        case 'ConsultaListaCarteira': opHelp = "ObrigacoesConsultaCarteira.htm"; //Obrigacoes - Consulta Carteira
            break;

        //Menu Operacoes de Bolsa  

        case 'OBCompraLista':
        case 'OBCompra':
        case 'OBCompraConf':
        case 'OBCompraOpEfect': opHelp = "BOLSA_Compra.htm"; //Op de Bolsa - Compra
            break;
        case 'OBVenda':
        case 'OBVendaConf':
        case 'OBVendaLista':
        case 'OBVendaOpEfect': opHelp = "BOLSA_Venda.htm"; //Op de Bolsa - Venda
            break;
        case 'OBListaCancela':
        case 'OBCancelaConf':
        case 'OBCancelaOpEfect': opHelp = "Bolsa_Cancelamento.htm"; //Op de Bolsa - Cancelamento
            break;
        case 'OBDetalhe':
        case 'OBListaConsulta': opHelp = "Bolsa_ConsOrdem.htm"; //Op de Bolsa - Consulta Ordens
            break;

        case 'DCListaConsulta': opHelp = "Bolsa_Carteira.htm"; //Op de Bolsa - Consulta Carteira
            break;
        case 'MVDetalhe':
        case 'MVListaConsulta': opHelp = "Bolsa_MaisValias.htm"; //Op de Bolsa - Consulta Mais Valias
            break;
        case 'NoticiasBolsa':
        case 'PaginaPrincipal': opHelp = "Bolsa_Agenda.htm"; //Op de Bolsa - Agenda
            break;
        //Menu Cotacoes  

        case 'consulta_bolsa': opHelp = "Cotacoes_Bolsa.htm"; //Cota��es - Bolsa
            break;
        case 'consulta_cambios': opHelp = "Cotacoes_Cambios.htm"; //Cota��es - C�mbios
            break;
        case 'consulta_fundos':
            opHelp = "Cotacoes_Fundos.htm"; //Cota��es - Fundos
            break;
        case 'consulta_capSeguro':
            opHelp = "Cotacoes_CapitalSeguro.htm"; //Cota��es - Capital Seguro
            break;
        //Menu OPV's  

        case 'FListaCompra':
        case 'Compra':
        case 'CompraConf':
        case 'CompraOpEfect':
        case 'CompraEmprestimo':
            opHelp = "OPV_IntOrdemCompra.htm"; //Opv Ordem de Compra
            break;

        case 'FListaAlteracao':
        case 'AlteracaoConf':
        case 'AlteracaoEmprestimo':
        case 'AlteracaoOpEfect':
        case 'Alteracao':
            opHelp = "OPV_Alteracao.htm"; //Opv Altera��o
            break;

        case 'OpvConsulta':
        case 'OpvDetalhe':
        case 'OpvEfectuado':
            opHelp = "OPV_Consulta.htm"; //Opv Consulta
            break;

        case 'CancelaConf':
        case 'CancelaOpEfect':
        case 'ListaCancela':
            opHelp = "OPV_Cancelamento.htm"; //Opv Cancelamento
            break;
        //Menu Exercicio de Direitos  

        case 'EDListaCancela':
        case 'EDListaConsulta':
        case 'EDListaOrdem':
            switch (WhatGif(window.parent.frames.INF_AREA.location)) {
                case 'consultaED':
                    opHelp = "ExerDireitos.htm#cons"; //exercicio de direitos
                    break;
                case 'OrdemED':
                    opHelp = "ExerDireitos.htm#ordem"; //exercicio de direitos
                    break;
                case 'cancelamentoED':
                    opHelp = "ExerDireitos.htm#canc"; //exercicio de direitos
                    break;
                case 'OrdemOT':
                    opHelp = "OPT_Ordem.htm"; //Operacoes Ordem de troca
                    break;
                case 'consultaOT':
                    opHelp = "OPT_Consulta.htm"; //Operacoes Consulta de troca
                    break;
                case 'cancelamentoOT':
                    opHelp = "OPT_Cancelamento.htm"; //Operacoes cancelamento de troca
                    break;
                default: opHelp = "help.asp";
            }
            break;


        case 'EDListaAlteraRateio':
        case 'EDAlteraRateio':
        case 'EDAlteraRateioConf':
        case 'EDAlteraRateioOpEfect':
            opHelp = "ExerDireitos.htm#rat"; //exercicio de direitos
            break;

        case 'EDCancelaConf':
        case 'EDCancelaOpEfect':
        case 'EDDetalhe':
        case 'EDOrdem':
        case 'EDOrdemConf':
        case 'EDOrdemOpEfect':
            opHelp = "help.asp";
            break;

        //Menu Exercicio de Opcoes  

        case 'EOListaCancela':
        case 'EOCancelaConf':
        case 'EOCancelaOpEfect': opHelp = "ExerOpcoesCanc.htm"; //Cancelamento Exercicio de Opcoes
            break;

        case 'EOListaConsulta':
        case 'EODetalhe':
            opHelp = "ExerOpcoesCons.htm"; //Consulta Exercicio de Opcoes
            break;

        case 'EOListaOrdem':
        case 'EOOrdem':
        case 'EOOrdemConf':
        case 'EOOrdemOpEfect':
            opHelp = "ExerOpcoes.htm"; //Ordem Exercicio de Opcoes
            break;

        //BPINet Habita��o 

        case 'BPINetHabitacao':
            opHelp = "ajuda_gpc.htm"; //GPC
            break;
        case 'ConsultaResumo':
        case 'ConsultaPropostasLista':
        case 'ConsultaDocumentosLista':
            opHelp = "ajuda_gpc.htm#prop"; //GPC
            break;

        // Personaliza��o   

        //Benefici�rios  
        case 'ListaBeneficiariosTransf':
        case 'ListaTransfBenef':
        case 'CriaBeneficiarios':
        case 'CriaBeneficiariosConf':
        case 'CriaBeneficiariosOpEfect':
        case 'DelBeneficiariosConf':
        case 'DelBeneficiariosOpEfect':
        case 'DetBeneficiarios':
        case 'DetalheTransferencia':
            opHelp = "PersBeneficiario.htm"; //Beneficiarios
            break;
        // Lista Benefici�rios 
        case 'ListaBeneficiarios':
            opHelp = "PersConsListaBeneficiario.htm"; //Beneficiarios Cons.Lista
            break;
        // Manuten��o Benefici�rios  
        case 'ManBeneficiariosLista':
        case 'ManBeneficiarios':
        case 'ManBeneficiariosConf':
        case 'ManBeneficiariosOpEfect':
            opHelp = "beneficiario_alterarEliminar.htm"; //Beneficiarios
            break;

        //Opera��es Predefinidas  

        case 'DetBenefEntidades':
        case 'ListaBenefEntidades':
            opHelp = "PersOperPreDefConsulta.htm"; // Consulta Oper. Predefinidas
            break;

        case 'DelBenefEntidadesConf':
        case 'DelBenefEntidadesOpEfect':
        case 'ManBenefEntidades':
        case 'ManBenefEntidadesConf':
        case 'ManBenefEntidadesOpEfect':
            opHelp = "PersOperPreDefAlteracao.htm"; // Altera Oper. Predefinidas
            break;

        case 'CriaBenefEntidades':
        case 'CriaBenefEntidadesConf':
        case 'CriaBenefEntidadesOpEfect':
            opHelp = "PersOperPreDefCriacao.htm"; // Cria Oper. Predefinidas
            break;

        //ClubeBPI  
        case 'CriaUtilizadores':
        case 'CriaUtilizadoresConf':
        case 'CriaUtilizadoresOpEfect':
        case 'DetUtilizadores':
        case 'ManUtilizadores':
        case 'ManUtilizadoresConf':
        case 'ManUtilizadoresOpEfect':
            opHelp = "PersClubeBPI.htm"; //Clube BPI
            break;


        // Manuten��o Extracto Digital  
        case 'SubscrExtractoDigital':
        case 'SubscrExtractoDigitalOpEfect':
        case 'ListaExtractosNucs':
            opHelp = "ExtractoDigital.htm"; //Manuten��o Extracto Digital
            break;

        //Codigo Secreto  

        case 'AlteracaoCodSecreto':
        case 'AlteracaoCodSecretoOK':
            opHelp = "CodSecreto.htm"; //C�digo Secreto
            break;

        //Contactos  
        // Contactos antes do SIP  
        //		case 'Contactos' :  
        //		case 'ContactosOpEfect' :				opHelp = "MeusContactos.htm";		//Contactos  
        //														break;   
        //Contactos ap�s o SIP  
        case 'ListaContactos':
        case 'ContactosAct':
        case 'CriaContactos':
        case 'CriaContactosOpEfect':
        case 'ContactosActOpEfect':
            opHelp = "MeusContactos.htm"; //Meus Contactos
            break;
        //Contrato  

        case 'contrato':
                if (PeqNegocio === "S") { //Contrato
                    opHelp = "contratoPN.htm";
                } else {
                    opHelp = "contrato.htm";
                }
            break;

        //Periodicidade Extracto  
        case 'PerExtractoInt':
        case 'PerExtractoIntConf':
        case 'PerExtractoIntOpEfect':
            opHelp = "PerExtracto.htm"; //Periodicidade de Extracto
            break;

        //Lista Pessoal  

        case 'ListaPessoalA':
        case 'ListaPessoal':
            opHelp = "ListaPessoal.htm"; //Personaliza��o - Lista Pessoas
            break;
        //Nome Adesao  
        case 'AlteracaoNomeAdesao':
        case 'AlteracaoNomeAdesaoOK':
            opHelp = "NomeAcesso.htm"; //Personaliza��o - Nome Acesso
            break;
        //Personalizacao  

        case 'personalizacao':
        case 'personalizacaoA':
            opHelp = "Acesso.htm"; //Personaliza��o
            break;

        //MeuMenu  

        case 'MeuMenu':
        case 'MeuMenuOK':
            opHelp = "PersMeuMenu.htm"; //Personaliza��o Meu Menu
            break;
        //Cons. Opera��es  

        case 'ListaOperacoes':
        case 'DetalheOperacao':
            opHelp = "PersConsOperacoes.htm"; //Personaliza��o Cons.Opera��es
            break;

        default: opHelp = "help.asp";
    }
    //opHelp = "?" + opHelp;

    if (Linguagem === "PT") {
        new_window = window.open('areaInf/help/' + opHelp, 'um', 'toolbar=no,menubar=no,status=no,resizable=YES,location=no,scrollbars=yes,left=400,top=200,width=375,height=300', false);
    }
    else {
        new_window = window.open('areaInf/help/' + Linguagem + "/" + opHelp, 'um', 'toolbar=no,menubar=no,status=no,resizable=YES,location=no,scrollbars=yes,left=400,top=200,width=375,height=300', false);
    }
    new_window.focus();
    return true;
}


