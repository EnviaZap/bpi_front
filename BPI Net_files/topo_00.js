/*global TipoBrowser, ViewPageInfo, MenuId, oMenuEscolhido:true, doEnable, oMenuSelected:true, l_AddMenu, ProtegeMenu, mn_MeuMenu:true, mn_Consultas:true, mn_Cartoes:true, mn_Investimento:true, mn_Bolsa:true, mn_Servicos:true */
/*global MeuMenu, Contas, Cartoes, Invest, Bolsa, Servicos, i:true, l_ArrMenu:true, top, oMenuMeuMenu:true */
/*jslint newcap:true*/ 
 
var flag_submit = true;
var Opcao		= 0;

function CarregaMeuMenu()
{ 
	'use strict';
	var oMenuMeuMenu = "l_menu1";
	
	if (ViewPageInfo != "1")
	{
		if (MenuId == "")
		{
			if (TipoBrowser == "IE"){
				oMenuEscolhido	= document.getElementById(oMenuMeuMenu);
			}	
			else{
				oMenuEscolhido = document.layers[0].document.layers[0].document.anchors[oMenuMeuMenu];
			}	
			doEnable(true,  oMenuEscolhido,1);
			oMenuSelected	= oMenuEscolhido;
		}
	}
}

function CarregaMenu()
{
	'use strict';
	if (ProtegeMenu == "N") 
	{
		mn_MeuMenu		= l_AddMenu(MeuMenu			, "top.menu.location.href = '/menu.asp?inf=meumenu';"		, true);
		mn_Consultas	= l_AddMenu(Contas			, "top.menu.location.href = '/menu.asp?inf=contasaordem';"	, true);
		mn_Cartoes		= l_AddMenu(Cartoes			, "top.menu.location.href = '/menu.asp?inf=cartoes';"		, true);
		mn_Investimento	= l_AddMenu(Invest			, "top.menu.location.href = '/menu.asp?inf=investimento';"	, true);
		mn_Bolsa		= l_AddMenu(Bolsa			, "top.menu.location.href = '/menu.asp?inf=bolsa';"			, true);
		mn_Servicos		= l_AddMenu(Servicos		, "top.menu.location.href = '/menu.asp?inf=servicos';"		, true);
	}
	else
	{
		mn_MeuMenu		= l_AddMenu(MeuMenu			, "top.menu.location.href = '/menu.asp?inf=meumenu';"		, false);
		mn_Consultas	= l_AddMenu(Contas			, "top.menu.location.href = '/menu.asp?inf=contasaordem';"	, false);
		mn_Cartoes		= l_AddMenu(Cartoes			, "top.menu.location.href = '/menu.asp?inf=cartoes';"		, false);
		mn_Investimento	= l_AddMenu(Invest			, "top.menu.location.href = '/menu.asp?inf=investimento';"	, false);
		mn_Bolsa		= l_AddMenu(Bolsa			, "top.menu.location.href = '/menu.asp?inf=bolsa';"			, false);
		mn_Servicos		= l_AddMenu(Servicos		, "top.menu.location.href = '/menu.asp?inf=servicos';"		, false);
	}

	var TextoObj, NomeMenu, newlayer;
	
	TextoObj = '<table border="0" width="100%" cellspacing="1" cellpadding="1" style="padding-right: 1px; padding-left: 1px; color: #ffffff; background-color: #ffffff">';
	document.td_menu.layers[0].document.write(TextoObj);

	TextoObj = '<tr style="BACKGROUND-COLOR: #000066">';								
	document.td_menu.layers[0].document.write(TextoObj);
		
	for( i = 0 ; i < l_ArrMenu.length ; i+=2 )
	{
		if( l_ArrMenu[i][1] != null )
		{	NomeMenu = "'l_menu" + String(i+1) + "'" ;
			TextoObj = '<td width="100" align=center><a name="l_menu' + String(i+1) + '" id="l_menu' + String(i+1) + '" href="javascript://" onclick="DoClick(' + NomeMenu + ',' + i + ');"><p style="COLOR: #ffffff;  BORDER-BOTTOM: #000066 1px solid;BORDER-LEFT: #000066 1px solid;BORDER-RIGHT: #000066 1px solid;BORDER-TOP: #000066 1px solid;BACKGROUND-COLOR: #000066;FONT-FAMILY: Verdana, Arial; FONT-SIZE: 11px;">' + l_ArrMenu[i][0] + '</p></a></td>';
			document.td_menu.layers[0].document.write(TextoObj);
		}
		else
		{
			TextoObj = '<td width="100" align=center id="l_menu' + String(i+1) + '" class="MenuBarItem">' + l_ArrMenu[i][0] + '</td>';
			document.td_menu.layers[0].document.write(TextoObj);
		}
	}
	TextoObj = '</tr>';								
	document.td_menu.layers[0].document.write(TextoObj);
	TextoObj = '</table>';
	document.td_menu.layers[0].document.write(TextoObj);
	document.td_menu.layers[0].document.close();
	
	
	newlayer = new Layer(document.td_menu.layers[0].clip.width,document.td_menu);
}

function Main() {
    'use strict';
    $(document).ready(
    function () {
        if (TipoBrowser == "IE") {
            CarregaMeuMenu();
        }
        else {
            CarregaMenu();
        }
    });
}

Main();

function Precario(ClienteNetBolsa)
{
	'use strict';
	var new_window;
    new_window = window.open('http://www.bancobpi.pt/pagina.asp?s=1&f=3121&opt=f','um','toolbar=no,menubar=no,status=no,resizable=YES,location=no,scrollbars=yes,left=400,top=200,width=375,height=300',false);
	new_window.focus();
	return true;
}

function Contacto()
{
	'use strict';
	var new_window;
	new_window = window.open('contacto.asp','um','toolbar=no,menubar=no,status=no,resizable=YES,location=no,scrollbars=yes,left=400,top=200,width=620,height=450',false);
	new_window.focus();
	return true;
}



function PagInicial()
{
	'use strict';
	doEnable(false, oMenuSelected,i);
	CarregaMeuMenu();
	top.menu.location.href		= '/menu.asp?inf=meumenu';
}



function CarregaMenuBolsa(IdMenu)
{	
	'use strict';
	var NumMenu;
	
	switch (IdMenu) 
	{
		case 'meumenu'		:
		case 'pagprincipal'	:	NumMenu = 1;
				break; 		

		case 'contasaordem' :	NumMenu = 3;
				break; 		

		case 'cartoes'		:	NumMenu = 5;
				break; 		

		case 'investimento' :	NumMenu = 7;
				break; 		

		case 'servicos'		:	NumMenu = 11;
				break; 		
	}
	
	if (IdMenu == "contacto"){
		top.menu.location.href		= '/menu.asp?inf=&idmenu=contacto';
	}	
	else
	{	
		oMenuEscolhido	    = "l_menu" + NumMenu;
		oMenuMeuMenu	    = "l_menu1";
	
		if (TipoBrowser == "IE")
		{
			oMenuMeuMenu	= document.getElementById(oMenuMeuMenu);
			oMenuEscolhido	= document.getElementById(oMenuEscolhido);
		}
		else
		{
			oMenuMeuMenu	= document.layers[0].document.layers[0].document.anchors[oMenuMeuMenu];
			oMenuEscolhido	= document.layers[0].document.layers[0].document.anchors[oMenuEscolhido];
		}
		
		doEnable(true,  oMenuEscolhido,NumMenu);
	
		oMenuSelected = oMenuEscolhido;
		if (IdMenu == 'pagprincipal')
		{
			IdMenu = 'meumenu';
		}
		top.menu.location.href		= '/menu.asp?inf=' + IdMenu;
	}
}