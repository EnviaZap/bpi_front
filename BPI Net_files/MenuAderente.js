/*jslint evil:true */
/*global ProtegeMenu, mn_MeuMenu:true, mn_Consultas:true, mn_Cartoes:true, mn_Investimento:true, mn_Bolsa:true, mn_Servicos:true, l_AddMenu, l_ArrMenu, MeuMenu, Contas, Cartoes, Invest, Servicos, Bolsa */

if (ProtegeMenu === "N") {
    mn_MeuMenu = l_AddMenu(MeuMenu, "top.menu.location.href = '/menu.asp?inf=meumenu';", true);
    mn_Consultas = l_AddMenu(Contas, "top.menu.location.href = '/menu.asp?inf=contasaordem';", true);
    mn_Cartoes = l_AddMenu(Cartoes, "top.menu.location.href = '/menu.asp?inf=cartoes';", true);
    mn_Investimento = l_AddMenu(Invest, "top.menu.location.href = '/menu.asp?inf=investimento';", true);
    mn_Bolsa = l_AddMenu(Bolsa, "top.menu.location.href = '/menu.asp?inf=bolsa';", true);
    mn_Servicos = l_AddMenu(Servicos, "top.menu.location.href = '/menu.asp?inf=servicos';", true);
}
else {
    mn_MeuMenu = l_AddMenu(MeuMenu, "top.menu.location.href = '/menu.asp?inf=meumenu';", false);
    mn_Consultas = l_AddMenu(Contas, "top.menu.location.href = '/menu.asp?inf=contasaordem';", false);
    mn_Cartoes = l_AddMenu(Cartoes, "top.menu.location.href = '/menu.asp?inf=cartoes';", false);
    mn_Investimento = l_AddMenu(Invest, "top.menu.location.href = '/menu.asp?inf=investimento';", false);
    mn_Bolsa = l_AddMenu(Bolsa, "top.menu.location.href = '/menu.asp?inf=bolsa';", false);
    mn_Servicos = l_AddMenu(Servicos, "top.menu.location.href = '/menu.asp?inf=servicos';", false);
}

// Build Top Menus
var sEnableText, i;

for (i = 0; i < l_ArrMenu.length; i += 2) {
    sEnableText = l_ArrMenu[i][2];

    if (l_ArrMenu[i][1] !== null) {
        document.write('<td width="100" align="center" style="cursor:pointer;" id="l_menu' + String(i + 1) + '" class="MenuBarItem" onclick="DoClick(this,' + i + ');">' + l_ArrMenu[i][0] + '</td>');
    }
    else {
        document.write('<td width="100" align="center" id="l_menu' + String(i + 1) + '" class="MenuBarItem">' + l_ArrMenu[i][0] + '</td>');
    }
}
