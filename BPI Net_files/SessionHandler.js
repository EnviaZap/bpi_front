﻿/*jslint newcap: true */
/*global isLO, isC */

/************************************
* Get the current value of a cookie *
************************************/
function GetCookieValue(key) {
    'use strict';
    var name = key + "=",
        cookieArray = document.cookie.split(';'),
        i = 0, cookie = "", cookieValue = "";

    for (i = 0; i < cookieArray.length; i++) {
        cookie = cookieArray[i];
        while (cookie.charAt(0) == ' ') {
            cookie = cookie.substring(1);
        }
        if (cookie.indexOf(name) == 0) {
            cookieValue = cookie.substring(name.length, cookie.length);
        }
    }

    return cookieValue;
}

/******************************
* Signs off the current user. *
******************************/
function TerminateSession() {
    'use strict';
    var signOff = true;

    // Navigations to/from Net Bolsa or from Campaigns are not supposed to trigger sign off events
    if (isC.toLowerCase() == 's' || GetCookieValue("BNB").toLowerCase() == 's') {
        signOff = false;
    }

    if (signOff) {
        document.cookie = "BNB=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
        $.post("/SignOff.asp");
    }
}

/************************************************************************************
* Sets up the session handler for the unloading event of the website's main window. *
************************************************************************************/
function SetupSessionHandlers() {
    'use strict';
    $(window).bind("beforeunload", function () {
        TerminateSession();
    });
}

/*****************************************************
* Document ready event.                              *
* Applies to: Internet Explorer, Firefox and Safari. *
*****************************************************/
$().ready(function () {
    'use strict';
    if (isLO.toLowerCase() == "true") {
        SetupSessionHandlers();
    }
});
