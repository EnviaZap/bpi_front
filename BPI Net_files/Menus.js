/*jslint nomen: true, debug: true, evil: true, vars: true */
/*global TipoBrowser, oMenuEscolhido:true, doEnable*/
var topMenu = null;
var bottomMenu = null;
var oldColor = null;


var g_ArrMenu = [];
var l_ArrMenu = [];

function g_AddMenu(sMenu, sLink) {
    'use strict';
    g_ArrMenu[g_ArrMenu.length] = [sMenu, sLink];
    g_ArrMenu[g_ArrMenu.length] = [];
    return g_ArrMenu.length - 1;
}

function l_AddMenu(sMenu, sLink, bEnabled) {
    'use strict';
    if (typeof (bEnabled) === "undefined") {
        bEnabled = true;
    }
    l_ArrMenu[l_ArrMenu.length] = [sMenu, sLink, bEnabled];
    l_ArrMenu[l_ArrMenu.length] = [];
    return l_ArrMenu.length - 1;
}

var oMenuSelected = null;

function DoClick(sMenuEscolhido, iNumMenu) {
    'use strict';
    if (TipoBrowser === "NN") {
        oMenuEscolhido = document.layers[0].document.layers[0].document.anchors[sMenuEscolhido];
    }
    else {
        oMenuEscolhido = eval(sMenuEscolhido);
    }

    var bIsOn;
    bIsOn = false;

    bIsOn = l_ArrMenu[iNumMenu][2]; 
    if (bIsOn) {
        if (oMenuSelected !== oMenuEscolhido) {
            doEnable(false, oMenuSelected, iNumMenu);
            doEnable(true, oMenuEscolhido, iNumMenu);
            oMenuSelected = oMenuEscolhido; 
        }
    }
    return false; 
}

function doEnable(b, objMenu, iNumMenu) {
    'use strict';
    if (!objMenu) {
        return false; 
    }
    if (b) {
        if (TipoBrowser !== "NN") {
            objMenu.style.color = "#000066";
            objMenu.style.backgroundColor = "#ffffff";
            objMenu.style.cursor = "default";
        }
        var MenuAbrir = l_ArrMenu[iNumMenu][1];
        eval(MenuAbrir);
    }
    else {
        if (TipoBrowser !== "NN") {
            objMenu.style.color = "#ffffff";
            objMenu.style.backgroundColor = "#000066";
            objMenu.style.cursor = "hand";
        }
    }
}

/*primeiroAcesso Outsystems - Pintar opcao de menu sem navegar*/
function doEnableNoNavegate(b, objMenu) {
    'use strict';
    if (!objMenu) {
        return false;
    }
    if (b) {
        if (TipoBrowser !== "NN") {
            objMenu.style.color = "#000066";
            objMenu.style.backgroundColor = "#ffffff";
            objMenu.style.cursor = "default";
			
			oMenuSelected = objMenu;
        }
    }
	return true;
}
